package picker_admin;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractJUnit4SpringContextTests;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import map.dao.MapLinesDao;
import map.dao.MapStorageDao;
import order.dao.OrderDataDao;

@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "file:src/main/webapp/WEB-INF/spring-config/spring-bean.xml",
		"file:src/main/webapp/WEB-INF/spring-config/spring-mvc.xml" })
public class DataInsert extends AbstractJUnit4SpringContextTests {

	@Autowired
	JdbcTemplate jdbcTemplate;

	private String placeFileName = "D:\\前瞻計畫\\2018大AI計畫\\萊爾富資料\\20180521北區常溫整箱區基本資料.xlsx";
	private String orderFileName = "D:\\前瞻計畫\\2018大AI計畫\\萊爾富資料\\訂單\\C.TXT";
	private HashMap<String, String> placeMap = new HashMap<>();
	private String mapStorageFileName = "D:\\前瞻計畫\\2018大AI計畫\\儲位配置\\座標data\\c_area.csv";
	private String mapLinesFile = "D:\\前瞻計畫\\2018大AI計畫\\儲位配置\\座標data\\lines.csv";

	@Test
	public void insertDataFromFile() {
		readPlace();
		readOrders();
		// readMaps();
	}

	private void readMaps() {
		System.out.println("讀取地圖資訊...");

		try {
			List<String> storages = FileUtils.readLines(new File(mapStorageFileName));
			ArrayList<Object[]> entries = new ArrayList<>();
			for (String line : storages) {
				String tmp[] = line.split(",");
				if (!tmp[0].isEmpty()) {
					Object entry[];
					if (tmp[0].startsWith("C")) {
						entry = new Object[] { "2", tmp[0], tmp[1], tmp[2], Integer.parseInt(tmp[3]) % 2, tmp[4],
								tmp[5] };
						System.out.println(line);
					} else {
						entry = new Object[] { "1", tmp[0], tmp[1], tmp[2], Integer.parseInt(tmp[3]) % 2, tmp[4],
								tmp[5] };
					}

					entries.add(entry);
				}
			}
			MapStorageDao.insertBatch(entries);

			List<String> lines = FileUtils.readLines(new File(mapLinesFile));
			entries = new ArrayList<>();
			for (String line : lines) {
				String tmp[] = line.split(",");
				if (!tmp[0].equals("x1")) {
					// Object entry[] = new Object[] {"1",tmp[0], tmp[1], tmp[2], tmp[3]};
					// entries.add(entry);
					Object entry[] = new Object[] { "1", String.valueOf(Math.round(Double.parseDouble(tmp[0]))),
							String.valueOf(Math.round(Double.parseDouble(tmp[1]))),
							String.valueOf(Math.round(Double.parseDouble(tmp[2]))),
							String.valueOf(Math.round(Double.parseDouble(tmp[3]))) };
					entries.add(entry);
				}
			}
			MapLinesDao.insertBatch(entries);

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private void readOrders() {

		System.out.println("讀取訂單...");

		File file = new File(orderFileName);

		try {
			HashMap<String, ArrayList<Object[]>> orderList = new HashMap<>();
			 
			//ArrayList<Object[]> flowList = new ArrayList<>();
			List<String> lines = FileUtils.readLines(file);
			int count = 0;
			for (String line : lines) {
				String[] order = line.split(",");
				String goodId = order[2];
				String placeId = placeMap.get(goodId);
				int warehouseId;
				if (StringUtils.startsWith(placeId, "C")) {
					warehouseId = 2;
				} else {
					warehouseId = 1;
				}

				String date = order[0].replace("/", "");
				String store = order[1];
				String qty = order[3];
				String yyyymm = date.substring(0, 6);
				if(!orderList.containsKey(yyyymm)) {
					orderList.put(yyyymm, new ArrayList<>());
				}
				orderList.get(yyyymm).add(new Object[] { warehouseId, date, store, goodId, placeId, qty, store, DateTimeFormatter.ofPattern("yyyyMMddHHmmss").format(LocalDateTime.now()),"ATone","","0"});
				//flowList.add(new Object[] { warehouseId, date, store, goodId, placeId, qty, store, "",
				//		new JDateTime().toString("YYYYMMDDhhmmss"), "", "", "", "", "", "" });
				count++;
				if (count % 5000 == 0) {
					System.out.println(count + "/" + lines.size());
				}

				if (count % 100000 == 0) {
					for(String yymm : orderList.keySet()) {
						OrderDataDao.insertBatch(orderList.get(yymm), yymm);
					}					
					//PickingFlowDao.insertBatch(flowList);
					orderList.clear();
					//flowList.clear();
				}
			}
			
			for(String yymm : orderList.keySet()) {
				OrderDataDao.insertBatch(orderList.get(yymm), yymm);
			}
			//PickingFlowDao.insertBatch(flowList);
			orderList.clear();
			//flowList.clear();

			System.out.println("訂單讀取完畢.");

		} catch (Exception e) {
			e.printStackTrace();
			System.exit(1);
		}

	}

	private void readPlace() {

		System.out.println("讀取揀位擋...");

		File file = new File(placeFileName);

		if (!file.exists()) {
			System.err.println("file not found: " + placeFileName);
			System.exit(1);
		}

		try {
			XSSFWorkbook workbook = new XSSFWorkbook(file);
			XSSFSheet sheet = workbook.getSheetAt(0);
			int rowNum = sheet.getPhysicalNumberOfRows();
			for (int y = 1; y < rowNum; y++) {
				XSSFRow row = sheet.getRow(y);
				DataFormatter df = new DataFormatter();
				if (row.getCell(2) == null || "".equals(row.getCell(2).toString())) {
					continue;
				}
				String goodId = df.formatCellValue(row.getCell(2));
				String placeId = df.formatCellValue(row.getCell(1));
				if (!placeMap.containsKey(goodId)) {
					placeMap.put(goodId, placeId);
				}
			}
			workbook.close();

			System.out.println("揀位擋讀取完畢.");

		} catch (Exception e) {
			e.printStackTrace();
			System.exit(1);
		}

	}

}
