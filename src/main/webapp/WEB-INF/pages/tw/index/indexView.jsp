<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!doctype html>
<html>

<%@include file="../include/includeHeader.jsp"%>
<body>
	<div id="wrapper">

		<%@include file="../include/includeNavbar.jsp"%>
		<%@include file="../include/includeSideNav.jsp"%>

		<div id="page-wrapper" class="gray-bg">
			<div class="row">
				<nav class="navbar" role="navigation" style="margin-bottom: 0">
					<div class="navbar-header">
						<a class="navbar-minimalize minimalize-styl-2 btn btn-primary"
							href="#"><i class="fa fa-bars"></i> </a>
					</div>
					<h1>功能總覽</h1>
				</nav>
			</div>

			<div id="main-content" class="wrapper wrapper-content">
				<!-- 主內容 -->
				<div id="banner" class="ibox float-e-margins">
					<div class="ibox-content">
						<div class="row">
							<div class="col-lg-3 col-sm-6 p-md text-center">
								<a href="./orderData"> <img src="images/ipicking_01.png"
									class="icon">
								</a>
								<h3>訂單上傳 / 管理</h3>
								<p>上傳和管理來自您的客戶的訂單，這些訂單將於後續步驟中作為揀貨單的來源。</p>
							</div>
							<div class="col-lg-3 col-sm-6 p-md text-center">
								<a href="./pickingOrder"> <img src="images/ipicking_02.png"
									class="icon"></a>
								<h3>產生揀貨單</h3>
								<p>根據訂單產生揀貨單。系統將在產生揀貨單的同時，針對每張揀貨單建議合適的揀貨順序。</p>
							</div>
							<div class="col-lg-3 col-sm-6 p-md text-center">
								<a href="./pickingFlow"><img src="images/ipicking_03.png"
									class="icon"></a>
								<h3>揀貨單管理</h3>
								<p>瀏覽和管理系統產生的揀貨單，此外，若揀貨人員有使用APP協助揀貨工作，也可在此功能中檢視揀貨進度。</p>
							</div>
							<div class="col-lg-3 col-sm-6 p-md text-center">
								<a target="_blank" href="http://60.248.82.82/app/pickingAssistant.apk"><img src="images/ipicking_04.png" class="icon"></a>
								<h3>下載揀貨助理APP</h3>
								<p>揀貨助理APP能在揀貨作業中引導揀貨員作業，並記錄揀貨作業狀況。</p>
							</div>
						</div>
					</div>
				</div>

				<!-- / 主內容 -->
			</div>


			<%@include file="../include/includeFooter.jsp"%>
		</div>

	</div>
	<%@include file="../include/includeFooterJs.jsp"%>
	<script src="js/index/index.js"></script>
</body>
</html>
