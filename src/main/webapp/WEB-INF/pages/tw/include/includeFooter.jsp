<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<div class="footer">
	<div class="logo">
		<img src="images/logo-itri-tw.png">
	</div>
	<div class="pull-right copy copy2">版權所有© 2018 工業技術研究院 <br />新竹縣竹東鎮中興路四段195號</div>
</div>

</div>

<div class="language">
	<a href="javascript:javascript:language.select('en');">En</a> 
	<a href="javascript:javascript:language.select('tw');" class="active">中</a>
	<!-- <a href="#en">En</a> 
	<a href="#tw" class="active">中</a> -->
	<div id="lang" style="display: none;">tw</div>
</div>
