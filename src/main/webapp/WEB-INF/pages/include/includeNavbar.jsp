<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<!doctype html>
<div class="row border-bottom">
	<nav class="navbar navbar-static-top  align-middle" role="navigation"
		style="margin-bottom: 0">

		<div class="navbar-header">
			<a class="navbar-minimalize minimalize-styl-2 btn btn-primary "
				href="#"><i class="fa fa-bars"></i> </a>
			
		</div>
		<div class="nav navbar-left">
			<h2 class="text-muted welcome-message">　Picking
					assistant</h2>
		</div>
		
		<ul class="nav navbar-top-links navbar-right">
			<li><a href="login"> <i class="fa fa-sign-out-alt"></i> Log
					out
			</a></li>
		</ul>

	</nav>
</div>

