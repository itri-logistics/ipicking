<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<!doctype html>
<script src="https://code.jquery.com/jquery-3.3.1.min.js"
	integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
	crossorigin="anonymous"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
	integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
	crossorigin="anonymous"></script>

<!-- <script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
	integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
	crossorigin="anonymous"></script> -->

<script
	src="<c:out value="${applicationScope.lib_link}"/>/js/bootstrap.min.js"></script>
<script
	src="<c:out value="${applicationScope.lib_link}"/>/js/plugins/metisMenu/jquery.metisMenu.js"></script>
<script
	src="<c:out value="${applicationScope.lib_link}"/>/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

<!-- Custom and plugin javascript -->
<script
	src="<c:out value="${applicationScope.lib_link}"/>/js/inspinia.js"></script>
<script
	src="<c:out value="${applicationScope.lib_link}"/>/js/plugins/pace/pace.min.js"></script>

<!-- Date range use moment.js same as full calendar plugin -->
<script
	src="<c:out value="${applicationScope.lib_link}"/>/js/plugins/fullcalendar/moment.min.js"></script>
<!-- Date range picker -->
<script
	src="<c:out value="${applicationScope.lib_link}"/>/js/plugins/daterangepicker/daterangepicker.js"></script>
<!-- Date picker -->
<script
	src="<c:out value="${applicationScope.lib_link}"/>/js/plugins/datapicker/bootstrap-datepicker.js"></script>

<!-- sweet alert -->
<!-- <script src="<c:out value="${applicationScope.lib_link}"/>/js/plugins/sweetalert/sweetalert.min.js"></script> -->
<!-- <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script> -->
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.26.29/sweetalert2.min.js"></script>

<script
	src="<c:out value="${applicationScope.lib_link}"/>/javascripts/handlebars-v4.0.5.js"></script>