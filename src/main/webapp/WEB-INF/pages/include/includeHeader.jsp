<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<!doctype html>
<head>
<title>動態訂單履行服務管理平台</title>
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<!-- Bootstrap CSS -->
<!-- <link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
	integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
	crossorigin="anonymous"> -->

<link rel="stylesheet"
	href="css/bootstrap.min.css" />

<!-- font awesome -->
<link rel="stylesheet"
	href="https://use.fontawesome.com/releases/v5.3.1/css/all.css"
	integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU"
	crossorigin="anonymous">

<link
	href="<c:out value="${applicationScope.lib_link}"/>/css/animate.css"
	rel="stylesheet">

<link href="<c:out value="${applicationScope.lib_link}"/>/css/plugins/datapicker/datepicker3.css" rel="stylesheet">
<link href="<c:out value="${applicationScope.lib_link}"/>/css/plugins/daterangepicker/daterangepicker-bs3.css" rel="stylesheet">
	
<!-- Sweet Alert -->
<!--<link href="<c:out value="${applicationScope.lib_link}"/>/css/plugins/sweetalert/sweetalert.css" rel="stylesheet">	-->
<link href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.26.29/sweetalert2.min.css" rel="stylesheet">


<!-- 主題樣式 -->
<!-- <link href="css/common/common.css" rel="stylesheet"> -->
<link href="<c:out value="${applicationScope.lib_link}"/>/css/style.css"
	rel="stylesheet">
</head>
