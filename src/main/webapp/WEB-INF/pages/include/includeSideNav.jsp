<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<!doctype html>
<nav class="navbar-default navbar-static-side" role="navigation"
	style="position: fixed;">
	<div class="sidebar-collapse">
		<ul class="nav metismenu" id="side-menu">
			<li class="nav-header">
				<div class="dropdown profile-element">
					<span> <img alt="image" class="img-circle"
						src="<c:out value="${applicationScope.lib_link}"/>/img/profile_small.jpg" />
					</span> <a data-toggle="dropdown" class="dropdown-toggle" href="#"> <span
						class="clear"> <span class="block m-t-xs"> <strong
								class="font-bold">User</strong>
						</span> <span class="text-muted text-xs block">ITRI <b
								class="caret"></b></span>
					</span>
					</a>
					<ul class="dropdown-menu animated fadeInRight m-t-xs">
						<li><a href="profile.html">Profile</a></li>
						<li class="divider"></li>
						<li><a href="login.html">Logout</a></li>
					</ul>
				</div>
			</li>
			<li><a href="./index"><i class="fa fa-th-large"></i> <span
					class="nav-label">Overview</span></a></li>
			<li><a href="./orderData"><i class="fa fa-clipboard"></i>
					<span class="nav-label">Upload and Manage Orders</span></a></li>
			<li><a href="./pickingOrder"><i class="fas fa-file-signature"></i>
					<span class="nav-label">Create Picking List</span></a></li>
			<li><a href="./pickingFlow"><i class="fas fa-boxes"></i>
					<span class="nav-label">Manage Picking List</span></a></li>
		</ul>

	</div>

</nav>