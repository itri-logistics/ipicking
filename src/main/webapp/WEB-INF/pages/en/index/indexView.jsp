<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!doctype html>
<html>

<%@include file="../include/includeHeader.jsp"%>
<body>
	<div id="wrapper">

		<%@include file="../include/includeNavbar.jsp"%>
		<%@include file="../include/includeSideNav.jsp"%>

		<div id="page-wrapper" class="gray-bg">
			<div class="row">
				<nav class="navbar" role="navigation" style="margin-bottom: 0">
					<div class="navbar-header">
						<a class="navbar-minimalize minimalize-styl-2 btn btn-primary"
							href="#"><i class="fa fa-bars"></i> </a>
					</div>
					<h1>Overview</h1>
				</nav>
			</div>

			<div id="main-content" class="wrapper wrapper-content">
				<!-- 主內容 -->
				<div id="banner" class="ibox float-e-margins">
					<div class="ibox-content">
						<div class="row">
							<div class="col-lg-3 col-sm-6 p-md text-center">
								<a href="./orderData"> <img src="images/ipicking_01.png"
									class="icon">
								</a>
								<h3>Upload and Manage Orders</h3>
								<p>Upload and manage your orders from your customers, and
									prepare to create the picking list in the following steps.</p>
							</div>
							<div class="col-lg-3 col-sm-6 p-md text-center">
								<a href="./pickingOrder"> <img src="images/ipicking_02.png"
									class="icon"></a>
								<h3>Create Picking List</h3>
								<p>Create the picking list from the orders.Also, the system
									will suggest the most suitable picking order for a picking
									list.</p>
							</div>
							<div class="col-lg-3 col-sm-6 p-md text-center">
								<a href="./pickingFlow"><img src="images/ipicking_03.png"
									class="icon"></a>
								<h3>Manage Picking List</h3>
								<p>In this step, you can browse the created picking list,
									and trace the picking progress if the picker using our APP
									during the picking operation.</p>
							</div>
							<div class="col-lg-3 col-sm-6 p-md text-center">
								<a target="_blank" href="http://60.248.82.82/app/pickingAssistant.apk"><img src="images/ipicking_04.png" class="icon"></a>
								<h3>Download the Picking Assistant App</h3>
								<p>This APP will guide the picker to pick the goods, tracing
									the picking progress.</p>
							</div>
						</div>
					</div>
				</div>

				<!-- / 主內容 -->
			</div>


			<%@include file="../include/includeFooter.jsp"%>
		</div>

	</div>
	<%@include file="../include/includeFooterJs.jsp"%>
	<script src="js/index/index.js"></script>
</body>
</html>
