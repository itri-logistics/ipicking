<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!doctype html>
<html>

<%@include file="../include/includeHeader.jsp"%>
<link
	href="css/plugins/dataTables/datatables.min.css"
	rel="stylesheet">
<link
	href="css/plugins/jasny/jasny-bootstrap.min.css"
	rel="stylesheet">

<body>
	<div id="wrapper">
		<%@include file="../include/includeNavbar.jsp"%>
		<%@include file="../include/includeSideNav.jsp"%>
		<div id="page-wrapper" class="gray-bg">

			<div class="row">
				<nav class="navbar" role="navigation" style="margin-bottom: 0">
					<div class="navbar-header">
						<a class="navbar-minimalize minimalize-styl-2 btn btn-primary"
							href="#"><i class="fa fa-bars"></i> </a>
					</div>
					<h1>Upload and Manage Orders</h1>
				</nav>
			</div>

			<div id="main-content" class="wrapper wrapper-content">
				<!-- 主內容 -->

				<!-- 訂單上傳 -->
				<div class="row">
					<div class="col-lg-12">
						<div id="banner" class="ibox float-e-margins">
							<div class="ibox-title">
								<h5>Upload Orders</h5>
								<div class="ibox-tools">
									<a class="collapse-link"> <i class="fa fa-chevron-up"></i>
									</a> <a class="dropdown-toggle" data-toggle="dropdown" href="#">
										<i class="fa fa-wrench"></i>
									</a>
								</div>
							</div>
							<div class="ibox-content">
								<form id="order-upload" role="form" class="form-inline">
									<div style="display: none;">
										<input name="companyAccount">
										<input name="userAccount">
									</div>
									<div class="form-group">
										<label class="form-group" for="upload-warehouse">Warehouse:
										</label> 
										<select class="form-control" id="upload-warehouse" name="warehouseId">
											
										</select>
									</div>
									<div class="form-group">
										<label class="form-group" for="upload-layout">Warehouse Layout: 
										</label> 
										<select class="form-control" id="upload-layout" name="layoutId">											
										</select>
									</div>
									<p>
										<a href="http://139.162.112.104/iDeploy/overview" target="_blank">Go to iDeploy to setup your warehouse</a>										
									</p>
									<div class="form-group">
										<div id="upload-file"
											class="fileinput fileinput-new input-group"
											data-provides="fileinput" style="margin-bottom: 0px;">
											<span class="input-group-addon btn btn-default btn-file">
												<span class="fileinput-new">Select file</span> <span
												class="fileinput-exists">Change</span> 
												<input type="file" name="file" />
											</span> <a href="#"
												class="input-group-addon btn btn-default fileinput-exists"
												data-dismiss="fileinput">Remove</a>
											<div class="form-control" data-trigger="fileinput">
												<i class="glyphicon glyphicon-file fileinput-exists"></i> <span
													class="fileinput-filename"> </span>
											</div>											
										</div>

									</div>
									<button id="upload" class="btn btn-primary"
										onclick="module.checkFile();" style="margin-bottom: 0px;">
										<i class="fa fa-upload"></i> Upload
									</button>
									<p>
										<a href="./orderData/getExample">Download example file.</a>
										<br />
										<code>We only support the xlsx file.</code>
									</p>
								</form>
								<div class="row">
									<div class="clearfix">
										<br />
									</div>
								</div>
								
							</div>

						</div>
					</div>

				</div>
				<!-- / 訂單上傳 -->

				<!-- 訂單列表 -->
				<div class="row">
					<div class="col-lg-12">
						<div id="banner" class="ibox float-e-margins">
							<div class="ibox-title">
								<h5>Order List</h5>
								<div class="ibox-tools">
									<a class="collapse-link"> <i class="fa fa-chevron-up"></i>
									</a> <a class="dropdown-toggle" data-toggle="dropdown" href="#">
										<i class="fa fa-wrench"></i>
									</a>
								</div>
							</div>
							<div class="ibox-content">
								<form id="order-filter" role="form" class="form-inline">
									<div style="display: none;">
										<input name="companyAccount">
										<input name="userAccount">
									</div>
									<div class="form-group">
										<label class="form-group" for="upload-warehouse">Warehouse:
										</label> 
										<select name="warehouseId" class="form-control" id="search-warehouse">
											<option value="all">All</option>
											<option value="2">樹林倉-C區</option>
											<option value="1">樹林倉-AB區</option>
										</select>
									</div>
									<div class="form-group">
										<!-- <select class="form-control" id="time-type">
											<option>Upload Date</option>
											<option>Order Date</option>
										</select>  -->
										<label class="form-group" for="datepicker">Order Date: </label>
										<div class="input-daterange input-group" id="datepicker">
											<span class="input-group-addon"><i
												class="fa fa-calendar-alt"></i></span> 
												<input id="start-time"
												type="text" class="input-sm form-control datepicker"
												name="from" value="" /> <span
												class="input-group-addon">to</span> 
												<input id="end-time"
												type="text" class="input-sm form-control datepicker"
												name="to" value="" />
										</div>
									</div>
									<div class="form-group">
										<label class="form-group" for="order-state">State: </label> 
										<select name="state"
											class="form-control" id="order-state">
											<option value="">All</option>
											<option value="0">Wait for Processing</option>
											<option value="1">Closed</option>
										</select>
									</div>
									<button id="get-order" class="btn btn-white" onclick="module.queryData();"
										style="margin-bottom: 0px;">
										<i class="fa fa-search"></i> Search
									</button>
								</form>

								<div class="row">
									<div class="clearfix">
										<br />
									</div>
								</div>

								<div class="table-responsive">
									<table id="order-table"
										class="table table-striped table-bordered table-hover dataTables-example">
										<thead>
											<tr>
												<th>Upload Time</th>
												<th>Warehouse</th>
												<th>Order Date</th>
												<th>#.Store</th>
												<th>Detail</th>
											</tr>
										</thead>
										<tbody id="order_list">											

										</tbody>
									</table>
									
								</div>
								<script id="order-template" type="text/x-handlebars-template">
											{{#each ds}}
											<tr>
												<td>{{uploadDateStr}}</td>
												<td>{{warehouse}}</td>
												<td>{{dateStr}}</td>
												<td>{{store}}</td>
												<td><button type="button"
														class="btn btn-outline btn-info"
														onclick="module.showDetail({{warehouseId}}, {{date}}, {{uploadDate}});">Detail</button></td>
											</tr>
											{{/each}}
								</script>
							</div>

						</div>
					</div>

				</div>
				<!-- / 訂單列表 -->

				<!-- 訂貨單明細 -->

				<div class="modal inmodal" id="order-detail" tabindex="-1"
					role="dialog" aria-hidden="true">
					<div class="modal-dialog modal-lg">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal">
									<span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
								</button>
								<h4 class="modal-title">Order Detail</h4>
								<div class="row">
									<div class="clearfix">
										<br />
									</div>
								</div>
								<div class="row">
									<div class="col-md-4 text-left">
										<i class="fas fa-warehouse"></i> Warehouse: <span id="warehouse">樹林倉-C區</span>
									</div>
									<div class="col-md-4 text-left">
										<i class="fa fa-calendar-alt"></i> Order Date: <span id="order-date">2018/09/03</span>
									</div>									
									<div class="col-md-4 text-left">
										<i class="fas fa-upload"></i> Upload Time: <span id="upload-time">2017/12/30
											08:00:00</span>
									</div>
									
								</div>
								<div class="row">
									<div class="col-md-4 text-left">
										<i class="fas fa-business-time"></i> State: <span id="state">wait
											for processing</span>
									</div>
									<div class="col-md-8 text-left">
										<i class="fas fa-store-alt"></i> Number of Stores: <span id="store-num">3</span>
									</div>
									
								</div>
							</div>
							<div class="modal-body">
								<div class="table-responsive">
									<table id="detail-table"
										class="table table-striped table-bordered table-hover dataTables-example">
										<thead>
											<tr>
												<th>Store ID</th>
												<th>Place ID</th>
												<th>Good ID</th>
												<th>qty</th>
											</tr>
										</thead>
										<tbody id="detail_list">											
											<tr>
												<td>9559</td>
												<td>72114712</td>
												<td>90</td>
											</tr>											
										</tbody>
									</table>
								</div>
								<script id="detail-template" type="text/x-handlebars-template">
											{{#each ds}}
											<tr>
												<td>{{storeId}}</td>												
												<td>{{placeId}}</td>
												<td>{{goodId}}</td>
												<td>{{qty}}</td>										
											</tr>
											{{/each}}
								</script>
							</div>
							<div class="modal-footer">
								<button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
							</div>
						</div>
					</div>
				</div>

				<!-- / 訂單明細 -->

				<!-- / 主內容 -->
			</div>

			<%@include file="../include/includeFooter.jsp"%>
		</div>

	</div>
	<%@include file="../include/includeFooterJs.jsp"%>

	<script
		src="js/plugins/dataTables/datatables.min.js"></script>
	<!-- Jasny -->
	<script
		src="js/plugins/jasny/jasny-bootstrap.min.js"></script>

	<script
		src="js/order/orderData.js?<c:out value="${applicationScope.js_version}"/>"></script>


</body>
</html>
