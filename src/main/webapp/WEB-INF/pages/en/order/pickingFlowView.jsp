<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!doctype html>
<html>

<%@include file="../include/includeHeader.jsp"%>
<link
	href="css/plugins/dataTables/datatables.min.css"
	rel="stylesheet">
<link
	href="css/plugins/jasny/jasny-bootstrap.min.css"
	rel="stylesheet">

<body>
	<div id="wrapper">
		<%@include file="../include/includeNavbar.jsp"%>
		<%@include file="../include/includeSideNav.jsp"%>
		<div id="page-wrapper" class="gray-bg">

			<div class="row">
				<nav class="navbar" role="navigation" style="margin-bottom: 0">
					<div class="navbar-header">
						<a class="navbar-minimalize minimalize-styl-2 btn btn-primary"
							href="#"><i class="fa fa-bars"></i> </a>
					</div>
					<h1>Manage Picking List</h1>
				</nav>
			</div>

			<div id="main-content" class="wrapper wrapper-content">
				<!-- 主內容 -->

				<!-- 揀貨單列表 -->
				<div class="row">
					<div class="col-lg-12">
						<div id="banner" class="ibox float-e-margins">
							<div class="ibox-title">
								<h5>Picking Lists</h5>
								<div class="ibox-tools">
									<a class="collapse-link"> <i class="fa fa-chevron-up"></i>
									</a> <a class="dropdown-toggle" data-toggle="dropdown" href="#">
										<i class="fa fa-wrench"></i>
									</a>
								</div>
							</div>
							<div class="ibox-content">
								<form id="order-filter" role="form" class="form-inline">
									<div style="display: none;">
										<input name="companyAccount">
										<input name="userAccount">
									</div>
									<div class="form-group">
										<label class="form-group" for="upload-warehouse">Warehouse:
										</label> <select class="form-control" id="upload-warehouse" name="warehouseId">
											<option value="">All</option>
											<option value="1">樹林倉-AB區</option>
											<option value="2">樹林倉-C區</option>
										</select>
									</div>
									<div class="form-group">
										<label class="form-group" for="datepicker">Picking
											Date: </label>
										<div class="input-daterange input-group" id="datepicker">
											<span class="input-group-addon"><i
												class="fa fa-calendar-alt"></i></span> 
											<input id="start-time"
												type="text" class="input-sm form-control  datepicker" name="from"/> 
												<span class="input-group-addon">to</span>
											<input id="end-time" type="text"
												class="input-sm form-control datepicker" name="to" />
											
										</div>
									</div>
									<div class="form-group">
										<label class="form-group" for="order-state">State: </label> 
										<select
											class="form-control" id="order-state" name="state">
											<option value="all">All</option>
											<option value="0">Wait for Download</option>
											<option value="1">Wait for Picking</option>
											<option value="2">On Picking</option>
											<option value="3">Finish</option>
										</select>
									</div>
									<!-- 
									<div class="form-group">
										<label class="form-group" for="order-state">Picker: </label> <select
											class="form-control" id="order-state">
											<option>All</option>
											<option>user</option>
										</select>
									</div> -->
									<button id="get-order" class="btn btn-white" style="margin-bottom: 0px;" onclick="module.queryData();">
										<i class="fa fa-search"></i> Search
									</button>
								</form>

								<div class="row">
									<div class="clearfix">
										<br />
									</div>
								</div>
								<div class="table-responsive">
									<table id="picking-table" 
										class="table table-striped table-bordered table-hover dataTables-example">
										<thead>
											<tr>
												<th>Warehouse</th>
												<th>Picking Date</th>
												<th>Store ID</th>
												<th>State</th>
												<th>Create Time</th>
												<th>APP Download Time</th>
												<th>Start Time</th>
												<th>Finish Time</th>
												<th>Picker</th>
												<th>Detail</th>
											</tr>
										</thead>
										<tbody id="picking_list">
										
											
										</tbody>
									</table>
									<script id="picking-template" type="text/x-handlebars-template">
											{{#each ds}}
											<tr>												
												<td>{{warehouse}}</td>
												<td>{{dateStr}}</td>
												<td>{{storeId}}</td>
												<td>{{state}}</td>
												<td>{{createDateStr}}</td>																								
												<td>{{downloadDateStr}}</td>																								
												<td>{{startDateStr}}</td>																								
												<td>{{finishDateStr}}</td>																								
												<td>user</td>																								
												<td><button type="button"
														class="btn btn-outline btn-info"
														onclick="module.showPickingDetail('{{warehouseId}}','{{date}}','{{createDate}}','{{storeId}}');">Detail</button></td>
											</tr>
											{{/each}}
									</script>
								</div>

							</div>

						</div>
					</div>

				</div>
				<!-- / 未截單列表 -->


				<!-- 揀貨單明細 -->

				<div class="modal inmodal" id="picking-detail" tabindex="-1"
					role="dialog" aria-hidden="true">
					<div class="modal-dialog modal-lg">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal">
									<span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
								</button>
								<h4 class="modal-title">Picking List Detail</h4>
								<div class="row">
									<div class="clearfix">
										<br />
									</div>
								</div>
								<div class="row">
									<div class="col-md-4 text-left">
										<i class="fa fa-calendar-alt"></i> Picking Date: <span id="picking-date" class="pd">2017/12/30</span>
									</div>
									<div class="col-md-8 text-left">
										<i class="fas fa-warehouse"></i> Warehouse:  <span id="picking-warehouse">樹林倉-C區</span>
									</div>
									
								</div>
								<div class="row">
									<div class="col-md-4 text-left">
										<i class="fas fa-store-alt"></i> Store ID:  <span id="store-id">2128</span>
									</div>
									<div class="col-md-4 text-left">
										<i class="fas fa-user-circle"></i> Picker:  <span id="picker">user</span>
									</div>
									<div class="col-md-4 text-left">
										<i class="fa fa-download"></i> Download Date:  <span id="download-date">No
											data</span>
									</div>
								</div>
							</div>
							<div class="modal-body">
								<div class="table-responsive">
									<table id="detail-table"
										class="table table-striped table-bordered table-hover dataTables-example">
										<thead>
											<tr>
												<th>Picking Order</th>
												<th>Place ID</th>
												<th>Good ID</th>
												<th>qty</th>
												<th>State</th>
												<th>Start Time</th>
												<th>Finish Time</th>
											</tr>
										</thead>
										<tbody id="detail-tbody">
											
										</tbody>
									</table>
								</div>
								<script id="detail-template" type="text/x-handlebars-template">
											{{#each ds}}
												<tr>
													<td>{{pickingOrder}}</td>
													<td>{{place}}</td>
													<td>{{goodId}}</td>
													<td>{{qty}}</td>
													<td>{{state}}</td>
													<td>{{startDateStr}}</td>
													<td>{{FinishDateStr}}</td>
												</tr>									
											{{/each}}
										</script>
								<h4>Warehouse Map</h4>
								<div id="map"></div>
							</div>
							<div class="modal-footer">
								<button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
							</div>
						</div>
					</div>
				</div>

				<!-- / 揀貨單明細 -->

				<!-- / 主內容 -->
			</div>

			<%@include file="../include/includeFooter.jsp"%>
		</div>

	</div>
	<%@include file="../include/includeFooterJs.jsp"%>
	<script src=https://d3js.org/d3.v5.min.js charset="utf-8"></script>
	<script
		src="js/plugins/dataTables/datatables.min.js"></script>
	<!-- Jasny -->
	<script
		src="js/plugins/jasny/jasny-bootstrap.min.js"></script>
	<script
		src="js/data/map.js?<c:out value="${applicationScope.js_version}"/>"></script>
	<script
		src="js/order/pickingFlow.js?<c:out value="${applicationScope.js_version}"/>"></script>


</body>
</html>
