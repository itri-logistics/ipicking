<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<div class="footer">
	<div class="logo">
		<img src="images/logo-itri.png">
	</div>
	<div class="pull-right copy">Copyright © 2018 Industrial
		Technology Research Institute 195, Sec. 4, Chung Hsing Rd., Chutung,
		Hsinchu, Taiwan 31057, R.O.C.</div>
</div>

</div>

<div class="language">
	<a href="javascript:javascript:language.select('en');" class="active">En</a> 
	<a href="javascript:javascript:language.select('tw');">中</a>
	<!-- <a href="#en" class="active">En</a> 
	<a href="#tw">中</a> -->
	<div id="lang" style="display: none;">en</div>
</div>
