<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<!doctype html>

<header>
	<div class="logo">
		<img src="images/logo-s.png"
			alt="Dynamic OFS"> <a href="../index"><img
			src="images/logo-ipicking.png" alt="iPicking" class="logo2"></a>
	</div>
	<div class="member">
		<img src="images/user_default.jpg">
		<p id="accountBar">
			<b>admin</b><br> ITRI
		</p>
		<button class="btn_black" onClick="memberSignOut()">Logout</button>
	</div>
</header>

<nav class="navbar-default navbar-static-side" role="navigation"
	style="position: fixed;">
	<div class="sidebar-collapse">
		<ul class="nav metismenu" id="side-menu">
			<a id="homeHref" onclick="goBack();"><img src="images/logo-s.png" width="192px"
				alt="Dynamic OPS"></a>
			<a href="./index"><img src="images/logo-ipicking.png"
				alt="iPicking" class="logo2"></a>
			<li id="navOverview"><a href="./index"><i class="fa fa-th-large"></i> <span
					class="nav-label">Overview</span></a></li>
			<li id="navOrderData"><a href="./orderData"><i class="fa fa-clipboard"></i> <span
					class="nav-label">Upload and Manage Orders</span></a></li>
			<li id="navPickingOrder"><a href="./pickingOrder"><i
					class="fas fa-file-signature"></i> <span class="nav-label">Create
						Picking List</span></a></li>
			<li id="navPickingFlow"><a href="./pickingFlow"><i class="fas fa-boxes"></i> <span
					class="nav-label">Manage Picking List</span></a></li>
		</ul>
	</div>
</nav>

