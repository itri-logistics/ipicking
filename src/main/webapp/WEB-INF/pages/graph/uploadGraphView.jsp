<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!doctype html>
<html>
<%@include file="./include/includeHeader.jsp"%>
<link href="css/graph/uploadGraph.css" rel="stylesheet" />
<body>

	<%@include file="./include/includeNavbar.jsp"%>
	<nav aria-label="breadcrumb" class="pull-right">
		<ol class="breadcrumb">
			<li class="breadcrumb-item"><a href="#">首頁</a></li>
			<li class="breadcrumb-item"><a href="#">揀貨助理</a></li>
			<li class="breadcrumb-item active" aria-current="page">上傳倉儲平面圖</li>
		</ol>
	</nav>
	<div class="container col-md-12">
		<div id="stepCreateSection" class="card">
			<div class="card-header">
				<h5 class="card-title">初始化平面圖</h5>
				<h6 class="card-subtitle mb-2 text-muted">上傳平面圖，並輸入場地尺寸</h6>
			</div>
			<div class="card-body">
				<div class="row">
					<form id="updateImgForm">
						<div class="form-row">
							<div class="input-group col">
								<label for="fileUpload" class="col-form-label">上傳場地圖片：</label>
								<div id="fileUpload" class="custom-file">
									<input id="uploadInput" name="file" type="file"
										class="custom-file-input"> <label
										class="custom-file-label btn btn-outline-secondary"
										for="uploadInput"></label>
								</div>

							</div>
							<label for="width" class="col-form-label">場地尺寸：</label>
							<div class="input-group col">
								<input id="width" type="number" class="form-control" value="3"
									placeholder="寬">
								<div class="input-group-append">
									<span class="input-group-text">公尺</span>
								</div>
							</div>
							<label for="length" class="col-form-label">X</label>
							<div class="input-group col">
								<input id="length" type="number" class="form-control" value="3"
									placeholder="長">
								<div class="input-group-append">
									<span class="input-group-text">公尺</span>
								</div>
							</div>
							<div class="col">
								<button id="createGrids" type="button" class="btn btn-primary"
									onclick="module.updateImg();">生成場地</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="clearfix">
				<br />
			</div>
		</div>
		<div id="stepAdjustSection" class="card" style="display:none;">
			<div class="card-header">
				<h5 class="card-title">平面圖調整</h5>
				<h6 class="card-subtitle mb-2 text-muted">請將平面圖縮放至合適的大小，並標示出可行走位置</h6>
			</div>
			<div class="card-body">
				<div class="row">
					<div class="col-md-1">圖片縮放：</div>
					<div class="col-md-6">
						<div class="slidecontainer">
							<input id="zoom" type="range" min="1" max="200" value="100"
								class="slider">
						</div>
					</div>
					<div class="col-md-1">
						<button id="createGrids" type="button" class="btn btn-primary"
							onclick="ui.gotoMode('GRAPH_FIXED');">調整完畢</button>
					</div>
				</div>

			</div>
		</div>
		<div class="row">
			<div class="clearfix">
				<br />
			</div>
		</div>
		<div id="stepMarkSection" class="card" style="display:none;">
			<div class="card-header">
				<h5 class="card-title">標示儲位</h5>
				<h6 class="card-subtitle mb-2 text-muted">請點選儲位的位置以標示儲位</h6>
			</div>
			<div class="card-body">
				<div class="row">
					<div class="col-md-1">
						<button id="createGrids" type="button" class="btn btn-primary"
							onclick="ui.gotoMode('STORAGE_SAVED');">標示完畢</button>
					</div>
				</div>

			</div>
		</div>
		<div class="row">
			<div class="clearfix">
				<br />
			</div>
		</div>
		<div id="stepConnetSection" class="card" style="display:none;">
			<div class="card-header">
				<h5 class="card-title">標示相鄰儲位</h5>
				<h6 class="card-subtitle mb-2 text-muted">圈選相鄰儲位的範圍，以標示每個儲位的相鄰儲位</h6>
			</div>
			<div class="card-body">
				<div class="row">
					<div class="col-md-1">
						<button id="createGrids" type="button" class="btn btn-primary"
							onclick="ui.gotoMode('CONNECT_SAVED');">標示完畢</button>
					</div>
				</div>

			</div>
		</div>
		<div class="row">
			<div class="clearfix">
				<br />
			</div>
		</div>
		<div id="storageSection" class="card" style="display:none;">			
			<div class="card-body">
				<div id="storageContainer" class="row" style="max-height:100px; overflow:auto;">
					<table id="storageTable" class="table">
						<thead>
							<tr>
								<th>儲位名稱</td>
								<th>相鄰儲位</td>
							</tr>
						</thead>
						<tbody id="storageTableBody">
						
						</tbody>
						<script id="storage-list-template" type="text/x-handlebars-template">
						{{#each ds}}
							<tr id="s-{{name}}">														
								<td>{{name}}</td>
								<td><input name="connect-{{name}}" value="{{connect}}" type="text" /></td>																	
							</tr>
						{{/each}}
						</script>			
					</table>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="clearfix">
				<br />
			</div>
		</div>
		<div id="outputSection" class="card" style="display:none;">			
			<div class="card-body">
				<div class="row" style="max-height:100px; overflow:auto;">
					<textarea class="form-control" id="output" rows="4"></textarea>					
				</div>
			</div>
		</div>
		<div class="row">
				<div class="clearfix">
					<br />
				</div>
			</div>
		<div id="graphContainer" class="row">
			<table id="graphTable" class="table">
				<tr>
					<td class="g-td"></td>
					<td class="g-td"></td>
					<td class="g-td"></td>
				</tr>
				<tr>
					<td class="g-td"></td>
					<td class="g-td"></td>
					<td class="g-td"></td>
				</tr>
				<tr>
					<td class="g-td"></td>
					<td class="g-td"></td>
					<td class="g-td"></td>
				</tr>
			</table>
			<canvas id="canvas"></canvas>
		</div>
		
		

		<!-- 批量新增儲位 -->
		<div class="modal fade" id="addStorageModal" tabindex="-1"
			role="dialog" aria-labelledby="addStorageModal" aria-hidden="true">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title" id="addStorageModalLabel">新增儲位</h5>
						<button type="button" class="close" data-dismiss="modal"
							aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<div class="modal-body">
						<form>
							<div class="form-group">
								<label for="cood" class="col-form-label">儲位座標</label> <input
									type="text" class="form-control" id="cood" name="cood" readOnly>
							</div>
							<div id="storageNameSection" class="form-group">
								<label for="storageName" class="col-form-label">儲位名稱</label> <input
									type="text" class="form-control" id="storageName"
									name="storageName">
							</div>
							<div class="form-group">
								<div class="form-check">
									<input class="form-check-input" type="checkbox" id="gridCheck">
									<label class="form-check-label" for="gridCheck">批量新增儲位</label>
								</div>
							</div>
							<div id="batchAdd" style="display: none;">
								<div class="form-group">
									<label for="numberMethod" class="col-form-label">編號方式</label>
									<div id="numberMethod">
										<div class="form-check form-check-inline">
											<input class="form-check-input" type="radio"
												name="numberMethodOptions" id="vertical" value="vertical" checked>
											<label class="form-check-label" for="vertical">垂直優先</label>
										</div>
										<div class="form-check form-check-inline">
											<input class="form-check-input" type="radio"
												name="numberMethodOptions" id="horizontal"
												value="horizontal"> <label class="form-check-label"
												for="horizontal">水平優先</label>
										</div>										
										<div class="form-check form-check-inline">
											<input class="form-check-input" type="checkbox"
												id="separatelyCheck"> <label
												class="form-check-label" for="separatelyCheck">水平、垂直分開編號</label>
										</div>
									</div>
								</div>
								<div class="form-group">
									<label for="storageNamePrefix" class="col-form-label">儲位名稱前綴字</label>
									<input type="text" class="form-control" id="storageNamePrefix"
										name="storageNamePrefix">
								</div>
								<div class="form-group">
									<label for="storageNameSuffix" class="col-form-label">儲位名稱後綴字</label>
									<input type="text" class="form-control" id="storageNameSuffix"
										name="storageNameSuffix">
								</div>
								<div id="storageNameConnectSection" class="form-group"
									style="display: none;">
									<label for="storageNameConnect" class="col-form-label">儲位名稱連接字</label>
									<input type="text" class="form-control" id="storageNameConnect"
										name="storageNameConnect">
								</div>
								<div id="startNumberSection" class="form-group">
									<label for="startNumber" class="col-form-label">起始編號</label> <input
										id="startNumber" class="form-control" type="number"
										name="startNumber" value="1" placeholder="起始編號"></input>
								</div>
								<div id="intervalNumberSection" class="form-group">
									<label for="intervalNumber" class="col-form-label">編號間隔</label>
									<input id="intervalNumber" class="form-control"
										type="number" name="intervalNumber" value="1"
										placeholder="編號間隔"></input>
								</div>
								<div id="verticalStartNumberSection" class="form-group"
									style="display: none;">
									<label for="verticalStartNumber" class="col-form-label">垂直起始編號</label>
									<input id="verticalStartNumber" class="form-control"
										type="number" name="verticalStartNumber" value="1"
										placeholder="垂直起始編號"></input>
								</div>
								<div class="form-group">
									<label for="verticalNum" class="col-form-label">垂直新增數量</label>
									<input id="verticalNum" class="form-control" type="number"
										name="verticalNum" value="1" placeholder="新增數量"></input>
									<div class="input-group-append">
										<span class="input-group-text">個</span>
									</div>
								</div>
								<div id="verticalIntervalNumberSection" class="form-group"
									style="display: none;">
									<label for="verticalIntervalNumber" class="col-form-label">垂直編號間隔</label>
									<input id="verticalIntervalNumber" class="form-control"
										type="number" name="verticalIntervalNumber" value="1"
										placeholder="垂直編號間隔"></input>
								</div>
								<div class="form-group">
									<label for="verticalInterval" class="col-form-label">間隔長度</label>
									<input id="verticalInterval" class="form-control" type="number"
										name="verticalInterval" placeholder="間隔"></input>
									<div class="input-group-append">
										<span class="input-group-text">公尺</span>
									</div>
								</div>
								<div class="form-group">
									<label for="verticalDirection" class="col-form-label">新增方向</label>
									<div id="verticalDirection">
										<div class="form-check form-check-inline">
											<input class="form-check-input" type="radio"
												name="verticalDirectionOptions" id="downward"
												value="downward" checked> <label
												class="form-check-label" for="downward">向下新增</label>
										</div>
										<div class="form-check form-check-inline">
											<input class="form-check-input" type="radio"
												name="verticalDirectionOptions" id="upward" value="upward">
											<label class="form-check-label" for="upward">向上新增</label>
										</div>
									</div>
								</div>
								<div id="horizontalStartNumberSection" class="form-group"
									style="display: none;">
									<label for="horizontalStartNumber" class="col-form-label">水平起始編號</label>
									<input id="horizontalStartNumber" class="form-control"
										type="number" name="horizontalStartNumber" value="1"
										placeholder="水平起始編號"></input>
								</div>
								<div class="form-group">
									<label for="horizontalNum" class="col-form-label">水平新增數量</label>
									<input id="horizontalNum" class="form-control" type="number"
										name="horizontalNum" value="1" placeholder="新增數量"></input>
									<div class="input-group-append">
										<span class="input-group-text">個</span>
									</div>
								</div>
								<div id="horizontalIntervalNumberSection" class="form-group"
									style="display: none;">
									<label for="horizontalIntervalNumber" class="col-form-label">水平編號間隔</label>
									<input id="horizontalIntervalNumber" class="form-control"
										type="number" name="horizontalIntervalNumber" value="1"
										placeholder="水平編號間隔"></input>
								</div>
								<div class="form-group">
									<label for="horizontalInterval" class="col-form-label">間隔長度</label>
									<input id="horizontalInterval" class="form-control"
										type="number" name="horizontalInterval" placeholder="間隔"></input>
									<div class="input-group-append">
										<span class="input-group-text">公尺</span>
									</div>
								</div>
								<div class="form-group">
									<label for="horizontalDirection" class="col-form-label">新增方向</label>
									<div id="horizontalDirection">
										<div class="form-check form-check-inline">
											<input class="form-check-input" type="radio"
												name="horizontalDirectionOptions" id="toRight"
												value="toRight" checked> <label
												class="form-check-label" for="toRight">向右新增</label>
										</div>
										<div class="form-check form-check-inline">
											<input class="form-check-input" type="radio"
												name="horizontalDirectionOptions" id="toLeft" value="toLeft">
											<label class="form-check-label" for="toLeft">向左新增</label>
										</div>
									</div>
								</div>
							</div>
						</form>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-secondary"
							data-dismiss="modal">取消</button>
						<button id="addStorageSubmit" type="button"
							class="btn btn-primary">新增儲位資訊</button>
					</div>
				</div>
			</div>
		</div>

	</div>

	<%@include file="../include/includeFooterJs.jsp"%>

	<!-- page js -->
	<script
		src="js/graph/uploadGraph.js?<c:out value="${applicationScope.js_version}"/>"></script>
</body>
</html>