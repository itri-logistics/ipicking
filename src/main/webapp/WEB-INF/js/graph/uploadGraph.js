$(document).ready(function() {
	console.debug('document ready.');

});

var module = {
	imgSrc : "",
	storage : {},
	updateImg : function() {

		var formData = new FormData(document.getElementById("updateImgForm"));
		console.debug(formData);

		$.ajax({
			url : './uploadGraph/updateImage',
			data : formData,
			dataType : 'json',
			processData : false,
			contentType : false,
			type : 'POST',
			success : function(datas) {
				console.debug(datas);
				if (datas.rs == 'error') {
					swal('更新失敗');
				} else {
					ui.createGrids('./uploadGraph/img/' + datas.content);
				}
			}
		});

		//ui.createGrids('./uploadGraph/img/20180328114637');

	},
	grouopNodes : function() {
		var ux = (ui.ex > ui.sx) ? ui.ex : ui.sx;
		var uy = (ui.ey > ui.sy) ? ui.ey : ui.sy;
		var lx = (ui.ex <= ui.sx) ? ui.ex : ui.sx;
		var ly = (ui.ey <= ui.sy) ? ui.ey : ui.sy;
		var group = [];
		for ( var key in module.storage) {
			var cood = module.storage[key].cood.split(',');

			var x = parseInt(cood[0]);
			var y = parseInt(cood[1]);
			if ((x >= lx && x <= ux) && (y >= ly && y <= uy)) {
				group.push(key);
			}
		}
		console.debug(group);
		for (var i = 0; i < group.length; i++) {
			var name = module.storage[group[i]].name;
			for (var j = 0; j < group.length; j++) {
				var _name = module.storage[group[j]].name;
				if ($.inArray(_name, module.storage[group[i]].connect) < 0) {
					module.storage[group[i]].connect.push(_name);
				}
			}
		}
		var models = [];
		for ( var key in module.storage) {
			models.push({
				name : key,
				connect : module.storage[key].connect.join(",")
			});
		}
		var source = $('#storage-list-template').html();
		var template = Handlebars.compile(source);
		var html = template({
			ds : models
		});
		$('#storageTableBody').html('').html(html);

	}
};

var ui = {
	mode : "",
	slider : $('#zoom'),
	sliderVal : 100,
	sx : -1,
	sy : -1,
	ex : -1,
	ey : -1,
	createGrids : function(src) {
		$('#stepCreateSection').hide();
		$('#stepAdjustSection').show();
		var w = $('#width').val();
		var l = $('#length').val();

		var html = '';

		for (var i = 0; i < l; i++) {
			html += '<tr>';
			for (var j = 0; j < w; j++) {
				html += '<td class="g-td"></td>';
			}
			html += '</tr>';
		}
		$('#graphTable').html('').html(html);
		canvasAction.draw(src);
	},
	gotoMode : function(mode) {
		ui.mode = mode;
		if (mode == 'GRAPH_FIXED') {
			$('#stepAdjustSection').hide();
			$('#stepMarkSection').show();

			canvasAction.removeEvent();
			$('.g-td')
					.each(
							function() {
								var y = $(this).parent().index();
								var x = $(this).index();
								$(this)
										.html(
												'<button id="'
														+ x
														+ '-'
														+ y
														+ '" type="button" class="btn btn-outline-dark btn-g"> </button>');

							});
		} else if (mode == 'STORAGE_SAVED') {
			$('#stepMarkSection').hide();
			$('#stepConnetSection').show();
			$('#storageSection').show();
			$('.btn-g').each(function() {
				if ($(this).hasClass('btn-outline-dark')) {
					$(this).addClass('btn-outline-info');
					$(this).removeClass('btn-outline-dark');
				}
			});
			var models = [];
			for ( var key in module.storage) {
				models.push({
					name : key,
					connect : module.storage[key].connect.join(",")
				});
			}
			var source = $('#storage-list-template').html();
			var template = Handlebars.compile(source);
			var html = template({
				ds : models
			});
			$('#storageTableBody').html('').html(html);
		} else if (mode == 'CONNECT_SAVED') { // 完成輸出
			var html = '';
			for ( var key in module.storage) {
				html += module.storage[key].name + ','
						+ module.storage[key].cood + ';'
						+ module.storage[key].connect.join(",") + '\n';
			}
			$('#output').html(html);
			$('#outputSection').show();
		}
	}
};

// 新增儲位
$(document).on(
		'click',
		'.btn-g',
		function() {
			if ($(this).hasClass('btn-outline-dark')) {
				var id = $(this).prop("id").split('-');
				var x = id[0];
				var y = id[1];
				$('#cood').val(x + ',' + y);
				$('#gridCheck').prop("checked", false);
				$('#storageName').show();
				$('#batchAdd').hide();
				$('#addStorageModal').modal('show');
			} else if ($(this).hasClass('btn-outline-info')
					|| $(this).hasClass('btn-info')) {
				var id = $(this).prop("id").split('-');
				if (ui.mode != "SELECTING") {
					ui.mode = "SELECTING";
					ui.sx = parseInt(id[0]);
					ui.sy = parseInt(id[1]);
				} else {
					ui.mode = "STORAGE_SAVED";
					ui.ex = parseInt(id[0]);
					ui.ey = parseInt(id[1]);
					module.grouopNodes();
					$('.btn-info').each(function() {

						$(this).addClass('btn-outline-info');
						$(this).removeClass('btn-info');
					});
				}

			}

		});



$(document).on('mouseover', '.btn-g', function() {
	if (ui.mode == "SELECTING") {
		var id = $(this).prop("id").split('-');
		ui.ex = parseInt(id[0]);
		ui.ey = parseInt(id[1]);
		var ux = (ui.ex > ui.sx) ? ui.ex : ui.sx;
		var uy = (ui.ey > ui.sy) ? ui.ey : ui.sy;
		var lx = (ui.ex <= ui.sx) ? ui.ex : ui.sx;
		var ly = (ui.ey <= ui.sy) ? ui.ey : ui.sy;
		console.debug('ux = ' + ux + ", lx = " + lx);
		console.debug('uy = ' + uy + ", ly = " + ly);
		$('.btn-g').each(function() {
			if (!$(this).hasClass('btn-success')) {
				var id = $(this).prop("id").split('-');
				var x = parseInt(id[0]);
				var y = parseInt(id[1]);
				if (x >= lx && x <= ux) {
					if (y >= ly && y <= uy) {
						$(this).addClass('btn-info');
						$(this).removeClass('btn-outline-info');
					}

				} else {
					$(this).addClass('btn-outline-info');
					$(this).removeClass('btn-info');
				}
			}

		});
	}

});

$(document).on('change', '#gridCheck', function(event) {

	if ($(this).prop('checked')) {
		$('#batchAdd').show();
		$('#storageNameSection').hide();
	} else {
		$('#storageNameSection').show();
		$('#storageName').show();
		$('#batchAdd').hide();
	}

});

$(document).on('change', '#separatelyCheck', function(event) {

	if ($(this).prop('checked')) {
		$('#verticalStartNumberSection').show();
		$('#horizontalStartNumberSection').show();
		$('#storageNameConnectSection').show();
		$('#verticalIntervalNumberSection').show();
		$('#horizontalIntervalNumberSection').show();
		$('#startNumber').hide();
		$('#intervalNumberSection').hide();
	} else {
		$('#verticalStartNumberSection').hide();
		$('#horizontalStartNumberSection').hide();
		$('#verticalIntervalNumberSection').hide();
		$('#horizontalIntervalNumberSection').hide();
		$('#intervalNumberSection').show();
		$('#storageNameConnectSection').hide();
		$('#startNumberSection').show();
	}

});

var addStorage = $(document)
		.on(
				'click',
				'#addStorageSubmit',
				function() {
					
					if ($('#gridCheck').prop("checked")) {// 批量新增
						var cood = $('#cood').val().split(',');
						var intervalNum = parseInt($('#intervalNumber').val());
						var vIntervalNum = parseInt($('#verticalIntervalNumber').val());
						var hIntervalNum = parseInt($('#horizontalIntervalNumber').val());
						var x = parseInt(cood[0]);
						var y = parseInt(cood[1]);

						var numberMethod = $(
								'input[name="numberMethodOptions"]:checked')
								.val(); // vertical / horizontal

						var vNum = parseInt($('#verticalNum').val());
						var vInterval = parseInt($('#verticalInterval').val());
						var vDir = $(
								'input[name="verticalDirectionOptions"]:checked')
								.val(); // downward / upward

						var hNum = parseInt($('#horizontalNum').val());
						var hInterval = parseInt($('#horizontalInterval').val());
						var hDir = $(
								'input[name="horizontalDirectionOptions"]:checked')
								.val(); // toLeft / toRight

						var prefix = $('#storageNamePrefix').val();
						var suffix = $('#storageNameSuffix').val();

						if ($('#separatelyCheck').prop("checked")) { // 分開編號

							var connect = $('#storageNameConnect').val();
							var vStartNum = parseInt($('#verticalStartNumber')
									.val());
							var hStartNum = parseInt($('#horizontalStartNumber')
									.val());

							if (numberMethod == 'vertical') { // 編號垂直優先
								for (var h = 0; h < hNum; h++) {

									for (var v = 0; v < vNum; v++) {
										var id = x + '-' + y;
										var name = prefix + paddingLeft(vStartNum + '',2) + connect
												+ hStartNum + suffix;
										module.storage[name] = {
											name : name,
											cood : x+','+y,
											connect : []
										};
										$('#' + id).addClass('btn-success');
										$('#' + id).removeClass(
												'btn-outline-dark');

										$('#' + id).tooltip({
											title : name,
											trigger : 'hover focus',
											placement : 'top'
										});
										if (vDir == 'downward') {
											y += vInterval;
										} else {
											y -= vInterval;
										}

										vStartNum+=vIntervalNum;
									}

									if (hDir == 'toRight') {
										x += hInterval;
									} else {
										x -= hInterval;
									}
									hStartNum+=hIntervalNum;
									vStartNum = parseInt($(
											'#verticalStartNumber').val());
									y = parseInt(cood[1]);

								}

							} else {
								for (var v = 0; v < vNum; v++) {
									for (var h = 0; h < hNum; h++) {
										var id = x + '-' + y;
										var name = prefix + paddingLeft(hStartNum + '',2) + connect
												+ vStartNum + suffix;
										module.storage[name] = {
											name : name,
											cood : x+','+y,
											connect : []
										};
										$('#' + id).addClass('btn-success');
										$('#' + id).removeClass(
												'btn-outline-dark');

										$('#' + id).tooltip({
											title : name,
											trigger : 'hover focus',
											placement : 'top'
										});

										if (hDir == 'toRight') {
											x += hInterval;
										} else {
											x -= hInterval;
										}
										hStartNum+=hIntervalNum;
									}
									if (vDir == 'downward') {
										y += vInterval;
									} else {
										y -= vInterval;
									}
									vStartNum+=vIntervalNum;
									hStartNum = parseInt($(
											'#horizontalStartNumber').val());
									x = parseInt(cood[0]);
								}
							}
						} else {

							var startNum = parseInt($('#startNumber').val());

							if (numberMethod == 'vertical') { // 編號垂直優先
								for (var h = 0; h < hNum; h++) {
									for (var v = 0; v < vNum; v++) {
										var id = x + '-' + y;
										var name = prefix + paddingLeft(startNum + '',2) + suffix;
										module.storage[name] = {
											name : name,
											cood : x+','+y,
											connect : []
										};
										$('#' + id).addClass('btn-success');
										$('#' + id).removeClass(
												'btn-outline-dark');

										$('#' + id).tooltip({
											title : name,
											trigger : 'hover focus',
											placement : 'top'
										});
										if (vDir == 'downward') {
											y += vInterval;
										} else {
											y -= vInterval;
										}

										startNum+=intervalNum;
									}

									if (hDir == 'toRight') {
										x += hInterval;
									} else {
										x -= hInterval;
									}

									y = parseInt(cood[1]);

								}

							} else {
								for (var v = 0; v < vNum; v++) {
									for (var h = 0; h < hNum; h++) {
										var id = x + '-' + y;
										var name = prefix + paddingLeft(startNum + '',2) + suffix;
										module.storage[name] = {
											name : name,
											cood : x+','+y,
											connect : []
										};
										$('#' + id).addClass('btn-success');
										$('#' + id).removeClass(
												'btn-outline-dark');

										$('#' + id).tooltip({
											title : name,
											trigger : 'hover focus',
											placement : 'top'
										});

										if (hDir == 'toRight') {
											x += hInterval;
										} else {
											x -= hInterval;
										}
										startNum+=intervalNum;
									}
									if (vDir == 'downward') {
										y += vInterval;
									} else {
										y -= vInterval;
									}
									x = parseInt(cood[0]);
								}
							}
						}

					} else {// 單一新增
						var name = $('#storageName').val();
						var cood = $('#cood').val();
						var id = cood.replace(',', '-');
						module.storage[name] = {
							name : name,
							cood : $('#cood').val(),
							connect : []
						};
						$('#' + id).addClass('btn-success');
						$('#' + id).removeClass('btn-outline-dark');

						$('#' + id).tooltip({
							title : name,
							trigger : 'hover focus',
							placement : 'top'
						});
					}

					$('#addStorageModal').modal('hide');
				});

$(document).on('input', '#zoom', function() {
	// console.debug( "slider: "+$(this).val() );
	canvasAction.zoom($(this).val() - ui.sliderVal);
	ui.sliderVal = $(this).val();
});

var canvasAction = {
	canvas : $('#canvas')[0],
	image : new Image,
	ctx : null,
	lastX : 0,
	lastY : 0,
	dragStart : null,
	dragged : null,
	draw : function(src) {
		$('#canvas').html('');
		canvasAction.canvas.width = $('#graphTable').width();
		canvasAction.canvas.height = $('#graphTable').height();

		canvasAction.lastX = canvasAction.canvas.width / 2;
		canvasAction.lastY = canvasAction.canvas.height / 2;

		canvasAction.image = new Image;

		canvasAction.ctx = canvasAction.canvas.getContext('2d');
		canvasAction.ctx.globalAlpha = 0.5;

		canvasAction.trackTransforms(canvasAction.ctx);
		canvasAction.redraw(true);

		canvasAction.image.src = src;

		canvasAction.dragStart = null;
		canvasAction.dragged = null;

		canvasAction.addEvent();

	},
	removeEvent : function() {
		canvasAction.canvas.removeEventListener('DOMMouseScroll',
				canvasAction.handleScroll);
		canvasAction.canvas.removeEventListener('mousewheel',
				canvasAction.handleScroll);
		canvasAction.canvas.removeEventListener('mousedown',
				canvasAction.mouseDownHandler);

		canvasAction.canvas.removeEventListener('mousemove',
				canvasAction.mouseMoveHandler);

		canvasAction.canvas.removeEventListener('mouseup',
				canvasAction.mouseUpHandler);
		$('#canvas').css('z-index', -1);
	},
	trackTransforms : function(ctx) {
		var svg = document.createElementNS("http://www.w3.org/2000/svg", 'svg');
		var xform = svg.createSVGMatrix();
		ctx.getTransform = function() {
			return xform;
		};

		var savedTransforms = [];
		var save = ctx.save;
		ctx.save = function() {
			savedTransforms.push(xform.translate(0, 0));
			return save.call(ctx);
		};

		var restore = ctx.restore;
		ctx.restore = function() {
			xform = savedTransforms.pop();
			return restore.call(ctx);
		};

		var scale = ctx.scale;
		ctx.scale = function(sx, sy) {
			xform = xform.scaleNonUniform(sx, sy);
			return scale.call(ctx, sx, sy);
		};

		var rotate = ctx.rotate;
		ctx.rotate = function(radians) {
			xform = xform.rotate(radians * 180 / Math.PI);
			return rotate.call(ctx, radians);
		};

		var translate = ctx.translate;
		ctx.translate = function(dx, dy) {
			xform = xform.translate(dx, dy);
			return translate.call(ctx, dx, dy);
		};

		var transform = ctx.transform;
		ctx.transform = function(a, b, c, d, e, f) {
			var m2 = svg.createSVGMatrix();
			m2.a = a;
			m2.b = b;
			m2.c = c;
			m2.d = d;
			m2.e = e;
			m2.f = f;
			xform = xform.multiply(m2);
			return transform.call(ctx, a, b, c, d, e, f);
		};

		var setTransform = ctx.setTransform;
		ctx.setTransform = function(a, b, c, d, e, f) {
			xform.a = a;
			xform.b = b;
			xform.c = c;
			xform.d = d;
			xform.e = e;
			xform.f = f;
			return setTransform.call(ctx, a, b, c, d, e, f);
		};

		var pt = svg.createSVGPoint();
		ctx.transformedPoint = function(x, y) {
			pt.x = x;
			pt.y = y;
			return pt.matrixTransform(xform.inverse());
		}
	},
	redraw : function(initial) {
		var p1 = canvasAction.ctx.transformedPoint(0, 0);
		var p2 = canvasAction.ctx.transformedPoint(canvasAction.canvas.width,
				canvasAction.canvas.height);
		canvasAction.ctx.clearRect(p1.x, p1.y, p2.x - p1.x, p2.y - p1.y);

		canvasAction.ctx.save();
		canvasAction.ctx.setTransform(1, 0, 0, 1, 0, 0);
		canvasAction.ctx.clearRect(0, 0, canvasAction.canvas.width,
				canvasAction.canvas.height);
		canvasAction.ctx.restore();
		if (initial) {
			canvasAction.image.onload = function() {
				canvasAction.ctx.drawImage(canvasAction.image, 0, 0,
						canvasAction.canvas.width, canvasAction.canvas.height);
			};
		} else {
			canvasAction.ctx.drawImage(canvasAction.image, 0, 0);
		}
	},
	addEvent : function() {

		canvasAction.canvas.addEventListener('mousedown',
				canvasAction.mouseDownHandler, false);

		canvasAction.canvas.addEventListener('mousemove',
				canvasAction.mouseMoveHandler, false);

		canvasAction.canvas.addEventListener('mouseup',
				canvasAction.mouseUpHandler, false);

		canvasAction.canvas.addEventListener('DOMMouseScroll',
				canvasAction.handleScroll, false);
		canvasAction.canvas.addEventListener('mousewheel',
				canvasAction.handleScroll, false);
		$('#canvas').css('z-index', 1);
	},
	mouseDownHandler : function(evt) {
		document.body.style.mozUserSelect = document.body.style.webkitUserSelect = document.body.style.userSelect = 'none';
		canvasAction.lastX = evt.offsetX
				|| (evt.pageX - canvasAction.canvas.offsetLeft);
		canvasAction.lastY = evt.offsetY
				|| (evt.pageY - canvasAction.canvas.offsetTop);
		canvasAction.dragStart = canvasAction.ctx.transformedPoint(
				canvasAction.lastX, canvasAction.lastY);
		canvasAction.dragged = false;
	},
	mouseMoveHandler : function(evt) {
		canvasAction.lastX = evt.offsetX
				|| (evt.pageX - canvasAction.canvas.offsetLeft);
		canvasAction.lastY = evt.offsetY
				|| (evt.pageY - canvasAction.canvas.offsetTop);
		canvasAction.dragged = true;
		if (canvasAction.dragStart) {
			var pt = canvasAction.ctx.transformedPoint(canvasAction.lastX,
					canvasAction.lastY);
			canvasAction.ctx.translate(pt.x - canvasAction.dragStart.x, pt.y
					- canvasAction.dragStart.y);
			canvasAction.redraw();
		}
	},
	mouseUpHandler : function(evt) {
		canvasAction.dragStart = null;
	},
	zoom : function(clicks) {
		console.debug(clicks);
		var scaleFactor = 1.01;
		var pt = canvasAction.ctx.transformedPoint(canvasAction.lastX,
				canvasAction.lastY);
		canvasAction.ctx.translate(pt.x, pt.y);
		var factor = Math.pow(scaleFactor, clicks);
		canvasAction.ctx.scale(factor, factor);
		canvasAction.ctx.translate(-pt.x, -pt.y);
		canvasAction.redraw();
	},
	zoomBySlider : function(factor) {
		var pt = canvasAction.ctx.transformedPoint(canvasAction.lastX,
				canvasAction.lastY);
		canvasAction.ctx.translate(pt.x, pt.y);
		canvasAction.ctx.scale(factor, factor);
		canvasAction.ctx.translate(-pt.x, -pt.y);
		canvasAction.redraw();
	},
	handleScroll : function(evt) {
		var delta = evt.wheelDelta ? evt.wheelDelta / 40
				: evt.detail ? -evt.detail : 0;
		if (delta) {
			canvasAction.zoom(delta);
		}

		return evt.preventDefault() && false;
	}
};

function paddingLeft(str,lenght){
	if(str.length >= lenght)
	return str;
	else
	return paddingLeft("0" +str,lenght);
}