var pickingOrders = {
	"L003" : ['CE104','CE108','CE116','CF103','CF104','CF106','CF111','CF114','CF117'],
	"9568" : ['CD102','CE113','CG101','CG102','CG104','CG105','CG110'],
	"9559" : ['CD102','CE113','CG101','CG105','CG112','CG110','CG104','CG102']
};

var start = {x: 6, y:-1};
var end = {x :54.5, y:-1.5};

var route = {
	"9559" : [{x:30,y:-1},{x:30,y:21},{x:38,y:21},{x:38,y:-1},{x:53.5,y:-1},{x:53.5,y:11},{x:54.5,y:11}],
	"9568" : [{x:30,y:-1},{x:30,y:21},{x:38,y:21},{x:38,y:-1},{x:53.5,y:-1},{x:53.5,y:1},{x:54.5,y:1},{x:54.5,y:3},{x:53.5,y:5},{x:54.5,y:9}],
	"L003" : [{x:38,y:-1},{x:38,y:21},{x:45.5,y:21},{x:45.5,y:17},{x:46.5, y:17},{x:46.5,y:15},{x:45.5,y:9},{x:46.5,y:7},{x:45.5,y:3},{x:46,y:-1}]	
};

var map_bak = [ {
	"warehouseId" : 2,
	"placeId" : "CA101",
	"x" : 4,
	"y" : 1,
	"orien" : 0,
	"width" : 2,
	"depth" : 2
}, {
	"warehouseId" : 2,
	"placeId" : "CA102",
	"x" : 8,
	"y" : 1,
	"orien" : 1,
	"width" : 2,
	"depth" : 2
}, {
	"warehouseId" : 2,
	"placeId" : "CA103",
	"x" : 4,
	"y" : 3,
	"orien" : 0,
	"width" : 2,
	"depth" : 2
}, {
	"warehouseId" : 2,
	"placeId" : "CA104",
	"x" : 8,
	"y" : 3,
	"orien" : 1,
	"width" : 2,
	"depth" : 2
}, {
	"warehouseId" : 2,
	"placeId" : "CA105",
	"x" : 4,
	"y" : 5,
	"orien" : 0,
	"width" : 2,
	"depth" : 2
}, {
	"warehouseId" : 2,
	"placeId" : "CA106",
	"x" : 8,
	"y" : 5,
	"orien" : 1,
	"width" : 2,
	"depth" : 2
}, {
	"warehouseId" : 2,
	"placeId" : "CA107",
	"x" : 4,
	"y" : 7,
	"orien" : 0,
	"width" : 2,
	"depth" : 2
}, {
	"warehouseId" : 2,
	"placeId" : "CA108",
	"x" : 8,
	"y" : 7,
	"orien" : 1,
	"width" : 2,
	"depth" : 2
}, {
	"warehouseId" : 2,
	"placeId" : "CA109",
	"x" : 4,
	"y" : 9,
	"orien" : 0,
	"width" : 2,
	"depth" : 2
}, {
	"warehouseId" : 2,
	"placeId" : "CA110",
	"x" : 8,
	"y" : 9,
	"orien" : 1,
	"width" : 2,
	"depth" : 2
}, {
	"warehouseId" : 2,
	"placeId" : "CA111",
	"x" : 4,
	"y" : 11,
	"orien" : 0,
	"width" : 2,
	"depth" : 2
}, {
	"warehouseId" : 2,
	"placeId" : "CA112",
	"x" : 8,
	"y" : 11,
	"orien" : 1,
	"width" : 2,
	"depth" : 2
}, {
	"warehouseId" : 2,
	"placeId" : "CA113",
	"x" : 4,
	"y" : 13,
	"orien" : 0,
	"width" : 2,
	"depth" : 2
}, {
	"warehouseId" : 2,
	"placeId" : "CA114",
	"x" : 8,
	"y" : 13,
	"orien" : 1,
	"width" : 2,
	"depth" : 2
}, {
	"warehouseId" : 2,
	"placeId" : "CA115",
	"x" : 4,
	"y" : 15,
	"orien" : 0,
	"width" : 2,
	"depth" : 2
}, {
	"warehouseId" : 2,
	"placeId" : "CA116",
	"x" : 8,
	"y" : 15,
	"orien" : 1,
	"width" : 2,
	"depth" : 2
}, {
	"warehouseId" : 2,
	"placeId" : "CA117",
	"x" : 4,
	"y" : 17,
	"orien" : 0,
	"width" : 2,
	"depth" : 2
}, {
	"warehouseId" : 2,
	"placeId" : "CA118",
	"x" : 8,
	"y" : 17,
	"orien" : 1,
	"width" : 2,
	"depth" : 2
}, {
	"warehouseId" : 2,
	"placeId" : "CA119",
	"x" : 4,
	"y" : 19,
	"orien" : 0,
	"width" : 2,
	"depth" : 2
}, {
	"warehouseId" : 2,
	"placeId" : "CA120",
	"x" : 8,
	"y" : 19,
	"orien" : 1,
	"width" : 2,
	"depth" : 2
}, {
	"warehouseId" : 2,
	"placeId" : "CA121",
	"x" : 4,
	"y" : 21,
	"orien" : 0,
	"width" : 2,
	"depth" : 2
}, {
	"warehouseId" : 2,
	"placeId" : "CA123",
	"x" : 4,
	"y" : 23,
	"orien" : 0,
	"width" : 2,
	"depth" : 2
}, {
	"warehouseId" : 2,
	"placeId" : "CB101",
	"x" : 12,
	"y" : 19,
	"orien" : 0,
	"width" : 2,
	"depth" : 2
}, {
	"warehouseId" : 2,
	"placeId" : "CB102",
	"x" : 16,
	"y" : 19,
	"orien" : 1,
	"width" : 2,
	"depth" : 2
}, {
	"warehouseId" : 2,
	"placeId" : "CB103",
	"x" : 12,
	"y" : 17,
	"orien" : 0,
	"width" : 2,
	"depth" : 2
}, {
	"warehouseId" : 2,
	"placeId" : "CB104",
	"x" : 16,
	"y" : 17,
	"orien" : 1,
	"width" : 2,
	"depth" : 2
}, {
	"warehouseId" : 2,
	"placeId" : "CB105",
	"x" : 12,
	"y" : 15,
	"orien" : 0,
	"width" : 2,
	"depth" : 2
}, {
	"warehouseId" : 2,
	"placeId" : "CB106",
	"x" : 16,
	"y" : 15,
	"orien" : 1,
	"width" : 2,
	"depth" : 2
}, {
	"warehouseId" : 2,
	"placeId" : "CB107",
	"x" : 12,
	"y" : 13,
	"orien" : 0,
	"width" : 2,
	"depth" : 2
}, {
	"warehouseId" : 2,
	"placeId" : "CB108",
	"x" : 16,
	"y" : 13,
	"orien" : 1,
	"width" : 2,
	"depth" : 2
}, {
	"warehouseId" : 2,
	"placeId" : "CB109",
	"x" : 12,
	"y" : 11,
	"orien" : 0,
	"width" : 2,
	"depth" : 2
}, {
	"warehouseId" : 2,
	"placeId" : "CB110",
	"x" : 16,
	"y" : 11,
	"orien" : 1,
	"width" : 2,
	"depth" : 2
}, {
	"warehouseId" : 2,
	"placeId" : "CB111",
	"x" : 12,
	"y" : 9,
	"orien" : 0,
	"width" : 2,
	"depth" : 2
}, {
	"warehouseId" : 2,
	"placeId" : "CB112",
	"x" : 16,
	"y" : 9,
	"orien" : 1,
	"width" : 2,
	"depth" : 2
}, {
	"warehouseId" : 2,
	"placeId" : "CB113",
	"x" : 12,
	"y" : 7,
	"orien" : 0,
	"width" : 2,
	"depth" : 2
}, {
	"warehouseId" : 2,
	"placeId" : "CB114",
	"x" : 16,
	"y" : 7,
	"orien" : 1,
	"width" : 2,
	"depth" : 2
}, {
	"warehouseId" : 2,
	"placeId" : "CB115",
	"x" : 12,
	"y" : 5,
	"orien" : 0,
	"width" : 2,
	"depth" : 2
}, {
	"warehouseId" : 2,
	"placeId" : "CB116",
	"x" : 16,
	"y" : 5,
	"orien" : 1,
	"width" : 2,
	"depth" : 2
}, {
	"warehouseId" : 2,
	"placeId" : "CB117",
	"x" : 12,
	"y" : 3,
	"orien" : 0,
	"width" : 2,
	"depth" : 2
}, {
	"warehouseId" : 2,
	"placeId" : "CB118",
	"x" : 16,
	"y" : 3,
	"orien" : 1,
	"width" : 2,
	"depth" : 2
}, {
	"warehouseId" : 2,
	"placeId" : "CB119",
	"x" : 12,
	"y" : 1,
	"orien" : 0,
	"width" : 2,
	"depth" : 2
}, {
	"warehouseId" : 2,
	"placeId" : "CB120",
	"x" : 16,
	"y" : 1,
	"orien" : 1,
	"width" : 2,
	"depth" : 2
}, {
	"warehouseId" : 2,
	"placeId" : "CB122",
	"x" : 16,
	"y" : 17,
	"orien" : 1,
	"width" : 2,
	"depth" : 2
}, {
	"warehouseId" : 2,
	"placeId" : "CB124",
	"x" : 16,
	"y" : 19,
	"orien" : 1,
	"width" : 2,
	"depth" : 2
}, {
	"warehouseId" : 2,
	"placeId" : "CC101",
	"x" : 20,
	"y" : 1,
	"orien" : 0,
	"width" : 2,
	"depth" : 2
}, {
	"warehouseId" : 2,
	"placeId" : "CC102",
	"x" : 24,
	"y" : 1,
	"orien" : 1,
	"width" : 2,
	"depth" : 2
}, {
	"warehouseId" : 2,
	"placeId" : "CC103",
	"x" : 20,
	"y" : 3,
	"orien" : 0,
	"width" : 2,
	"depth" : 2
}, {
	"warehouseId" : 2,
	"placeId" : "CC104",
	"x" : 24,
	"y" : 3,
	"orien" : 1,
	"width" : 2,
	"depth" : 2
}, {
	"warehouseId" : 2,
	"placeId" : "CC105",
	"x" : 20,
	"y" : 5,
	"orien" : 0,
	"width" : 2,
	"depth" : 2
}, {
	"warehouseId" : 2,
	"placeId" : "CC106",
	"x" : 24,
	"y" : 5,
	"orien" : 1,
	"width" : 2,
	"depth" : 2
}, {
	"warehouseId" : 2,
	"placeId" : "CC107",
	"x" : 20,
	"y" : 7,
	"orien" : 0,
	"width" : 2,
	"depth" : 2
}, {
	"warehouseId" : 2,
	"placeId" : "CC108",
	"x" : 24,
	"y" : 7,
	"orien" : 1,
	"width" : 2,
	"depth" : 2
}, {
	"warehouseId" : 2,
	"placeId" : "CC109",
	"x" : 20,
	"y" : 9,
	"orien" : 0,
	"width" : 2,
	"depth" : 2
}, {
	"warehouseId" : 2,
	"placeId" : "CC110",
	"x" : 24,
	"y" : 9,
	"orien" : 1,
	"width" : 2,
	"depth" : 2
}, {
	"warehouseId" : 2,
	"placeId" : "CC111",
	"x" : 20,
	"y" : 11,
	"orien" : 0,
	"width" : 2,
	"depth" : 2
}, {
	"warehouseId" : 2,
	"placeId" : "CC112",
	"x" : 24,
	"y" : 11,
	"orien" : 1,
	"width" : 2,
	"depth" : 2
}, {
	"warehouseId" : 2,
	"placeId" : "CC113",
	"x" : 20,
	"y" : 13,
	"orien" : 0,
	"width" : 2,
	"depth" : 2
}, {
	"warehouseId" : 2,
	"placeId" : "CC114",
	"x" : 24,
	"y" : 13,
	"orien" : 1,
	"width" : 2,
	"depth" : 2
}, {
	"warehouseId" : 2,
	"placeId" : "CC115",
	"x" : 20,
	"y" : 15,
	"orien" : 0,
	"width" : 2,
	"depth" : 2
}, {
	"warehouseId" : 2,
	"placeId" : "CC116",
	"x" : 24,
	"y" : 15,
	"orien" : 1,
	"width" : 2,
	"depth" : 2
}, {
	"warehouseId" : 2,
	"placeId" : "CC117",
	"x" : 20,
	"y" : 17,
	"orien" : 0,
	"width" : 2,
	"depth" : 2
}, {
	"warehouseId" : 2,
	"placeId" : "CC118",
	"x" : 24,
	"y" : 17,
	"orien" : 1,
	"width" : 2,
	"depth" : 2
}, {
	"warehouseId" : 2,
	"placeId" : "CC119",
	"x" : 20,
	"y" : 19,
	"orien" : 0,
	"width" : 2,
	"depth" : 2
}, {
	"warehouseId" : 2,
	"placeId" : "CC120",
	"x" : 24,
	"y" : 19,
	"orien" : 1,
	"width" : 2,
	"depth" : 2
}, {
	"warehouseId" : 2,
	"placeId" : "CC121",
	"x" : 20,
	"y" : 1,
	"orien" : 0,
	"width" : 2,
	"depth" : 2
}, {
	"warehouseId" : 2,
	"placeId" : "CC122",
	"x" : 24,
	"y" : 1,
	"orien" : 1,
	"width" : 2,
	"depth" : 2
}, {
	"warehouseId" : 2,
	"placeId" : "CC123",
	"x" : 20,
	"y" : 3,
	"orien" : 0,
	"width" : 2,
	"depth" : 2
}, {
	"warehouseId" : 2,
	"placeId" : "CC124",
	"x" : 24,
	"y" : 3,
	"orien" : 1,
	"width" : 2,
	"depth" : 2
}, {
	"warehouseId" : 2,
	"placeId" : "CC125",
	"x" : 20,
	"y" : 5,
	"orien" : 0,
	"width" : 2,
	"depth" : 2
}, {
	"warehouseId" : 2,
	"placeId" : "CC126",
	"x" : 24,
	"y" : 5,
	"orien" : 1,
	"width" : 2,
	"depth" : 2
}, {
	"warehouseId" : 2,
	"placeId" : "CC127",
	"x" : 20,
	"y" : 7,
	"orien" : 0,
	"width" : 2,
	"depth" : 2
}, {
	"warehouseId" : 2,
	"placeId" : "CC128",
	"x" : 24,
	"y" : 7,
	"orien" : 1,
	"width" : 2,
	"depth" : 2
}, {
	"warehouseId" : 2,
	"placeId" : "CC129",
	"x" : 20,
	"y" : 9,
	"orien" : 0,
	"width" : 2,
	"depth" : 2
}, {
	"warehouseId" : 2,
	"placeId" : "CC130",
	"x" : 24,
	"y" : 9,
	"orien" : 1,
	"width" : 2,
	"depth" : 2
}, {
	"warehouseId" : 2,
	"placeId" : "CC131",
	"x" : 20,
	"y" : 11,
	"orien" : 0,
	"width" : 2,
	"depth" : 2
}, {
	"warehouseId" : 2,
	"placeId" : "CC132",
	"x" : 24,
	"y" : 11,
	"orien" : 1,
	"width" : 2,
	"depth" : 2
}, {
	"warehouseId" : 2,
	"placeId" : "CC133",
	"x" : 20,
	"y" : 13,
	"orien" : 0,
	"width" : 2,
	"depth" : 2
}, {
	"warehouseId" : 2,
	"placeId" : "CC134",
	"x" : 24,
	"y" : 13,
	"orien" : 1,
	"width" : 2,
	"depth" : 2
}, {
	"warehouseId" : 2,
	"placeId" : "CC135",
	"x" : 20,
	"y" : 15,
	"orien" : 0,
	"width" : 2,
	"depth" : 2
}, {
	"warehouseId" : 2,
	"placeId" : "CC136",
	"x" : 24,
	"y" : 15,
	"orien" : 1,
	"width" : 2,
	"depth" : 2
}, {
	"warehouseId" : 2,
	"placeId" : "CC137",
	"x" : 20,
	"y" : 17,
	"orien" : 0,
	"width" : 2,
	"depth" : 2
}, {
	"warehouseId" : 2,
	"placeId" : "CC138",
	"x" : 24,
	"y" : 17,
	"orien" : 1,
	"width" : 2,
	"depth" : 2
}, {
	"warehouseId" : 2,
	"placeId" : "CC139",
	"x" : 20,
	"y" : 19,
	"orien" : 0,
	"width" : 2,
	"depth" : 2
}, {
	"warehouseId" : 2,
	"placeId" : "CC140",
	"x" : 24,
	"y" : 19,
	"orien" : 1,
	"width" : 2,
	"depth" : 2
}, {
	"warehouseId" : 2,
	"placeId" : "CD101",
	"x" : 28,
	"y" : 19,
	"orien" : 0,
	"width" : 2,
	"depth" : 2
}, {
	"warehouseId" : 2,
	"placeId" : "CD102",
	"x" : 32,
	"y" : 19,
	"orien" : 1,
	"width" : 2,
	"depth" : 2
}, {
	"warehouseId" : 2,
	"placeId" : "CD103",
	"x" : 28,
	"y" : 17,
	"orien" : 0,
	"width" : 2,
	"depth" : 2
}, {
	"warehouseId" : 2,
	"placeId" : "CD104",
	"x" : 32,
	"y" : 17,
	"orien" : 1,
	"width" : 2,
	"depth" : 2
}, {
	"warehouseId" : 2,
	"placeId" : "CD105",
	"x" : 28,
	"y" : 15,
	"orien" : 0,
	"width" : 2,
	"depth" : 2
}, {
	"warehouseId" : 2,
	"placeId" : "CD106",
	"x" : 32,
	"y" : 15,
	"orien" : 1,
	"width" : 2,
	"depth" : 2
}, {
	"warehouseId" : 2,
	"placeId" : "CD107",
	"x" : 28,
	"y" : 13,
	"orien" : 0,
	"width" : 2,
	"depth" : 2
}, {
	"warehouseId" : 2,
	"placeId" : "CD108",
	"x" : 32,
	"y" : 13,
	"orien" : 1,
	"width" : 2,
	"depth" : 2
}, {
	"warehouseId" : 2,
	"placeId" : "CD109",
	"x" : 28,
	"y" : 11,
	"orien" : 0,
	"width" : 2,
	"depth" : 2
}, {
	"warehouseId" : 2,
	"placeId" : "CD110",
	"x" : 32,
	"y" : 11,
	"orien" : 1,
	"width" : 2,
	"depth" : 2
}, {
	"warehouseId" : 2,
	"placeId" : "CD111",
	"x" : 28,
	"y" : 9,
	"orien" : 0,
	"width" : 2,
	"depth" : 2
}, {
	"warehouseId" : 2,
	"placeId" : "CD112",
	"x" : 32,
	"y" : 9,
	"orien" : 1,
	"width" : 2,
	"depth" : 2
}, {
	"warehouseId" : 2,
	"placeId" : "CD113",
	"x" : 28,
	"y" : 7,
	"orien" : 0,
	"width" : 2,
	"depth" : 2
}, {
	"warehouseId" : 2,
	"placeId" : "CD114",
	"x" : 32,
	"y" : 7,
	"orien" : 1,
	"width" : 2,
	"depth" : 2
}, {
	"warehouseId" : 2,
	"placeId" : "CD115",
	"x" : 28,
	"y" : 5,
	"orien" : 0,
	"width" : 2,
	"depth" : 2
}, {
	"warehouseId" : 2,
	"placeId" : "CD116",
	"x" : 32,
	"y" : 5,
	"orien" : 1,
	"width" : 2,
	"depth" : 2
}, {
	"warehouseId" : 2,
	"placeId" : "CD117",
	"x" : 28,
	"y" : 3,
	"orien" : 0,
	"width" : 2,
	"depth" : 2
}, {
	"warehouseId" : 2,
	"placeId" : "CD118",
	"x" : 32,
	"y" : 3,
	"orien" : 1,
	"width" : 2,
	"depth" : 2
}, {
	"warehouseId" : 2,
	"placeId" : "CD119",
	"x" : 28,
	"y" : 1,
	"orien" : 0,
	"width" : 2,
	"depth" : 2
}, {
	"warehouseId" : 2,
	"placeId" : "CD120",
	"x" : 32,
	"y" : 1,
	"orien" : 1,
	"width" : 2,
	"depth" : 2
}, {
	"warehouseId" : 2,
	"placeId" : "CD121",
	"x" : 28,
	"y" : 19,
	"orien" : 0,
	"width" : 2,
	"depth" : 2
}, {
	"warehouseId" : 2,
	"placeId" : "CD123",
	"x" : 28,
	"y" : 17,
	"orien" : 0,
	"width" : 2,
	"depth" : 2
}, {
	"warehouseId" : 2,
	"placeId" : "CD125",
	"x" : 28,
	"y" : 15,
	"orien" : 0,
	"width" : 2,
	"depth" : 2
}, {
	"warehouseId" : 2,
	"placeId" : "CD126",
	"x" : 32,
	"y" : 15,
	"orien" : 1,
	"width" : 2,
	"depth" : 2
}, {
	"warehouseId" : 2,
	"placeId" : "CD127",
	"x" : 28,
	"y" : 13,
	"orien" : 0,
	"width" : 2,
	"depth" : 2
}, {
	"warehouseId" : 2,
	"placeId" : "CD128",
	"x" : 32,
	"y" : 13,
	"orien" : 1,
	"width" : 2,
	"depth" : 2
}, {
	"warehouseId" : 2,
	"placeId" : "CD129",
	"x" : 28,
	"y" : 11,
	"orien" : 0,
	"width" : 2,
	"depth" : 2
}, {
	"warehouseId" : 2,
	"placeId" : "CD130",
	"x" : 32,
	"y" : 11,
	"orien" : 1,
	"width" : 2,
	"depth" : 2
}, {
	"warehouseId" : 2,
	"placeId" : "CD131",
	"x" : 28,
	"y" : 9,
	"orien" : 0,
	"width" : 2,
	"depth" : 2
}, {
	"warehouseId" : 2,
	"placeId" : "CD132",
	"x" : 32,
	"y" : 9,
	"orien" : 1,
	"width" : 2,
	"depth" : 2
}, {
	"warehouseId" : 2,
	"placeId" : "CD133",
	"x" : 28,
	"y" : 7,
	"orien" : 0,
	"width" : 2,
	"depth" : 2
}, {
	"warehouseId" : 2,
	"placeId" : "CD134",
	"x" : 32,
	"y" : 7,
	"orien" : 1,
	"width" : 2,
	"depth" : 2
}, {
	"warehouseId" : 2,
	"placeId" : "CD135",
	"x" : 28,
	"y" : 5,
	"orien" : 0,
	"width" : 2,
	"depth" : 2
}, {
	"warehouseId" : 2,
	"placeId" : "CD136",
	"x" : 32,
	"y" : 5,
	"orien" : 1,
	"width" : 2,
	"depth" : 2
}, {
	"warehouseId" : 2,
	"placeId" : "CD137",
	"x" : 28,
	"y" : 3,
	"orien" : 0,
	"width" : 2,
	"depth" : 2
}, {
	"warehouseId" : 2,
	"placeId" : "CD139",
	"x" : 28,
	"y" : 1,
	"orien" : 0,
	"width" : 2,
	"depth" : 2
}, {
	"warehouseId" : 2,
	"placeId" : "CE101",
	"x" : 36,
	"y" : 1,
	"orien" : 0,
	"width" : 2,
	"depth" : 2
}, {
	"warehouseId" : 2,
	"placeId" : "CE102",
	"x" : 40,
	"y" : 1,
	"orien" : 1,
	"width" : 2,
	"depth" : 2
}, {
	"warehouseId" : 2,
	"placeId" : "CE103",
	"x" : 36,
	"y" : 3,
	"orien" : 0,
	"width" : 2,
	"depth" : 2
}, {
	"warehouseId" : 2,
	"placeId" : "CE104",
	"x" : 40,
	"y" : 3,
	"orien" : 1,
	"width" : 2,
	"depth" : 2
}, {
	"warehouseId" : 2,
	"placeId" : "CE105",
	"x" : 36,
	"y" : 5,
	"orien" : 0,
	"width" : 2,
	"depth" : 2
}, {
	"warehouseId" : 2,
	"placeId" : "CE106",
	"x" : 40,
	"y" : 5,
	"orien" : 1,
	"width" : 2,
	"depth" : 2
}, {
	"warehouseId" : 2,
	"placeId" : "CE107",
	"x" : 36,
	"y" : 7,
	"orien" : 0,
	"width" : 2,
	"depth" : 2
}, {
	"warehouseId" : 2,
	"placeId" : "CE108",
	"x" : 40,
	"y" : 7,
	"orien" : 1,
	"width" : 2,
	"depth" : 2
}, {
	"warehouseId" : 2,
	"placeId" : "CE109",
	"x" : 36,
	"y" : 9,
	"orien" : 0,
	"width" : 2,
	"depth" : 2
}, {
	"warehouseId" : 2,
	"placeId" : "CE110",
	"x" : 40,
	"y" : 9,
	"orien" : 1,
	"width" : 2,
	"depth" : 2
}, {
	"warehouseId" : 2,
	"placeId" : "CE111",
	"x" : 36,
	"y" : 11,
	"orien" : 0,
	"width" : 2,
	"depth" : 2
}, {
	"warehouseId" : 2,
	"placeId" : "CE112",
	"x" : 40,
	"y" : 11,
	"orien" : 1,
	"width" : 2,
	"depth" : 2
}, {
	"warehouseId" : 2,
	"placeId" : "CE113",
	"x" : 36,
	"y" : 13,
	"orien" : 0,
	"width" : 2,
	"depth" : 2
}, {
	"warehouseId" : 2,
	"placeId" : "CE114",
	"x" : 40,
	"y" : 13,
	"orien" : 1,
	"width" : 2,
	"depth" : 2
}, {
	"warehouseId" : 2,
	"placeId" : "CE115",
	"x" : 36,
	"y" : 15,
	"orien" : 0,
	"width" : 2,
	"depth" : 2
}, {
	"warehouseId" : 2,
	"placeId" : "CE116",
	"x" : 40,
	"y" : 15,
	"orien" : 1,
	"width" : 2,
	"depth" : 2
}, {
	"warehouseId" : 2,
	"placeId" : "CE117",
	"x" : 36,
	"y" : 17,
	"orien" : 0,
	"width" : 2,
	"depth" : 2
}, {
	"warehouseId" : 2,
	"placeId" : "CE118",
	"x" : 40,
	"y" : 17,
	"orien" : 1,
	"width" : 2,
	"depth" : 2
}, {
	"warehouseId" : 2,
	"placeId" : "CE119",
	"x" : 36,
	"y" : 19,
	"orien" : 0,
	"width" : 2,
	"depth" : 2
}, {
	"warehouseId" : 2,
	"placeId" : "CE120",
	"x" : 40,
	"y" : 19,
	"orien" : 1,
	"width" : 2,
	"depth" : 2
}, {
	"warehouseId" : 2,
	"placeId" : "CF101",
	"x" : 44,
	"y" : 19,
	"orien" : 0,
	"width" : 2,
	"depth" : 2
}, {
	"warehouseId" : 2,
	"placeId" : "CF102",
	"x" : 48,
	"y" : 19,
	"orien" : 1,
	"width" : 2,
	"depth" : 2
}, {
	"warehouseId" : 2,
	"placeId" : "CF103",
	"x" : 44,
	"y" : 17,
	"orien" : 0,
	"width" : 2,
	"depth" : 2
}, {
	"warehouseId" : 2,
	"placeId" : "CF104",
	"x" : 48,
	"y" : 17,
	"orien" : 1,
	"width" : 2,
	"depth" : 2
}, {
	"warehouseId" : 2,
	"placeId" : "CF105",
	"x" : 44,
	"y" : 15,
	"orien" : 0,
	"width" : 2,
	"depth" : 2
}, {
	"warehouseId" : 2,
	"placeId" : "CF106",
	"x" : 48,
	"y" : 15,
	"orien" : 1,
	"width" : 2,
	"depth" : 2
}, {
	"warehouseId" : 2,
	"placeId" : "CF107",
	"x" : 44,
	"y" : 13,
	"orien" : 0,
	"width" : 2,
	"depth" : 2
}, {
	"warehouseId" : 2,
	"placeId" : "CF108",
	"x" : 48,
	"y" : 13,
	"orien" : 1,
	"width" : 2,
	"depth" : 2
}, {
	"warehouseId" : 2,
	"placeId" : "CF109",
	"x" : 44,
	"y" : 11,
	"orien" : 0,
	"width" : 2,
	"depth" : 2
}, {
	"warehouseId" : 2,
	"placeId" : "CF110",
	"x" : 48,
	"y" : 11,
	"orien" : 1,
	"width" : 2,
	"depth" : 2
}, {
	"warehouseId" : 2,
	"placeId" : "CF111",
	"x" : 44,
	"y" : 9,
	"orien" : 0,
	"width" : 2,
	"depth" : 2
}, {
	"warehouseId" : 2,
	"placeId" : "CF112",
	"x" : 48,
	"y" : 9,
	"orien" : 1,
	"width" : 2,
	"depth" : 2
}, {
	"warehouseId" : 2,
	"placeId" : "CF113",
	"x" : 44,
	"y" : 7,
	"orien" : 0,
	"width" : 2,
	"depth" : 2
}, {
	"warehouseId" : 2,
	"placeId" : "CF114",
	"x" : 48,
	"y" : 7,
	"orien" : 1,
	"width" : 2,
	"depth" : 2
}, {
	"warehouseId" : 2,
	"placeId" : "CF115",
	"x" : 44,
	"y" : 5,
	"orien" : 0,
	"width" : 2,
	"depth" : 2
}, {
	"warehouseId" : 2,
	"placeId" : "CF116",
	"x" : 48,
	"y" : 5,
	"orien" : 1,
	"width" : 2,
	"depth" : 2
}, {
	"warehouseId" : 2,
	"placeId" : "CF117",
	"x" : 44,
	"y" : 3,
	"orien" : 0,
	"width" : 2,
	"depth" : 2
}, {
	"warehouseId" : 2,
	"placeId" : "CF118",
	"x" : 48,
	"y" : 3,
	"orien" : 1,
	"width" : 2,
	"depth" : 2
}, {
	"warehouseId" : 2,
	"placeId" : "CF119",
	"x" : 44,
	"y" : 1,
	"orien" : 0,
	"width" : 2,
	"depth" : 2
}, {
	"warehouseId" : 2,
	"placeId" : "CF120",
	"x" : 48,
	"y" : 1,
	"orien" : 1,
	"width" : 2,
	"depth" : 2
}, {
	"warehouseId" : 2,
	"placeId" : "CG101",
	"x" : 52,
	"y" : 1,
	"orien" : 0,
	"width" : 2,
	"depth" : 2
}, {
	"warehouseId" : 2,
	"placeId" : "CG102",
	"x" : 56,
	"y" : 1,
	"orien" : 1,
	"width" : 2,
	"depth" : 2
}, {
	"warehouseId" : 2,
	"placeId" : "CG103",
	"x" : 52,
	"y" : 3,
	"orien" : 0,
	"width" : 2,
	"depth" : 2
}, {
	"warehouseId" : 2,
	"placeId" : "CG104",
	"x" : 56,
	"y" : 3,
	"orien" : 1,
	"width" : 2,
	"depth" : 2
}, {
	"warehouseId" : 2,
	"placeId" : "CG105",
	"x" : 52,
	"y" : 5,
	"orien" : 0,
	"width" : 2,
	"depth" : 2
}, {
	"warehouseId" : 2,
	"placeId" : "CG106",
	"x" : 56,
	"y" : 5,
	"orien" : 1,
	"width" : 2,
	"depth" : 2
}, {
	"warehouseId" : 2,
	"placeId" : "CG107",
	"x" : 52,
	"y" : 7,
	"orien" : 0,
	"width" : 2,
	"depth" : 2
}, {
	"warehouseId" : 2,
	"placeId" : "CG108",
	"x" : 56,
	"y" : 7,
	"orien" : 1,
	"width" : 2,
	"depth" : 2
}, {
	"warehouseId" : 2,
	"placeId" : "CG109",
	"x" : 52,
	"y" : 9,
	"orien" : 0,
	"width" : 2,
	"depth" : 2
}, {
	"warehouseId" : 2,
	"placeId" : "CG110",
	"x" : 56,
	"y" : 9,
	"orien" : 1,
	"width" : 2,
	"depth" : 2
}, {
	"warehouseId" : 2,
	"placeId" : "CG111",
	"x" : 52,
	"y" : 11,
	"orien" : 0,
	"width" : 2,
	"depth" : 2
}, {
	"warehouseId" : 2,
	"placeId" : "CG112",
	"x" : 56,
	"y" : 11,
	"orien" : 1,
	"width" : 2,
	"depth" : 2
}, {
	"warehouseId" : 2,
	"placeId" : "CG113",
	"x" : 52,
	"y" : 13,
	"orien" : 0,
	"width" : 2,
	"depth" : 2
}, {
	"warehouseId" : 2,
	"placeId" : "CG114",
	"x" : 56,
	"y" : 13,
	"orien" : 1,
	"width" : 2,
	"depth" : 2
}, {
	"warehouseId" : 2,
	"placeId" : "CG115",
	"x" : 52,
	"y" : 15,
	"orien" : 0,
	"width" : 2,
	"depth" : 2
}, {
	"warehouseId" : 2,
	"placeId" : "CG116",
	"x" : 56,
	"y" : 15,
	"orien" : 1,
	"width" : 2,
	"depth" : 2
}, {
	"warehouseId" : 2,
	"placeId" : "CG117",
	"x" : 52,
	"y" : 17,
	"orien" : 0,
	"width" : 2,
	"depth" : 2
}, {
	"warehouseId" : 2,
	"placeId" : "CG118",
	"x" : 56,
	"y" : 17,
	"orien" : 1,
	"width" : 2,
	"depth" : 2
}, {
	"warehouseId" : 2,
	"placeId" : "CG119",
	"x" : 52,
	"y" : 19,
	"orien" : 0,
	"width" : 2,
	"depth" : 2
}, {
	"warehouseId" : 2,
	"placeId" : "CG120",
	"x" : 56,
	"y" : 19,
	"orien" : 1,
	"width" : 2,
	"depth" : 2
} ];
var pickingDetail = {
	"L003" : [ {
		"date" : "20181211",
		"orderId" : "L003",
		"goodId" : "99014237",
		"place" : "CE116",
		"qty" : 1,
		"storeId" : "L003",
		"pickingOrder" : 3,
		"createDate" : "20181210174140 ",
		"downloadDate" : null,
		"startDate" : null,
		"FinishDate" : null,
		"creator" : "system",
		"pickingId" : "20181211-L003",
		"picker" : null
	}, {
		"date" : "20181211",
		"orderId" : "L003",
		"goodId" : "99013722",
		"place" : "CF111",
		"qty" : 1,
		"storeId" : "L003",
		"pickingOrder" : 7,
		"createDate" : "20181210174140 ",
		"downloadDate" : null,
		"startDate" : null,
		"FinishDate" : null,
		"creator" : "system",
		"pickingId" : "20181211-L003",
		"picker" : null
	}, {
		"date" : "20181211",
		"orderId" : "L003",
		"goodId" : "99013714",
		"place" : "CF117",
		"qty" : 1,
		"storeId" : "L003",
		"pickingOrder" : 9,
		"createDate" : "20181210174140 ",
		"downloadDate" : null,
		"startDate" : null,
		"FinishDate" : null,
		"creator" : "system",
		"pickingId" : "20181211-L003",
		"picker" : null
	}, {
		"date" : "20181211",
		"orderId" : "L003",
		"goodId" : "99013696",
		"place" : "CF106",
		"qty" : 1,
		"storeId" : "L003",
		"pickingOrder" : 6,
		"createDate" : "20181210174140 ",
		"downloadDate" : null,
		"startDate" : null,
		"FinishDate" : null,
		"creator" : "system",
		"pickingId" : "20181211-L003",
		"picker" : null
	}, {
		"date" : "20181211",
		"orderId" : "L003",
		"goodId" : "99013695",
		"place" : "CF103",
		"qty" : 1,
		"storeId" : "L003",
		"pickingOrder" : 4,
		"createDate" : "20181210174140 ",
		"downloadDate" : null,
		"startDate" : null,
		"FinishDate" : null,
		"creator" : "system",
		"pickingId" : "20181211-L003",
		"picker" : null
	}, {
		"date" : "20181211",
		"orderId" : "L003",
		"goodId" : "99013576",
		"place" : "CF114",
		"qty" : 1,
		"storeId" : "L003",
		"pickingOrder" : 8,
		"createDate" : "20181210174140 ",
		"downloadDate" : null,
		"startDate" : null,
		"FinishDate" : null,
		"creator" : "system",
		"pickingId" : "20181211-L003",
		"picker" : null
	}, {
		"date" : "20181211",
		"orderId" : "L003",
		"goodId" : "99013466",
		"place" : "CF104",
		"qty" : 1,
		"storeId" : "L003",
		"pickingOrder" : 5,
		"createDate" : "20181210174140 ",
		"downloadDate" : null,
		"startDate" : null,
		"FinishDate" : null,
		"creator" : "system",
		"pickingId" : "20181211-L003",
		"picker" : null
	}, {
		"date" : "20181211",
		"orderId" : "L003",
		"goodId" : "99013040",
		"place" : "CE108",
		"qty" : 3,
		"storeId" : "L003",
		"pickingOrder" : 2,
		"createDate" : "20181210174140 ",
		"downloadDate" : null,
		"startDate" : null,
		"FinishDate" : null,
		"creator" : "system",
		"pickingId" : "20181211-L003",
		"picker" : null
	}, {
		"date" : "20181211",
		"orderId" : "L003",
		"goodId" : "99011203",
		"place" : "CE104",
		"qty" : 150,
		"storeId" : "L003",
		"pickingOrder" : 1,
		"createDate" : "20181210174140 ",
		"downloadDate" : null,
		"startDate" : null,
		"FinishDate" : null,
		"creator" : "system",
		"pickingId" : "20181211-L003",
		"picker" : null
	} ],
	"9568" : [ {
		"date" : "20181211",
		"orderId" : "9568",
		"goodId" : "99011416",
		"place" : "CD102",
		"qty" : 1000,
		"storeId" : "9568",
		"pickingOrder" : 1,
		"createDate" : "20181210174140 ",
		"downloadDate" : null,
		"startDate" : null,
		"FinishDate" : null,
		"creator" : "system",
		"pickingId" : "20181211-9568",
		"picker" : null
	}, {
		"date" : "20181211",
		"orderId" : "9568",
		"goodId" : "99011356",
		"place" : "CG101",
		"qty" : 2000,
		"storeId" : "9568",
		"pickingOrder" : 3,
		"createDate" : "20181210174140 ",
		"downloadDate" : null,
		"startDate" : null,
		"FinishDate" : null,
		"creator" : "system",
		"pickingId" : "20181211-9568",
		"picker" : null
	}, {
		"date" : "20181211",
		"orderId" : "9568",
		"goodId" : "99011068",
		"place" : "CE113",
		"qty" : 200,
		"storeId" : "9568",
		"pickingOrder" : 2,
		"createDate" : "20181210174140 ",
		"downloadDate" : null,
		"startDate" : null,
		"FinishDate" : null,
		"creator" : "system",
		"pickingId" : "20181211-9568",
		"picker" : null
	}, {
		"date" : "20181211",
		"orderId" : "9568",
		"goodId" : "72133740",
		"place" : "CG102",
		"qty" : 4,
		"storeId" : "9568",
		"pickingOrder" : 4,
		"createDate" : "20181210174140 ",
		"downloadDate" : null,
		"startDate" : null,
		"FinishDate" : null,
		"creator" : "system",
		"pickingId" : "20181211-9568",
		"picker" : null
	}, {
		"date" : "20181211",
		"orderId" : "9568",
		"goodId" : "72124801",
		"place" : "CG104",
		"qty" : 5,
		"storeId" : "9568",
		"pickingOrder" : 5,
		"createDate" : "20181210174140 ",
		"downloadDate" : null,
		"startDate" : null,
		"FinishDate" : null,
		"creator" : "system",
		"pickingId" : "20181211-9568",
		"picker" : null
	}, {
		"date" : "20181211",
		"orderId" : "9568",
		"goodId" : "72111578",
		"place" : "CG110",
		"qty" : 12,
		"storeId" : "9568",
		"pickingOrder" : 7,
		"createDate" : "20181210174140 ",
		"downloadDate" : null,
		"startDate" : null,
		"FinishDate" : null,
		"creator" : "system",
		"pickingId" : "20181211-9568",
		"picker" : null
	}, {
		"date" : "20181211",
		"orderId" : "9568",
		"goodId" : "72109094",
		"place" : "CG105",
		"qty" : 5,
		"storeId" : "9568",
		"pickingOrder" : 6,
		"createDate" : "20181210174140 ",
		"downloadDate" : null,
		"startDate" : null,
		"FinishDate" : null,
		"creator" : "system",
		"pickingId" : "20181211-9568",
		"picker" : null
	} ],
	"9559" : [ {
		"date" : "20181211",
		"orderId" : "9559",
		"goodId" : "99011416",
		"place" : "CD102",
		"qty" : 1000,
		"storeId" : "9559",
		"pickingOrder" : 1,
		"createDate" : "20181210174140 ",
		"downloadDate" : null,
		"startDate" : null,
		"FinishDate" : null,
		"creator" : "system",
		"pickingId" : "20181211-9559",
		"picker" : null
	}, {
		"date" : "20181211",
		"orderId" : "9559",
		"goodId" : "99011356",
		"place" : "CG101",
		"qty" : 1000,
		"storeId" : "9559",
		"pickingOrder" : 3,
		"createDate" : "20181210174140 ",
		"downloadDate" : null,
		"startDate" : null,
		"FinishDate" : null,
		"creator" : "system",
		"pickingId" : "20181211-9559",
		"picker" : null
	}, {
		"date" : "20181211",
		"orderId" : "9559",
		"goodId" : "99011068",
		"place" : "CE113",
		"qty" : 100,
		"storeId" : "9559",
		"pickingOrder" : 2,
		"createDate" : "20181210174140 ",
		"downloadDate" : null,
		"startDate" : null,
		"FinishDate" : null,
		"creator" : "system",
		"pickingId" : "20181211-9559",
		"picker" : null
	}, {
		"date" : "20181211",
		"orderId" : "9559",
		"goodId" : "72133740",
		"place" : "CG102",
		"qty" : 12,
		"storeId" : "9559",
		"pickingOrder" : 8,
		"createDate" : "20181210174140 ",
		"downloadDate" : null,
		"startDate" : null,
		"FinishDate" : null,
		"creator" : "system",
		"pickingId" : "20181211-9559",
		"picker" : null
	}, {
		"date" : "20181211",
		"orderId" : "9559",
		"goodId" : "72124801",
		"place" : "CG104",
		"qty" : 2,
		"storeId" : "9559",
		"pickingOrder" : 7,
		"createDate" : "20181210174140 ",
		"downloadDate" : null,
		"startDate" : null,
		"FinishDate" : null,
		"creator" : "system",
		"pickingId" : "20181211-9559",
		"picker" : null
	}, {
		"date" : "20181211",
		"orderId" : "9559",
		"goodId" : "72114712",
		"place" : "CG112",
		"qty" : 90,
		"storeId" : "9559",
		"pickingOrder" : 5,
		"createDate" : "20181210174140 ",
		"downloadDate" : null,
		"startDate" : null,
		"FinishDate" : null,
		"creator" : "system",
		"pickingId" : "20181211-9559",
		"picker" : null
	}, {
		"date" : "20181211",
		"orderId" : "9559",
		"goodId" : "72111578",
		"place" : "CG110",
		"qty" : 24,
		"storeId" : "9559",
		"pickingOrder" : 6,
		"createDate" : "20181210174140 ",
		"downloadDate" : null,
		"startDate" : null,
		"FinishDate" : null,
		"creator" : "system",
		"pickingId" : "20181211-9559",
		"picker" : null
	}, {
		"date" : "20181211",
		"orderId" : "9559",
		"goodId" : "72109094",
		"place" : "CG105",
		"qty" : 5,
		"storeId" : "9559",
		"pickingOrder" : 4,
		"createDate" : "20181210174140 ",
		"downloadDate" : null,
		"startDate" : null,
		"FinishDate" : null,
		"creator" : "system",
		"pickingId" : "20181211-9559",
		"picker" : null
	} ]
};