var prent = null, def_logout = $.Deferred(), def_account = $.Deferred(), def_goback = $.Deferred(),def_homePath = $.Deferred();
var userAccount = null, companyAccount = null;

window.addEventListener("message", function (event) {
	prent = event;
	if (event.data.type == "load") {
		readyOn();
	} else if (event.data.type == "logout") {
		def_logout.resolve(event.data);
	} else if (event.data.type == "getCheckLogStatus") {
		var initData = event.data.data;
		userAccount = initData["account"];
		companyAccount = initData["company"];
		var barTitle = "<b>" + userAccount + "</b><br/>" + companyAccount;
		$("#accountBar").html(barTitle);
		def_account.resolve({ userAccount: userAccount, companyAccount: companyAccount });
	} else if (event.data.type == "getLanguage") {
		def_goback.resolve(event.data);
	} else if (event.data.type == "getHomePath"){
		def_homePath.resolve(event.data);
	}
})

function goBack() {
	sendMsg("getLanguage");
	sendMsg("getHomePath","ofs");
	$.when(def_goback,def_homePath).done(function (value,value2) {
		var webLanguage = value.data;
		window.location.assign(webLanguage == "eng" ? value2.data['eng'] : value2.data['tw']);
	});
}

function readyOn() {
	sendMsg("load", window.location);
	sendMsg("getCheckLogStatus");
	if (window.location.pathname.indexOf('tw') != -1) {
	    language = 1;
	    sendMsg("setWebTitle",{"type":"iPicking","language":"tw"});//更換
	}else{
	    sendMsg("setWebTitle",{"type":"iPicking","language":"eng"});//更換
	}
}

function memberSignOut() {
	sendMsg("logout");
	sendMsg("getHomePath","ofs");
	$.when(def_logout,def_homePath).done(function (value,value2) {
		var initData = value.data;
		var webLanguage = value.language;
		window.location.assign(webLanguage == "eng" ? value2.data['eng'] : value2.data['tw']);
	});
}

function sendMsg(type, data) {
	var req = { type: type, data: JSON.stringify(data) }
	prent.source.postMessage(req, prent.origin);
}

try {
	if (!(typeof parent.getCheckLogStatus === "function")) window.location.assign(homePath);
} catch (err) {

}
