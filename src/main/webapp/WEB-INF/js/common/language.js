var language = {
	now : $('#lang').html(),
	select : function(lang) {
		sessionStorage.setItem("lang", lang);
		language.now = lang;
		$.get('./changeLang?lang=' + lang, function(data) {
			$.when(def_account).done(function(value) {
				if (lang == 'tw') {
					sendMsg("setLanguage", {
						language : "tw"
					});
					sendMsg("setWebTitle", {
						"type" : "iPicking",
						"language" : "tw"
					});
				} else if (lang == 'en') {
					sendMsg("setLanguage", {
						language : "eng"
					});
					sendMsg("setWebTitle", {
						"type" : "iPicking",
						"language" : "eng"
					});
				}
				var url = window.location.href.split('?');

				window.location.replace(url[0]);

			});

		});
		// var url = window.location.href;
		// window.location.href = url+'?newLang='+lang;
	}
}

var lang = {
	all : {
		en : 'All',
		tw : '全部'
	},
	modelName : {
		en : 'Setting #',
		tw : '配置 #'
	},
	state : {
		waitForProcessing : {
			en : 'wait for processing',
			tw : '未截單'
		},
		close : {
			en : 'close',
			tw : '已截單'
		},
		closeTime : {
			en : 'close at ',
			tw : '截單時間：'
		},
		waitForDownload : {
			en : 'Wait for download',
			tw : '等待下載'
		},
		waitForPicking : {
			en : 'Wait for Picking',
			tw : '等待揀貨'
		},
		onPicking : {
			en : 'On Picking',
			tw : '揀貨中'
		},
		finish : {
			en : 'Finish',
			tw : '揀貨完成'
		},
		noData : {
			en : 'No Data',
			tw : '無資料'
		}
	},
	loadingFail : {
		title : {
			en : 'Loading Fail.',
			tw : '讀取失敗'
		}
	},
	uploading : {
		title : {
			en : 'Uploading...',
			tw : '資料上傳中...'
		},
		description : {
			en : 'Please do not close the window',
			tw : '請勿關閉視窗'
		}
	},
	uploadingFail : {
		en : 'Uploading Fail.',
		tw : '上傳失敗'
	},
	finish : {
		en : 'Finish.',
		tw : '完成'
	},
	loadingWarehouseFail : {
		title : {
			en : 'Loading Warehouse Fail.',
			tw : '讀取倉儲資料失敗'
		}
	},
	coditionError : {
		title : {
			en : 'Condition Error',
			tw : '條件錯誤'
		},
		description : {
			en : 'Start time must early than end time.',
			tw : '開始時間不能晚於結束時間'
		}
	},
	tooMuchDataError : {
		title : {
			en : 'Too much data to query.',
			tw : '讀取資料超出上限'
		},
		description : {
			en : 'Query duration must less than a week.',
			tw : '資料讀取區間請勿超過一週'
		}
	},
	orderData : {
		queryDataLoading : {
			title : {
				en : 'Loading Order Data...',
				tw : '訂單資料讀取中...'
			},
			description : {
				en : 'Please do not close the window',
				tw : '請勿關閉視窗'
			}
		},
		replaceConfirm : {
			title : {
				en : 'Do you want to replace the exist order?',
				tw : '是否取代現有訂單'
			},
			description : {
				en : 'We found some dates in the file have had orders, if you upload the file, these order will be replaced.',
				tw : '檔案內部分日期已有訂單資料，如果您上傳了這個檔案，該日期的訂單將被新資料取代。'
			},
			confirmButtonText : {
				en : 'Yes, replace it.',
				tw : '是的，取代現有訂單'
			},
			cancelButtonText : {
				en : 'No, cancel.',
				tw : '取消'
			},
			canceled : {
				title : {
					en : 'Canceled',
					tw : '操作已取消'
				},
				description : {
					en : 'Nothing happend.',
					tw : ''
				}
			}
		},
		positionConfirm : {
			title : {
				en : 'Do you want to continue with the file?',
				tw : '部分物品不存在於倉庫配置內，是否繼續？'
			},
			description : {
				en : 'We found some goods in the file cannot be found in the warehouse setting, if you upload the file, the following goods will be ignored: ',
				tw : '檔案內部分物品不存在於倉庫配置內，如果您上傳了這個檔案，訂單內的這些物品將被略過：'
			},
			confirmButtonText : {
				en : 'Yes, continue.',
				tw : '是的，繼續上傳'
			},
			cancelButtonText : {
				en : 'No, cancel.',
				tw : '取消'
			},
			canceled : {
				title : {
					en : 'Canceled',
					tw : '操作已取消'
				},
				description : {
					en : 'Nothing happend.',
					tw : ''
				}
			}
		}
	},
	pickingOrder : {
		running : {
			title : {
				en : 'Computing...',
				tw : '計算中...'
			},
			description : {
				en : 'Please do not close the window',
				tw : '請勿關閉視窗'
			}
		},
		noOrderError : {
			title : {
				en : 'No orders are selected.',
				tw : '尚未選取訂單'
			},
			description : {
				en : 'You must select orders to create the picking list.',
				tw : '您必須選取訂單已產生揀貨單'
			}
		},
		createConfirm : {
			title : {
				en : 'Warning',
				tw : '警告'
			},
			description : {
				beforeOrder : {
					en : {
						p1 : 'The picking date is before the latest order date(',
						p2 : '), do you want to continue?'
					},
					tw : {
						p1 : '揀貨日早於最後訂單日(',
						p2 : ')，是否繼續？'
					}
				},
				beforeToday : {
					en : 'The picking date is before today, do you want to continue?',
					tw : '揀貨日早於今日，是否繼續？'
				},
				finalConfirm : function(minDate, maxDate, pickingDate, count) {
					var text;
					if (language.now == 'tw') {
						text = '即將由產生揀貨單。訂單數：' + count + '，訂單日期：'
						if (minDate == maxDate) {
							text += minDate
						} else {
							text += minDate + " 至 " + maxDate
						}
						text += "，揀貨日期：" + pickingDate + "，是否繼續？";
					} else {
						text = "The Picking List will be created by the "
								+ count + " orders that the ";
						if (minDate == maxDate) {
							text += "order date is " + minDate
						} else {
							text += "duration is from " + minDate + " to "
									+ maxDate
						}
						text += ", and the picking date is " + pickingDate
								+ ", do you want to continue?";
					}
					return text;
				}
			},
			confirmButtonText : {
				en : 'Yes, continue.',
				tw : '是的，產生揀貨單'
			},
			cancelButtonText : {
				en : 'No, cancel.',
				tw : '取消'
			},
			canceled : {
				title : {
					en : 'Canceled',
					tw : '操作已取消'
				},
				description : {
					en : 'Nothing happend.',
					tw : ''
				}
			}
		}
	},
	pickingFlow : {
		queryDataLoading : {
			title : {
				en : 'Loading Order Data...',
				tw : '訂單資料讀取中...'
			},
			description : {
				en : 'Please do not close the window',
				tw : '請勿關閉視窗'
			}
		},
	},
	dataTable : {
		tw : {
			"emptyTable" : "無資料...",
			"processing" : "處理中...",
			"loadingRecords" : "載入中...",
			"lengthMenu" : "顯示 _MENU_ 項結果",
			"zeroRecords" : "沒有符合的結果",
			"info" : "顯示第 _START_ 至 _END_ 項結果，共 _TOTAL_ 項",
			"infoEmpty" : "顯示第 0 至 0 項結果，共 0 項",
			"infoFiltered" : "(從 _MAX_ 項結果中過濾)",
			"infoPostFix" : "",
			"search" : "搜尋:",
			"paginate" : {
				"first" : "第一頁",
				"previous" : "上一頁",
				"next" : "下一頁",
				"last" : "最後一頁"
			},
			"aria" : {
				"sortAscending" : ": 升冪排列",
				"sortDescending" : ": 降冪排列"
			}
		},
		en : {
			"sEmptyTable" : "No data available in table",
			"sInfo" : "Showing _START_ to _END_ of _TOTAL_ entries",
			"sInfoEmpty" : "Showing 0 to 0 of 0 entries",
			"sInfoFiltered" : "(filtered from _MAX_ total entries)",
			"sInfoPostFix" : "",
			"sInfoThousands" : ",",
			"sLengthMenu" : "Show _MENU_ entries",
			"sLoadingRecords" : "Loading...",
			"sProcessing" : "Processing...",
			"sSearch" : "Search:",
			"sZeroRecords" : "No matching records found",
			"oPaginate" : {
				"sFirst" : "First",
				"sLast" : "Last",
				"sNext" : "Next",
				"sPrevious" : "Previous"
			},
			"oAria" : {
				"sSortAscending" : ": activate to sort column ascending",
				"sSortDescending" : ": activate to sort column descending"
			}
		}
	}

};

var get_params = function(search_string) {

	var parse = function(params, pairs) {
		var pair = pairs[0];
		var parts = pair.split('=');
		var key = decodeURIComponent(parts[0]);
		var value = decodeURIComponent(parts.slice(1).join('='));

		// Handle multiple parameters of the same name
		if (typeof params[key] === "undefined") {
			params[key] = value;
		} else {
			params[key] = [].concat(params[key], value);
		}

		return pairs.length == 1 ? params : parse(params, pairs.slice(1))
	}

	// Get rid of leading ?
	return search_string.length == 0 ? {} : parse({}, search_string.substr(1)
			.split('&'));
}

$(document).ready(function() {

	var params = get_params(location.search);
	var r = params['lang'];

	if (r != null && r != sessionStorage.getItem("lang")) {
		language.select(r);
	} else {
		if (sessionStorage.getItem("lang") != null) {
			if ($('#lang').html() != sessionStorage.getItem("lang")) {
				language.select(sessionStorage.getItem("lang"));
				// language.select($('#lang').html());
			}
		} else {
			sessionStorage.setItem("lang", $('#lang').html());
			language.now = $('#lang').html();
		}
	}

	$.when(def_account).done(function(value) {
		if (language.now == 'tw') {

			sendMsg("setLanguage", {
				language : "tw"
			});
			sendMsg("setWebTitle", {
				"type" : "iPicking",
				"language" : "tw"
			});

		} else if (lang == 'en') {

			sendMsg("setLanguage", {
				language : "eng"
			});
			sendMsg("setWebTitle", {
				"type" : "iPicking",
				"language" : "eng"
			});

		}

	});

});
