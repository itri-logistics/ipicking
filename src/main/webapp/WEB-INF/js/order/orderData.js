var companyAccount = 'ITRI';
var userAccount = 'admin';
var module = {	
	warehouse : {},
	orderMap : [],
	orderIdArray : [],
	orderDetail : {},
	ideployIp:"http://172.104.67.158/",//http://ideploy.forelog.org.tw
	queryLayout : function() {
		
		var wid= $('#upload-warehouse').val();
		console.debug(wid);		
		var url = module.ideployIp+"/iDeploy/login";
		$.ajax({
			type : "POST",
			contentType : "application/json; charset=utf-8",
			url : url,
			
			data : JSON.stringify({
				"companyAccount" : companyAccount,
				"userAccount" : userAccount
			}),
			success : function(data) {
				console.debug(data);
				var token = data.token;			
					
				$.ajax({
					type : "GET",
					headers: {
				        'Authorization': 'Bearer '+token,
				    },
					url : module.ideployIp+'/iDeploy/api/jwt/warehouse/'+wid+'/model_ids',					
					success : function(models) {
						// console.debug(models);
						
						var html='';
						var list = models.model_id_list;
						$.each(list, function( key, value ) {
							  html+='<option value="'+value+'">'+lang.modelName[language.now]+(key+1)+'</option>';
						});
						$('#upload-layout').html(html);
					},
					error : function(xhr, status, error) {
						swal(lang.loadingWarehouseFail.title[language.now], error, "error");
					}
				});
				
			},
			error : function() {
				alert("權限檢查錯誤");
			}

		});
	},
	queryWarehouse : function() {
		// var companyAccount = document.getElementById('companyAccount').value;
		// var userAccount = document.getElementById('UserAccount').value;
		
		var url = module.ideployIp+"/iDeploy/login";
		$.ajax({
			type : "POST",
			contentType : "application/json; charset=utf-8",
			url : url,			
			data : JSON.stringify({
				"companyAccount" : companyAccount,
				"userAccount" : userAccount
			}),
			success : function(data) {
				console.debug(data);
				var token = data.token;
				
				$.ajax({
					type : "GET",
					headers: {
				        'Authorization': 'Bearer '+token,
				    },
					url : module.ideployIp+'/iDeploy/api/jwt/warehouse_names',					
					success : function(models) {
						// console.debug(models);
						module.warehouse = models;
						var html='';
						$.each(models, function( key, value ) {
							  html+='<option value="'+key+'">'+value+'</option>';
						});
						$('#upload-warehouse').html(html);
						$('#search-warehouse').html('<option value="">'+lang.all[language.now]+'</option>'+html);
						
						module.queryLayout();
						module.queryData();
					},
					error : function(xhr, status, error) {
						swal(lang.loadingWarehouseFail.title[language.now], error, "error");
					}
				});
				
			},
			error : function() {
				alert("權限檢查錯誤");
			}

		});
	},
	showDetail : function(warehouseId, date, updateDate) {
		var orderId = warehouseId + '-' + date + '-' + updateDate;
		var idx = $.inArray(orderId, module.orderIdArray);
		var order = module.orderMap[idx];
		$('#order-date').html(formatDate(date));
		$('#warehouse').html(module.warehouse[warehouseId]);
		$('#upload-time').html(formatDate(updateDate));
		$('#store-num').html(order.store);
		$('#state').html(order.state);
		if (ui.detailTable != null) {
			ui.detailTable.destroy();
		}
		var source = $('#detail-template').html();
		var template = Handlebars.compile(source);
		var html = template({
			ds : module.orderDetail[orderId]
		});
		$('#detail_list').html('').html(html);
		ui.detailTable = ui.bindTable("detail-table");
		$('#order-detail').modal('show');
	},
	queryData : function() {
		var diff = new Date($('#end-time').val())
				- new Date($('#start-time').val());
		if (diff < 0) {
			swal(lang.coditionError.title[language.now], lang.coditionError.description[language.now],
			"error");
			return;
		}

		if ((diff) / 86400000 > 7) {
			swal(lang.tooMuchDataError.title[language.now],
					lang.tooMuchDataError.description[language.now], "error");
			return;
		}

		var param = $('#order-filter').serialize();
		console.debug(param);
		swal({
			title : lang.orderData.queryDataLoading.title[language.now],
			text : lang.orderData.queryDataLoading.description[language.now],
			type : "info",
			allowOutsideClick : false,
			onOpen : function() {
				swal.showLoading();

				$.ajax({
							url : './orderData/getData?' + param,
							type : 'GET',
							success : function(models) {
								console.debug(models);
								module.orderIdArray = [];
								module.orderMap = [];
								module.orderDetail = {};
								if (ui.orderTable != null) {
									ui.orderTable.destroy();
								}
								var datas = models.content;
								var storeIds = [];
								if (datas.length > 0) {
									for (var i = 0; i < datas.length; i++) {
										var orderId = datas[i].warehouseId
												+ '-' + datas[i].date + '-'
												+ datas[i].uploadDate;
										var idx = $.inArray(orderId,
												module.orderIdArray);
										if (idx < 0) {
											module.orderIdArray.push(orderId);
											module.orderMap
													.push({
														warehouseId : datas[i].warehouseId,
														warehouse : module.warehouse[datas[i].warehouseId],
														date : datas[i].date,
														dateStr : formatDate(datas[i].date),
														uploadDate : datas[i].uploadDate,
														uploadDateStr : formatDate(datas[i].uploadDate),
														state : datas[i].state == 0 ? lang.state.waitForProcessing[language.now]
																: lang.state.closeTime[language.now]
																		+ formatDate(datas[i].closeDate),
														store : 0
													});
											module.orderDetail[orderId] = [];
											idx = module.orderMap.length - 1;
										}
										if($.inArray(orderId + "-"+datas[i].storeId, storeIds) < 0){
											storeIds.push(orderId + "-"+datas[i].storeId);
											module.orderMap[idx].store += 1;
										}										
										module.orderDetail[orderId].push({
											storeId : datas[i].storeId,
											orderId : datas[i].orderId,
											goodId : datas[i].goodId,
											placeId : datas[i].placeId,
											qty : datas[i].qty
										});
									}

									var source = $('#order-template').html();
									var template = Handlebars.compile(source);
									var html = template({
										ds : module.orderMap
									});
									$('#order_list').html('').html(html);
								} else {
									$('#order_list').html('');
								}

								ui.orderTable = ui.bindTable("order-table");
								swal.close();
							},
							error : function(xhr, status, error) {
								swal(lang.loadingFail[language.now], error, "error");
							}
						});
			}
		});

	},
	checkPosition : function(){
		var now = getNowTime();
		var formData = new FormData(document.getElementById("order-upload"));
		swal({
			title : lang.uploading.title[language.now],
			text :  lang.uploading.description[language.now],
			type : "warning",
			allowOutsideClick : false,
			onOpen : function() {
				swal.showLoading();

				$.ajax({
					url : './orderData/checkPosition',
					data : formData,
					dataType : 'json',
					processData : false,
					contentType : false,
					type : 'POST',
					success : function(datas) {
						console.debug(datas);
						if (datas.rs == 'error') {
							swal.hideLoading();
							swal(lang.uploadingFail[language.now], "", "error");
						} else if(datas.content.length>0 ){							
							swal.hideLoading();
							swal.fire({
								 allowOutsideClick : false,
								 title: lang.orderData.positionConfirm.title[language.now],
								 text: lang.orderData.positionConfirm.description[language.now] + datas.content,
								 type: 'warning',
								 showCancelButton: true,
								 confirmButtonText: lang.orderData.positionConfirm.confirmButtonText[language.now],
								 cancelButtonText: lang.orderData.positionConfirm.cancelButtonText[language.now],
							}).then((result) => {
								if (result.value) {
									swal({
										title : lang.uploading.title[language.now],
										text : lang.uploading.description[language.now],
										type : "warning",
										allowOutsideClick : false,
										onOpen : function(){											
											module.upload(now);											
										}
								    });
							} else if (result.dismiss === Swal.DismissReason.cancel) {
								   swal.fire(
										lang.orderData.replaceConfirm.canceled.title[language.now],
										lang.orderData.replaceConfirm.canceled.description[language.now],
										'success'
								    );
								  }
							});
						}else{
							module.upload(now);							
						}
					}
				});

			}
		});
	},
	checkFile : function(){
		var now = getNowTime();
		var formData = new FormData(document.getElementById("order-upload"));
		swal({
			title : lang.uploading.title[language.now],
			text :  lang.uploading.description[language.now],
			type : "warning",
			allowOutsideClick : false,
			onOpen : function() {
				swal.showLoading();

				$.ajax({
					url : './orderData/checkOrder',
					data : formData,
					dataType : 'json',
					processData : false,
					contentType : false,
					type : 'POST',
					success : function(datas) {
						console.debug(datas);
						if (datas.rs == 'error') {
							swal.hideLoading();
							swal(lang.uploadingFail[language.now], "", "error");
						} else if(datas.content.length>0 ){							
							swal.hideLoading();
							swal.fire({
								 allowOutsideClick : false,
								 title: lang.orderData.replaceConfirm.title[language.now],
								 text: lang.orderData.replaceConfirm.description[language.now],
								 type: 'warning',
								 showCancelButton: true,
								 confirmButtonText: lang.orderData.replaceConfirm.confirmButtonText[language.now],
								 cancelButtonText: lang.orderData.replaceConfirm.cancelButtonText[language.now],
							}).then((result) => {
								if (result.value) {
									swal({
										title : lang.uploading.title[language.now],
										text : lang.uploading.description[language.now],
										type : "warning",
										allowOutsideClick : false,
										onOpen : function(){											
											module.checkPosition();											
										}
								    });
							} else if (result.dismiss === Swal.DismissReason.cancel) {
								   swal.fire(
										lang.orderData.replaceConfirm.canceled.title[language.now],
										lang.orderData.replaceConfirm.canceled.description[language.now],
										'success'
								    );
								  }
							});
						}else{
							module.checkPosition();						
						}
					}
				});

			}
		});
	},
	upload : function(now) {
		var formData = new FormData(document.getElementById("order-upload"));

		$.ajax({
			url : './orderData/uploadOrder',
			data : formData,
			dataType : 'json',
			processData : false,
			contentType : false,
			type : 'POST',
			success : function(datas) {
				console.debug(datas);
				if (datas == 'error') {
					module.queryData();
					swal.hideLoading();
					swal(lang.uploadingFail[language.now], "", "error");
				} else {
					module.queryData();
					swal.hideLoading();
					swal(lang.finish[language.now], "", "success");
				}
			}
		});

	}

};

var ui = {
	orderTable : null,
	detailTable : null,
	bindTable : function(domId) {
		var table = $('#' + domId).DataTable({
			language: lang.dataTable[language.now],
			pageLength : 25,
			responsive : true,
			order : [ [ 0, "desc" ] ]
		});
		return table;
	}
};

$(document).on('change', '#upload-warehouse', function() {
	
	module.queryLayout();

});

$(document).ready(function() {

	$("#order-upload,#order-filter").on("submit", function(event) {
		event.preventDefault();
	});

	$('#navOrderData').addClass('active');

	$('#end-time').val(getToday());
	$('#start-time').val(getYesterday());
	$('.datepicker').datepicker({
		format : 'yyyy/mm/dd',
		todayBtn : "linked",
		calendarWeeks : true,
		autoclose : true,
		todayHighlight : true
	});

	$.when(def_account).done(function(value){	
		companyAccount = value.companyAccount;
		userAccount = value.userAccount;
		
//		console.log(value.companyAccount);//公司帳號
//		console.log(value.userAccount);//會員帳號
		
		$('input[name="companyAccount"]').val(companyAccount);
		$('input[name="userAccount"]').val(userAccount);
		
		module.queryWarehouse();
		
	});
	
//	$('input[name="companyAccount"]').val(companyAccount);;// debug
//	$('input[name="userAccount"]').val(userAccount);;// debug
	
//	module.queryWarehouse();// debug
	
	

});
