var companyAccount = 'ITRI';
var userAccount = 'admin';
var detailTable, orderTable;
var module = {
	warehouse : {},		
	orderLists : [],
	createDateList :[],
	pickingList :[],
	ideployIp:"http://172.104.67.158/",//http://ideploy.forelog.org.tw
	queryWarehouse : function() {
		// var companyAccount = document.getElementById('companyAccount').value;
		// var userAccount = document.getElementById('UserAccount').value;
		
		var url = module.ideployIp+"/iDeploy/login";
		$.ajax({
			type : "POST",
			contentType : "application/json; charset=utf-8",
			url : url,			
			data : JSON.stringify({
				"companyAccount" : companyAccount,
				"userAccount" : userAccount
			}),
			success : function(data) {
				console.debug(data);
				var token = data.token;
				
				$.ajax({
					type : "GET",
					headers: {
				        'Authorization': 'Bearer '+token,
				    },
					url : module.ideployIp+'/iDeploy/api/jwt/warehouse_names',					
					success : function(models) {
						// console.debug(models);
						var html='';
						module.warehouse = models;
						$.each(models, function( key, value ) {
							  html+='<option value="'+key+'">'+value+'</option>';
						});
						$('#upload-warehouse').html('<option value="">'+lang.all[language.now]+'</option>'+html);
						module.queryData();
					},
					error : function(xhr, status, error) {
						swal(lang.loadingWarehouseFail.title[language.now], error, "error");
					}
				});
				
			},
			error : function() {
				alert("權限檢查錯誤");
			}

		});
	},
	showPickingDetail : function(warehouseId, pickingDate, createDate, storeId) {
		var path;
		var list;
		
		var url = module.ideployIp+"/iDeploy/login";
		$.ajax({
			type : "POST",
			contentType : "application/json; charset=utf-8",
			url : url,
			
			data : JSON.stringify({
				"companyAccount" : companyAccount,
				"userAccount" : userAccount
			}),
			success : function(data) {
				console.debug(data);
				var token = data.token;			
					
				$.ajax({
					type : "GET",
					headers: {
				        'Authorization': 'Bearer '+token,
				    },
					url : module.ideployIp+'/iDeploy/api/jwt/warehouse/'+warehouseId+'/model_ids',					
					success : function(models) {
						
						module.showPickingDetailMap(warehouseId, pickingDate, createDate, storeId, models.model_id_list, models.model_id_list.length - 1);
						
					},
					error : function(xhr, status, error) {
						swal(lang.loadingWarehouseFail.title[language.now], error, "error");
					}
				});
				
			},
			error : function() {
				alert("權限檢查錯誤");
			}

		});
				
		$('#store-id').html(storeId);
		$('#picking-date').html(formatDate(pickingDate));
		$('#picking-warehouse').html(module.warehouse[warehouseId]);
		
	},
	showPickingDetailMap : function(warehouseId, pickingDate, createDate, storeId, layoutIdList, idx){		
		var layoutId = layoutIdList[idx];
		$.ajax({
			url : './map/get?warehouseId='+warehouseId+'&layoutId='+layoutId+'&companyAccount='+companyAccount+"&userAccount="+userAccount,
			type : 'GET',
			success : function(models) {
				console.debug(models);						
				if(models.length>0){
					map = models;
				
					$.ajax({
						url : './pickingFlow/getPath?warehouseId='+warehouseId+'&pickingDate=' + pickingDate+"&createDate="+createDate+"&storeId="+storeId,
						type : 'GET',
						success : function(models) {
							console.debug(models);						
							if(models.rs=='success'){
								path = models.content;
								
								$.ajax({
									url : './pickingFlow/getData?warehouseId='+warehouseId+'&pickingDate=' + pickingDate+"&createDate="+createDate+"&storeId="+storeId,
									type : 'GET',
									success : function(models) {
										console.debug(models);						
										if(models.rs=='success'){
											for(var i = 0;i<models.content.length;i++){
												models.content[i]['warehouse'] = module.warehouse[models.content[i].warehouseId];
												if(models.content[i].downloadDate==''){
													models.content[i]['state'] = lang.state.waitForDownload[language.now];
												}else if(models.content[i].startDate==''){
													models.content[i]['state'] = lang.state.waitForPicking[language.now];
												}else if(models.content[i].finishDate==''){
													models.content[i]['state'] = lang.state.onPicking[language.now];
												}else{
													models.content[i]['state'] = lang.state.finish[language.now];
												}
												
												if(models.content[i].downloadDate==''){
													models.content[i]['downloadDateStr'] = lang.state.noData[language.now];
												}else{
													models.content[i]['downloadDateStr'] = formatDate(models.content[i].downloadDate);													
												}
												if(models.content[i].startDate==''){
													models.content[i]['startDateStr'] = lang.state.noData[language.now];
												}else{
													models.content[i]['startDateStr'] = formatDate(models.content[i].startDate);
												}
												if(models.content[i].FinishDate==''){
													models.content[i]['FinishDateStr'] = lang.state.noData[language.now];
												}else{
													models.content[i]['FinishDateStr'] = formatDate(models.content[i].FinishDate);
												}
											}
											list = models.content;
											list.sort(function(a, b){
												return parseInt(a.pickingOrder) - parseInt(b.pickingOrder);
											});
											
											// draw table	
											if (ui.detailTable != null) {
												ui.detailTable.destroy();
											}					
												
											var source = $('#detail-template').html();
											var template = Handlebars.compile(source);
											var html = template({
												ds : list
											});
											$('#detail-tbody').html('').html(html);
													
											ui.detailTable = ui.bindTable("detail-table", true);							
											
											var order = [];									
											for(var i=0;i<list.length;i++){										
												order.push(list[i].place);
											}
											
											$('#download-date').html(models.content[0]['downloadDateStr']);
											ui.drawMap(path, order, map);
											$('#picking-detail').modal('show');
										}
									},
									error : function(xhr, status, error) {
										swal(lang.loadingFail[language.now], error, "error");
									}
								});
							}
						},
						error : function(xhr, status, error) {
							swal(lang.loadingFail[language.now], error, "error");
						}
					});
				
				}else if(idx >0){
					
					idx -= 1;
						
					module.showPickingDetailMap(warehouseId, pickingDate, createDate, storeId, layoutIdList, idx);
				}else{
					swal(lang.loadingFail[language.now], "empty map data", "error");
				}
			},
			error : function(xhr, status, error) {
				swal(lang.loadingFail[language.now], error, "error");
			}
		});
	},
	runCheck : function(){
		var pd = $('#picking-date-input').val();
		var pdInt = parseInt(pd.replace(/\//g,""));
		var maxDate = -1;
		var minDate = 9999999999;
		var count = 0;
		ui.orderTable.$('.check-order').each(function() {
			if ($(this).prop('checked')) {
				// check-warehouse1-date2-store3-good4
				var id = $(this).prop('id').split('-');
				
				var dateInt = parseInt(id[2]);
				
				if(dateInt < minDate){
					minDate = dateInt;
				}
				if(dateInt > maxDate){
					maxDate = dateInt;
				}
				count++;
			}
			
		});
		
		if(count<=0){
			swal(lang.pickingOrder.noOrderError.title[language.now],lang.pickingOrder.noOrderError.description[language.now],'error');
		}else if(maxDate > pdInt){
			var str = maxDate.toString();
			str = str.substr(0,4)+'/'+str.substr(4,2)+'/'+str.substr(6,2);
			swal.fire({
				 allowOutsideClick : false,
				 title: lang.pickingOrder.createConfirm.title[language.now],
				 text: lang.pickingOrder.createConfirm.description.beforeOrder[language.now].p1+str+lang.pickingOrder.createConfirm.description.beforeOrder[language.now].p2,
				 type: 'warning',
				 showCancelButton: true,
				 confirmButtonText: lang.pickingOrder.createConfirm.confirmButtonText[language.now],
				 cancelButtonText: lang.pickingOrder.createConfirm.cancelButtonText[language.now],
			}).then((result) => {
				if (result.value) {
					module.run();	
			} else if (result.dismiss === Swal.DismissReason.cancel) {
				   swal.fire(
						   lang.pickingOrder.createConfirm.canceled.title[language.now],
						   lang.pickingOrder.createConfirm.canceled.description[language.now],
						   'success'
				    );
				   
				  }
			});
		}else if(pdInt < parseInt(getToday().replace(/\//g,""))){
			swal.fire({
				 allowOutsideClick : false,
				 title: lang.pickingOrder.createConfirm.title[language.now],
				 text: lang.pickingOrder.createConfirm.description.beforeToday[language.now],
				 type: 'warning',
				 showCancelButton: true,
				 confirmButtonText: lang.pickingOrder.createConfirm.confirmButtonText[language.now],
				 cancelButtonText: lang.pickingOrder.createConfirm.cancelButtonText[language.now],
			}).then((result) => {
				if (result.value) {
					module.run();	
			} else if (result.dismiss === Swal.DismissReason.cancel) {
				   swal.fire(
						   lang.pickingOrder.createConfirm.canceled.title[language.now],
						   lang.pickingOrder.createConfirm.canceled.description[language.now],
						   'success'
				    );
				  }
			});
		}else{
			var maxStr = maxDate.toString();
			maxStr = maxStr.substr(0,4)+'/'+maxStr.substr(4,2)+'/'+maxStr.substr(6,2);
			var minStr = minDate.toString();
			minStr = minStr.substr(0,4)+'/'+minStr.substr(4,2)+'/'+minStr.substr(6,2);
			
			swal.fire({
				 allowOutsideClick : false,
				 title: lang.pickingOrder.createConfirm.title[language.now],
				 text: lang.pickingOrder.createConfirm.description.finalConfirm(minStr,maxStr,pd,count),
				 type: 'warning',
				 showCancelButton: true,
				 confirmButtonText: lang.pickingOrder.createConfirm.confirmButtonText[language.now],
				 cancelButtonText: lang.pickingOrder.createConfirm.cancelButtonText[language.now],
			}).then((result) => {
				if (result.value) {
					module.run();	
			} else if (result.dismiss === Swal.DismissReason.cancel) {
				swal.fire(
						   lang.pickingOrder.createConfirm.canceled.title[language.now],
						   lang.pickingOrder.createConfirm.canceled.description[language.now],
						   'success'
				    );
				   
				  }
			});
		}
	},
	run : function() {
		ui.showRunConfirmDialog();
		var pd = $('#picking-date-input').val();
		
		var create = [];
		var createStores = {};
		ui.orderTable.$('.check-order').each(function() {
			if ($(this).prop('checked')) {
				// check-warehouse1-date2-store3-good4
				var id = $(this).prop('id').split('-');
				var warehouseId = id[1];
				var ds = id[2]+'-'+id[3];
				
				if($.inArray(warehouseId, create) < 0){
					create.push(warehouseId);
					createStores[warehouseId] = {};
					createStores[warehouseId].dateStore = [];
				}
				if($.inArray(ds, createStores[warehouseId].dateStore) < 0){
					createStores[warehouseId].dateStore.push(ds);
					createStores[warehouseId][ds] = [];
				}
				createStores[warehouseId][ds].push(id[4]);
				console.debug(createStores);
			}
			
		});		
		
		var deferredObjectItems = [];
		module.createDateList = [];
		for(var i = 0;i<create.length;i++){
			var warehouseId = create[i];
			dsList = createStores[warehouseId].dateStore;
			var sl = '';
			for(var j = 0;j<dsList.length;j++){
				if(j>0){
					sl+=';';
				}
				sl+= dsList[j];
				var ds = dsList[j];
				for(var h = 0;h < createStores[warehouseId][ds].length;h++){
					if(h>0){
						sl+=','+createStores[warehouseId][ds][h];
					}else{
						sl+='-'+createStores[warehouseId][ds][h];
					}					
				}
			}
			var pa = {
				warehouseId : create[0],
				pickingDate : pd,
				storeList : sl,
				companyAccount: companyAccount
			};
			console.debug(pa);
			var q = $.ajax({
				url : './pickingOrder/create',
				data : pa,
				type : 'POST',
				success : function(datas) {
					// console.debug(datas);
					if (datas.rs == 'error') {
						console.debug(datas);
					} else {
						if(datas.content!=''){							
							module.createDateList.push(datas.content);
						}
					}
				}
			});
			deferredObjectItems.push(q);
		}
		
		$.when.apply($, deferredObjectItems).done(function(){	
			
			module.queryPickingList();
		});
	
	},	
	queryPickingList : function(){
		var pd = $('#picking-date-input').val();
		var deferredObjectItems = [];
		module.pickingList = [];
		for(var i = 0;i<module.createDateList.length;i++){
			var cd = module.createDateList[i];
			var q = $.ajax({
				url : './pickingOrder/getData?pickingDate=' + pd+"&createDate="+cd+"&companyAccount="+companyAccount,
				type : 'GET',
				success : function(models) {
					
					console.debug(models);						
					if(models.rs=='success'){
						for(var i = 0;i<models.content.length;i++){
							models.content[i]['warehouse'] = module.warehouse[models.content[i].warehouseId];
							models.content[i]['dateStr'] = formatDate(models.content[i].date);
							models.content[i]['createDateStr'] = formatDate(models.content[i].createDate);
							module.pickingList.push(models.content[i]);
						}
						
					}
				},
				error : function(xhr, status, error) {
					swal(lang.loadingFail[language.now], error, "error");
				}
			});
			
			deferredObjectItems.push(q);
		}
		
		$.when.apply($, deferredObjectItems).done(function(){	
			console.debug(module.pickingList);					
									
			if (ui.pickingTable != null) {
				ui.pickingTable.destroy();
			}
			
			if(module.pickingList.length>0){				
				
				var source = $('#picking-template').html();
				var template = Handlebars.compile(source);
				var html = template({
					ds : module.pickingList
				});
				$('#picking_list').html('').html(html);
			}else{
				$('#picking_list').html('');
			}		
			$('#picking-list').show();
			
			ui.pickingTable = ui.bindTable("picking-table",false);
			module.queryData();
			// swal.hideLoading();
			// swal('Finish.', "", "success");
		});
		
		
	},
	queryData : function() {
		var diff = new Date($('#end-time').val())-new Date($('#start-time').val());
		if(diff < 0){
			swal(lang.coditionError.title[language.now], lang.coditionError.description[language.now],
			"error");
			return;
		}
		
		if((diff)/86400000 >7){
			swal(lang.tooMuchDataError.title[language.now],
					lang.tooMuchDataError.description[language.now], "error");
			return;
		}
		
		var param = $('#order-filter').serialize();
		console.debug(param);
		swal({
			title : lang.orderData.queryDataLoading.title[language.now],
			text : lang.orderData.queryDataLoading.description[language.now],
			type : "info",
			allowOutsideClick : false,
			onOpen : function() {
				swal.showLoading();

				$.ajax({
					url : './orderData/getData?' + param,
					type : 'GET',
					success : function(models) {
						console.debug(models);						
						if (ui.orderTable != null) {
							ui.orderTable.destroy();
						}
						module.orderLists = models.content;
						
						if(module.orderLists.length>0){
							for(var i = 0;i<module.orderLists.length;i++){
								module.orderLists[i]['warehouse'] = module.warehouse[module.orderLists[i].warehouseId];
								models.content[i]['dateStr'] = formatDate(models.content[i].date);
								models.content[i]['uploadDateStr'] = formatDate(models.content[i].uploadDate);
							}
							
							var source = $('#order-template').html();
							var template = Handlebars.compile(source);
							var html = template({
								ds : module.orderLists
							});
							$('#order_list').html('').html(html);
						}else{
							$('#order_list').html('');
						}					
						
						ui.orderTable = ui.bindTable("order-table",false);
						swal.close();
					},
					error : function(xhr, status, error) {
						swal(lang.loadingFail[language.now], error, "error");
					}
				});
			}
			
		});

	}

};

var scale = 11;
var ui = {
	orderTable: null,
	pickingTable: null,
	detailsTable: null,
	svg : null,
	storages :[],
	bindTable : function(domId, asc) {
		var table = $('#' + domId).DataTable(
				{
					pageLength : 25,
					responsive : true,
					language: lang.dataTable[language.now],
					order : [ [ 0, asc?"asc":"desc" ] ]
				});
		return table;
	},
	showRunConfirmDialog : function() {
		swal({
			title : lang.pickingOrder.running.title[language.now],
			text : lang.pickingOrder.running.description[language.now],
			type : "warning",
			allowOutsideClick : false,
			onOpen : function() {
				swal.showLoading();
				// module.run();

			}
		});

	},
	drawMap : function(path, pickingOrder) {
		if (ui.svg != null) {
			$('#map').html('');
		}
		
		ui.storages = [];
		var margin = {
			top : -20,
			right : 0,
			bottom : 10,
			left : -20
		};
		var width = $('#picking-detail').width() - margin.left
				- margin.right;
		
		var maxY = 0;
		for (var i = 0; i < map.length; i++) {
			if(map[i].y > maxY){
				maxY = parseFloat(map[i].y) + parseFloat(map[i].width);
			}
		}
		maxY = maxY*scale + 30;
		if(maxY < 200){
			maxY = 200;
		}
		var height = maxY - margin.top - margin.bottom;
		ui.svg = d3.select('#map').append('svg').attr("width",
				width + margin.left + margin.right).attr("height",
				height + margin.top + margin.bottom).append("g").attr(
				"transform",
				"translate(" + margin.left + "," + margin.top + ")");
		
		for (var i = 0; i < map.length; i++) {
			ui.appendLine(map[i]);			
		}
		
		ui.appendRoute(path);
		
		for (var i = 0; i < map.length; i++) {			
			var order = $.inArray(map[i].placeId, pickingOrder);
			if( order>= 0){
				ui.appendDot(map[i],order+1, order == pickingOrder.length-1);
			}
		}

	},
	appendRoute : function(step){
		
		// var step = route[store];
		var x1 = start.x * scale;
		var y1 = start.y * scale;
		var x2,y2;
		for(var i=0;i<step.length;i++){
			x2 = step[i].x * scale;
			y2 = step[i].y * scale;
			ui.svg.append('line').attr('x1', x1).attr('y1', y1).attr('x2', x2)
			.attr('y2', y2).style('stroke', 'green').style('stroke-width',
					3).style('stroke-dasharray', 3);
			x1 = x2;
			y1 = y2;
		}
		
//		x2 = end.x * scale;
//		y2 = end.y * scale;
//		ui.svg.append('line').attr('x1', x1).attr('y1', y1).attr('x2', x2)
//		.attr('y2', y2).style('stroke', 'green').style('stroke-width',
//				3).style('stroke-dasharray', 3);
//		console.debug(step);
		
	},
	appendDot : function(storage, order, last){
		
		var x = parseFloat(storage.x) * scale;
		var y = parseFloat(storage.y)* scale;
		
		if(last){
			ui.svg.append('circle').attr('cx', x).attr('cy', y - 2).attr('r', 6).attr("stroke","green").attr('fill','green');
			ui.svg.append('text').attr('x', x).attr('y',  y + 2).attr("text-anchor", "middle").style(
					'fill', 'white').style('font-size', '10px').style(
					'font-weight', 'bold').text(order);
		}else if(order==1){
			ui.svg.append('circle').attr('cx', x).attr('cy', y - 2).attr('r', 6).attr("stroke","#dd6b37").attr('fill','#dd6b37');
			ui.svg.append('text').attr('x', x).attr('y',  y + 2).attr("text-anchor", "middle").style(
					'fill', 'white').style('font-size', '10px').style(
					'font-weight', 'bold').text(order);
		}else{
			ui.svg.append('circle').attr('cx', x).attr('cy', y - 2).attr('r', 6).attr("stroke","#734aa2").attr('fill','#734aa2');
			ui.svg.append('text').attr('x', x).attr('y',  y + 2).attr("text-anchor", "middle").style(
					'fill', 'white').style('font-size', '10px').style(
					'font-weight', 'bold').text(order);
		}
		
	},
	appendLine : function(storage) {
		
		var orien = storage.orien == "0" ? -1 : 1; // 0:朝右,1:朝左
		var x = parseFloat(storage.x);
		var y = parseFloat(storage.y);
		var depth = parseFloat(storage.depth);
		var width = parseFloat(storage.width);
		var x1 = x * scale;
		var x2 = (x + depth * orien) * scale;
		var y1 = (y + width / 2) * scale;
		var y2 = (y - width / 2) * scale;		
		
		ui.svg.append('line').attr('x1', x1).attr('y1', y1).attr('x2', x2)
				.attr('y2', y1).style('stroke', 'black').style('stroke-width',
						2);
		ui.svg.append('line').attr('x1', x2).attr('y1', y1).attr('x2', x2)
				.attr('y2', y2).style('stroke', 'black').style('stroke-width',
						2);
		ui.svg.append('line').attr('x1', x1).attr('y1', y2).attr('x2', x2)
				.attr('y2', y2).style('stroke', 'black').style('stroke-width',
						2);

		if($.inArray(storage.x+','+storage.y, ui.storages)<0){
			ui.svg.append('text').attr('x', (x - depth/2 ) * scale + 5*orien ).attr('y',  (y + width / 2 )* scale - 2).style(
					'fill', 'steelblue').style('font-size', '10px').style(
					'font-weight', 'bold').text(storage.placeId);
		}else{
			ui.svg.append('text').attr('x', (x - depth/2 ) * scale + 5*orien ).attr('y',  y * scale -2).style(
					'fill', 'red').style('font-size', '10px').style(
					'font-weight', 'bold').text(storage.placeId);
		}
		
		
		
		ui.storages.push(storage.x+','+storage.y);
		
	}
};

$(document).on('change', '#check-all-orders', function() {
	
	ui.orderTable.$('.check-order').each(function() {
		
		$(this).prop('checked', $('#check-all-orders').prop('checked'));
	});
});

$(document).on('change', '.check-order', function() {
	var all = true;

	$('.check-order').each(function() {
		if (!$(this).prop('checked')) {
			all = false;
		}
	});

	$('#check-all-orders').prop('checked', all)

});

$(document)
		.ready(
				function() {					
					$('#order-table').show();
					$('#navPickingOrder').addClass('active');
					$("#create-form,#order-filter").on("submit", function(event) {
						event.preventDefault();
					});

					$('.datepicker').val(getToday());
					$('#start-time').val(getYesterday());
					$('.datepicker').datepicker({
						format : 'yyyy/mm/dd',
						todayBtn : "linked",
						keyboardNavigation : false,
						forceParse : false,
						calendarWeeks : true,
						autoclose : true,
						todayHighlight : true
					});
					
					$.when(def_account).done(function(value){	
						companyAccount = value.companyAccount;
						userAccount = value.userAccount;						
						$('input[name="companyAccount"]').val(companyAccount);
						$('input[name="userAccount"]').val(userAccount);
						module.queryWarehouse();
					});
					
//					module.queryWarehouse();	// debug					

				});