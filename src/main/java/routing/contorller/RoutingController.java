package routing.contorller;

import param.RsMsg;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import routing.service.RoutingService;


/***
 * iDeploy的接口
 * 
 * 套件:routing
 * 
 * @author ATone create by 2017/06/12
 **/

@Controller
@RequestMapping(value = "/routing")
public class RoutingController{

	// 截單用，產生揀貨單
	@RequestMapping(value = "/cal", method = RequestMethod.POST)
	public @ResponseBody Object createPickingOrder(
			@RequestParam(value = "warehouse", defaultValue = "") String warehouse,
			@RequestParam(value = "rackLine", defaultValue = "") String rackLine,
			@RequestParam(value = "method", defaultValue = "Z") String method,
			@RequestParam(value = "orderList", defaultValue = "") String orderList,
			@RequestParam(value = "companyAccount", defaultValue = "") String company,
			@RequestParam(value = "userAccount", defaultValue = "") String user, ModelMap model) {

		System.out.println("warehouse:"+warehouse);
		System.out.println("rackLine:"+rackLine);
		// orderList: [揀貨日期],[門市代號],[商品編號],[出貨量];[揀貨日期],[門市代號],[商品編號],[出貨量]...
		// warehouse: [揀位],[x],[y],[width],[depth],[orien],[商品編號];[揀位],[x],[y],[width],[depth],[orien],[商品編號];...
		// rackLine: [x1],[y1],[x2],[y2];[x1],[y1],[x2],[y2];...
		try {
			
			// System.out.println("warehouseId = "+warehouseId+", method = "+method+", companyAccount = "+company+", userAccount = "+user+", layoutList = "+layoutList+", orderList = "+orderList);
			return new RsMsg(RsMsg.SUCCESS,
					RoutingService.createPickingPath(company, user, warehouse, rackLine, orderList, method));
		} catch (Exception e) {
			e.printStackTrace();
			return new RsMsg("error", e);
		}
	}

}
