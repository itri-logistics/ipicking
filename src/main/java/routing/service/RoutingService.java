package routing.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import org.springframework.stereotype.Service;

import alg.Aisles;
import alg.Dijkstra;
import alg.Path;
import alg.SimpleGeneticAlgorithm;
import alg.SimpleGeneticAlgorithm.TspFunction;
import alg.Zshap;
import order.dao.OrderDataDao;
import order.dao.PickingFlowDao;

@Service
public class RoutingService {

	static final String METHOD_U_SHAP = "U";
	static final String METHOD_Z_SHAP = "Z";
	static final String METHOD_GA = "GA";

	static double pickTime = 0.1;
	static double moveRate = 90;

	static float[] cutLine = new float[] { 37.05f, 24.1f, 5.7f };

	// layoutStr: [揀位],[商品編號];[揀位],[商品編號]...
	// orderStr: [揀貨日期],[門市代號],[商品代號],[出貨量];[揀貨日期],[門市代號],[商品代號],[出貨量]...
	// warehouseStr:
	// [揀位],[x],[y],[width],[depth],[orien],[商品編號];[揀位],[x],[y],[width],[depth],[orien],[商品編號];...
	// rackLineStr: [x1],[y1],[x2],[y2];[x1],[y1],[x2],[y2];...
	public static HashMap<String, String> createPickingPath(String company, String user, String warehouseStr,
			String rackLineStr, String orderStr, String method) {

		// 讀取揀位資料
		HashMap<String, String> layout = new HashMap<>();
		// 建地圖、讀取揀位資料
		HashMap<String, HashMap<String, Double>> nodes = new HashMap<>();
		ArrayList<String> nodeName = new ArrayList<>();
		double[][] graph = readMap(company, user, warehouseStr, rackLineStr, nodes, nodeName, layout);

		System.out.println("read layout done. " + layout.toString());

		// 產生初始揀貨單
		HashMap<String, Map<String, String>> orderData = new HashMap<>();
		TreeMap<String, HashSet<String>> orderList = createPickingOrder(layout, orderStr, orderData);

		// System.out.println("read order data done. " + orderData.toString());
		System.out.println("read order data done. ");

		String[] orderIdArray = orderList.keySet().toArray(new String[orderList.keySet().size()]);

		if (method.equals(METHOD_GA)) {
			return ga(nodes, orderIdArray, orderList, orderData, graph, nodeName);
		} else if (method.equals(METHOD_Z_SHAP)) {
			return zShape(nodes, orderIdArray, orderList, orderData);
		}

		return null;
		// String createDate = new JDateTime().toString("YYYYMMDDhhmmss");

	}

	private static HashMap<String, String> parseLayout(String layoutStr) {
		HashMap<String, String> layout = new HashMap<>();

		String[] places = layoutStr.split(";");
		for (String p : places) {
			String[] info = p.split(",");
			layout.put(info[1], info[0]); // K: 商品編號, V: 揀位
		}

		return layout;
	}

	private static HashMap<String, String> zShape(HashMap<String, HashMap<String, Double>> nodes, String[] orderIdArray,
			TreeMap<String, HashSet<String>> orderList, final HashMap<String, Map<String, String>> orderData) {

		String start = "start";
		ArrayList<String[]> bestSolutiones = new ArrayList<>();
		ArrayList<Double> minDistances = new ArrayList<>();
		ArrayList<ArrayList<Path>> bestPathes = new ArrayList<>();

		// 建通道
		Aisles aisles = new Aisles();
		for (String key : nodes.keySet()) {
			if (!key.contains("line")) {
				char codeChar = key.charAt(1);
				if (!key.equals(start) && Character.isLetter(codeChar)) {
					String aisleId = key.substring(1, 2);
					int code = Integer.parseInt(key.substring(2, key.length()));
					aisles.addNode(aisleId, code % 2 == 0 ? Aisles.RIGHT : Aisles.LEFT, key, nodes.get(key));
				}
			}
		}
		aisles.sort(true);

		int cores = Runtime.getRuntime().availableProcessors();

		ZShapeRunnable[] runnables = new ZShapeRunnable[orderIdArray.length];
		ThreadPoolExecutor executor = new ThreadPoolExecutor(cores, cores * 3, 120, TimeUnit.SECONDS,
				new LinkedBlockingDeque<Runnable>());
		for (int ii = 0; ii < orderIdArray.length; ii++) {
			String gId = orderIdArray[ii];
			runnables[ii] = new ZShapeRunnable(orderList.get(gId).toArray(new String[orderList.get(gId).size()]),
					aisles, nodes);
			executor.execute(runnables[ii]);
		}

		executor.shutdown();
		try {
			while (!executor.awaitTermination(10, TimeUnit.SECONDS)) {
				System.out.println("執行中任務數 =" + executor.getActiveCount());
				System.out.println("佇列任務數 =" + executor.getCompletedTaskCount() + " / " + orderIdArray.length);
			}
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		for (int ii = 0; ii < orderIdArray.length; ii++) {
			try {
				String gId = orderIdArray[ii];

				String[] bestSolution = runnables[ii].getBestSolution();
				bestSolutiones.add(bestSolution);
				minDistances.add(runnables[ii].getDistance());

				ArrayList<Path> bestPath = runnables[ii].getBestPath();

				int po = 1;
				for (String s : bestSolution) {
					orderData.get(gId + "-" + s).put(PickingFlowDao.FIELD_ARRAY[PickingFlowDao.FIELD_PICKING_ORDER],
							String.valueOf(po));
					po++;
				}

				double[] _dis = new double[bestPath.size()];
				for (int i = 0; i < _dis.length; i++) {
					_dis[i] = bestPath.get(i).getTotalDistance();
				}

				bestPathes.add(bestPath);

			} catch (Exception e) {
				e.printStackTrace();
			}
			if (ii % 100 == 0) {
				System.out.println("Z-Shape: " + (ii + 1) + "/" + orderIdArray.length);
			}
		}

		System.out.println("Z-Shape: ");
		double td = 0;
		double tt = 0;
		ArrayList<float[]> at = calTimeZs(nodes, bestSolutiones, bestPathes, orderIdArray, orderData);
		for (int i = 0; i < orderIdArray.length; i++) {
			try {
				String gId = orderIdArray[i];
				System.out.println(minDistances.get(i));
				if (minDistances.get(i) == 0.0)
					continue;

				System.out.println("路序 " + gId + ": " + Arrays.toString(bestSolutiones.get(i)));
				System.out.println("總距離 " + gId + ": " + minDistances.get(i));
				td += minDistances.get(i);
				if (at.get(i).length > 0) {
					tt += at.get(i)[at.get(i).length - 1];
				}
//				}else {
//					tt += minDistances.get(i) / moveRate;
//				}

			} catch (Exception e) {
				e.printStackTrace();
			}
		}

//		if(td > 0 && tt <=0) {
//			tt = td / moveRate;
//		}

		HashMap<String, String> result = new HashMap<>();

		result.put("total_distance", String.format("%.4f", td));
		result.put("total_duration", String.format("%.4f", tt));

		return result;

	}

	private static HashMap<String, String> ga(HashMap<String, HashMap<String, Double>> nodes, String[] orderIdArray,
			TreeMap<String, HashSet<String>> orderList, final HashMap<String, Map<String, String>> orderData,
			double[][] graph, ArrayList<String> nodeName) {

		String start = "start";

		System.out.println("read graph done. " + Arrays.toString(graph));
		HashMap<String, Dijkstra> dijkstraMap = new HashMap<>();

		dijkstraMap.put("start", new Dijkstra(graph, nodes.get("start").get("idx").intValue()));
		dijkstraMap.get("start").buildPath();

		// 產生揀位表
		HashSet<String> placeList = new HashSet<>();
		for (String key : orderList.keySet()) {
			placeList.addAll(orderList.get(key));
			// System.out.println(key + ": " + orderList.get(key).toString());
		}

		String[] place = placeList.toArray(new String[placeList.size()]);

		for (String s : place) {
			try {
				int src = nodes.get(s).get("idx").intValue();
				dijkstraMap.put(s, new Dijkstra(graph, src));
				dijkstraMap.get(s).buildPath();
			} catch (Exception e) {
				System.err.println(s);
				e.printStackTrace();
				// System.exit(1);
			}

		}
		ArrayList<String[]> bestSolutionesGa = new ArrayList<>();
		ArrayList<Double> minDistancesGa = new ArrayList<>();
		ArrayList<ArrayList<Path>> bestPathesGa = new ArrayList<>();

		// 定義 fitness function
		TspFunction f = new TspFunction() {

			@Override
			public double cal(String[] sol) {
				double d = 0;
				ArrayList<Path> path = new ArrayList<>();
				String _start = start;
				for (String s : sol) {
					int idx = nodes.get(s).get("idx").intValue();
					path.add(dijkstraMap.get(_start).findPath(idx));
					d += path.get(path.size() - 1).getTotalDistance();
					_start = s;
				}
				int idx = nodes.get(start).get("idx").intValue();
				path.add(dijkstraMap.get(_start).findPath(idx));
				d += path.get(path.size() - 1).getTotalDistance();

				// d += calTime(sol, path, i);
				return d;
			}
		};

		int cores = Runtime.getRuntime().availableProcessors();

		GaRunnable[] runnables = new GaRunnable[orderIdArray.length];
		ThreadPoolExecutor executor = new ThreadPoolExecutor(cores, cores * 3, 120, TimeUnit.SECONDS,
				new LinkedBlockingDeque<Runnable>());
		for (int ii = 0; ii < orderIdArray.length; ii++) {
			String gId = orderIdArray[ii];
			runnables[ii] = new GaRunnable(f, orderList.get(gId).toArray(new String[orderList.get(gId).size()]));
			executor.execute(runnables[ii]);
		}

		executor.shutdown();
		try {
			while (!executor.awaitTermination(10, TimeUnit.SECONDS)) {
				System.out.println("執行中任務數 =" + executor.getActiveCount());
				System.out.println("佇列任務數 =" + executor.getCompletedTaskCount() + " / " + orderIdArray.length);
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		for (int ii = 0; ii < orderIdArray.length; ii++) {
			String gId = orderIdArray[ii];

			String[] bestSolution = runnables[ii].getBestSolution();

			bestSolutionesGa.add(bestSolution);
			minDistancesGa.add(f.cal(bestSolution));

			ArrayList<Path> bestPath = new ArrayList<>();
			String _start = start;
			int po = 1;
			for (String s : bestSolution) {
				int idx = nodes.get(s).get("idx").intValue();
				bestPath.add(dijkstraMap.get(_start).findPath(idx));
				_start = s;
				orderData.get(gId + "-" + s).put(PickingFlowDao.FIELD_ARRAY[PickingFlowDao.FIELD_PICKING_ORDER],
						String.valueOf(po));
				po++;
			}

			double[] _dis = new double[bestPath.size()];
			for (int i = 0; i < _dis.length; i++) {
				_dis[i] = bestPath.get(i).getTotalDistance();
			}

			bestPathesGa.add(bestPath);

			System.out.println("GA: " + ii + "/" + orderIdArray.length);
		}

		System.out.println("GA: ");
		double td = 0;
		double tt = 0;
		ArrayList<float[]> at = calTime(bestSolutionesGa, bestPathesGa, orderIdArray, orderData);
		for (int i = 0; i < orderIdArray.length; i++) {
			String gId = orderIdArray[i];
			System.out.println("路序 " + gId + ": " + Arrays.toString(bestSolutionesGa.get(i)));
			System.out.println("總距離 " + gId + ": " + minDistancesGa.get(i));
			td += minDistancesGa.get(i);
			tt += at.get(i)[at.get(i).length - 1];
		}

		HashMap<String, String> result = new HashMap<>();

		result.put("total_distance", String.valueOf(td));
		result.put("total_duration", String.valueOf(tt));

		return result;

	}

	private static ArrayList<float[]> calTime(ArrayList<String[]> solutiones, ArrayList<ArrayList<Path>> pathes,
			String[] orderIdArray, HashMap<String, Map<String, String>> orderData) {
		ArrayList<float[]> arriveTimes = new ArrayList<>();
		int orderNum = solutiones.size();

		for (int i = 0; i < orderNum; i++) {
			String[] solution = solutiones.get(i);
			if (solution == null) {
				float[] arriveTime = new float[0];
				arriveTimes.add(arriveTime);
				continue;
			}
			ArrayList<Path> path = pathes.get(i);
			int nodeNum = solution.length;
			float[] arriveTime = new float[nodeNum];
			for (int j = 0; j < nodeNum; j++) {
				arriveTime[j] = ((float) (path.get(j).getTotalDistance() / moveRate) + (float) pickTime
						* ((float) Integer.parseInt(orderData.get(orderIdArray[i] + "-" + solution[j]).get("qty"))));
			}

			arriveTimes.add(arriveTime);

		}
		return arriveTimes;
	}

	private static ArrayList<float[]> calTimeZs(HashMap<String, HashMap<String, Double>> nodes,
			ArrayList<String[]> solutiones, ArrayList<ArrayList<Path>> pathes, String[] orderIdArray,
			HashMap<String, Map<String, String>> order) {
		ArrayList<float[]> arriveTimes = new ArrayList<>();
		ArrayList<HashMap<String, Float>> crossTimes = new ArrayList<>(); // 每當單通道每個通過檢查點的時間
		// int orderNum = solutiones.size();
		int orderNum = orderIdArray.length;
		// int orderNum = pathes.size();

		for (int i = 0; i < orderNum; i++) {
			try {
				String[] solution = solutiones.get(i);
				if (solution == null || i >= pathes.size()) {
					float[] arriveTime = new float[0];
					HashMap<String, Float> crossTime = new HashMap<>();
					crossTimes.add(crossTime);
					arriveTimes.add(arriveTime);
					continue;
				}
				ArrayList<Path> path = pathes.get(i);
				int nodeNum = solution.length;
				// int nodeNum = path.size();
				float[] arriveTime = new float[nodeNum];
				HashMap<String, Float> crossTime = new HashMap<>();
				for (int j = 0; j < nodeNum; j++) {
					if (j >= path.size()) {
						System.err.println(orderIdArray[i] + ": " + j + ", " + solution.length + ", " + path.size());
						break;
					}
					int qty = Integer.parseInt(order.get(orderIdArray[i] + "-" + solution[j])
							.get(OrderDataDao.FIELD_ARRAY[OrderDataDao.FIELD_QTY]));
					if (i == 0) {
						arriveTime[j] = (float) (path.get(j).getTotalDistance() / moveRate);

						if (j > 0) {
							String stay = "";
							String fromArea = solution[j - 1].substring(0, 1);
							String fromLane = solution[j - 1].substring(1, 2);

							arriveTime[j] += arriveTime[j - 1] + pickTime * (double) qty;
							float lastY = nodes.get(solution[j - 1]).get("y").floatValue();
							if (j == nodeNum - 1) {// 偵測是否離開通道

								for (int k = cutLine.length - 1; k >= 0; k--) {
									if (k == 0) {
										stay = fromArea + fromLane + k;
										break;
									}
									if (lastY > cutLine[k] && lastY < cutLine[k - 1]) {
										stay = fromArea + fromLane + k;
									}
								}
								if (!crossTime.containsKey(stay))
									crossTime.put(stay, arriveTime[j - 1]);
								continue;
							} else {
								String toLane = solution[j].substring(1, 2);
								if (fromLane.equals(toLane)) {

									float y = nodes.get(solution[j]).get("y").floatValue();

									float yMax = Math.max(y, lastY);
									float yMin = Math.min(y, lastY);
									for (int k = 0; k < cutLine.length; k++) {
										if (yMin < cutLine[k] && yMax > cutLine[k]) {
											crossTime.put(fromArea + fromLane + k, arriveTime[j - 1]);
										}
									}
								} else {
									for (int k = cutLine.length - 1; k >= 0; k--) {
										if (k == 0) {
											crossTime.put(fromArea + fromLane + k, arriveTime[j - 1]);
											break;
										}
										if (lastY > cutLine[k] && lastY < cutLine[k - 1]) {
											crossTime.put(fromArea + fromLane + k, arriveTime[j - 1]);
										}
									}
								}
							}
						}

					} else {
						arriveTime[j] = (float) (path.get(j).getTotalDistance() / moveRate);
						String stay = "";
						if (j > 0) {
							arriveTime[j] += arriveTime[j - 1] + pickTime * (double) qty;
							String fromArea = solution[j - 1].substring(0, 1);
							String fromLane = solution[j - 1].substring(1, 2);
							float lastY = nodes.get(solution[j - 1]).get("y").floatValue();
							if (j == nodeNum - 1) {// 偵測是否離開通道
								for (int k = cutLine.length - 1; k >= 0; k--) {
									if (k == 0) {
										stay = fromArea + fromLane + k;
										break;
									}
									if (lastY > cutLine[k] && lastY < cutLine[k - 1]) {
										stay = fromArea + fromLane + k;
									}
								}
								if (!crossTime.containsKey(stay))
									crossTime.put(stay, arriveTime[j - 1]);
							} else {
								String toArea = solution[j].substring(0, 1);
								String toLane = solution[j].substring(1, 2);
								float y = nodes.get(solution[j]).get("y").floatValue();

								if (fromLane.equals(toLane)) {
									float yMax = Math.max(y, lastY);
									float yMin = Math.min(y, lastY);
									for (int k = 0; k < cutLine.length; k++) {
										if (yMin < cutLine[k] && yMax > cutLine[k]) {
											crossTime.put(fromArea + fromLane + k, arriveTime[j - 1]);
										}
									}
								} else {

									for (int k = cutLine.length - 1; k >= 0; k--) {
										if (k == 0) {
											crossTime.put(fromArea + fromLane + k, arriveTime[j - 1]);
											break;
										}
										if (lastY > cutLine[k] && lastY < cutLine[k - 1]) {
											crossTime.put(fromArea + fromLane + k, arriveTime[j - 1]);
										}
									}
								}

								for (int k = cutLine.length - 1; k >= 0; k--) {
									if (k == 0) {
										stay = toArea + toLane + k;
										break;
									}
									if (y > cutLine[k] && y < cutLine[k - 1]) {
										stay = toArea + toLane + k;
									}
								}
								if (crossTimes.get(i - 1).containsKey(stay)
										&& arriveTime[j - 1] < crossTimes.get(i - 1).get(stay)) {
									arriveTime[j] += crossTimes.get(i - 1).get(stay) - arriveTime[j - 1];
								}
							}
						} else {
							String toArea = solution[j].substring(0, 1);
							String toLane = solution[j].substring(1, 2);
							float y = nodes.get(solution[j]).get("y").floatValue();
							for (int k = cutLine.length - 1; k >= 0; k--) {
								if (k == 0) {
									stay = toArea + toLane + k;
									break;
								}
								if (y > cutLine[k] && y < cutLine[k - 1]) {
									stay = toArea + toLane + k;
								}
							}
							if (crossTimes.get(i - 1).containsKey(stay)) {
								arriveTime[j] += crossTimes.get(i - 1).get(stay);
							}
						}

					}

				}

				crossTimes.add(crossTime);
				arriveTimes.add(arriveTime);

			} catch (Exception e) {

				e.printStackTrace();
				System.exit(1);
			}
		}
		return arriveTimes;
	}

	// 將同門市、同日的訂單併成同一張揀貨單
	// layout: [揀位],[商品編號];[揀位],[商品編號]...
	// orderStr: [揀貨日期],[門市代號],[商品代號],[出貨量];[揀貨日期],[門市代號],[商品代號],[出貨量]...
	private static TreeMap<String, HashSet<String>> createPickingOrder(HashMap<String, String> layout, String orderStr,
			HashMap<String, Map<String, String>> orderData) {

		TreeMap<String, HashSet<String>> orderList = new TreeMap<>();

		String[] orders = orderStr.split(";");

		for (String order : orders) {
			String[] info = order.split(",");
			String pid = info[0] + "-" + info[1]; // 揀貨日期-門市代號
			if (!orderList.containsKey(pid)) {
				orderList.put(pid, new HashSet<>());
			}
			String place = layout.get(info[2]);
			System.out.println("Pid and Location:"+info[2]+","+place);//揀貨單產品號碼與與儲位
			orderList.get(pid).add(place);
			Map<String, String> orderMap = new HashMap<>();
			orderMap.put(OrderDataDao.FIELD_ARRAY[OrderDataDao.FIELD_DATE], info[0]);
			orderMap.put(OrderDataDao.FIELD_ARRAY[OrderDataDao.FIELD_STORE_ID], info[1]);
			orderMap.put(OrderDataDao.FIELD_ARRAY[OrderDataDao.FIELD_PLACE], place);
			orderMap.put(OrderDataDao.FIELD_ARRAY[OrderDataDao.FIELD_QTY], info[3]);

			orderList.get(pid).add(place);
			orderData.put(pid + "-" + place, orderMap);
		}
//		 System.out.println(orderData);
		return orderList;
	}

	// warehouseStr:
	// [揀位],[x],[y],[width],[depth],[orien],[商品編號];[揀位],[x],[y],[width],[depth],[orien],[商品編號];...
	// rackLine: [x1],[y1],[x2],[y2];[x1],[y1],[x2],[y2];...
	private static double[][] readMap(String company, String user, String warehouseStr, String rackLineStr,
			final HashMap<String, HashMap<String, Double>> nodes, final ArrayList<String> nodeName,
			final HashMap<String, String> layout) {

		// Map<String, Object> start = WarehouseDao.getWarehouse(warehouseId);
		/* 初始位置 */
		HashMap<String, Double> node = new HashMap<>();
		node.put("idx", (double) nodes.size());
		// node.put("x",
		// Double.parseDouble(start.get(WarehouseDao.FIELD_ARRAY[WarehouseDao.FIELD_START_X]).toString()));
		// node.put("y",
		// Double.parseDouble(start.get(WarehouseDao.FIELD_ARRAY[WarehouseDao.FIELD_START_Y]).toString()));
		node.put("x", 0.0);
		node.put("y", 0.0);
		node.put("width", 0.0);

		nodes.put("start", node);
		nodeName.add("start");

		/* 分割儲位配置資料 */
		String[] storages = warehouseStr.split(";");
		for (int i = 0; i < storages.length; i++) {
			String[] storage = storages[i].split(",");
			node = new HashMap<>();
			node.put("idx", (double) nodes.size());
			node.put("x", Double.parseDouble(storage[1]));
			node.put("y", Double.parseDouble(storage[2]));
			node.put("width", Double.parseDouble(storage[3]));
			String storageName = storage[0];
			nodes.put(storageName, node);
			// 當儲位有放物品再放入
//			if (storage[6].indexOf("n/a") == -1) 
			layout.put(storage[6], storageName); // K: 商品編號, V: 揀位
			nodeName.add(storageName);
		}

		String[] lines = rackLineStr.split(";");// 全部的貨架所佔面積
		List<Map<String, Object>> _lines = new ArrayList<>();
		HashSet<Float> cutLines = new HashSet<>();
		for (int i = 0; i < lines.length; i++) {
			String[] line = lines[i].split(",");
			cutLines.add(Float.parseFloat(String.valueOf(line[1])));// y1
			cutLines.add(Float.parseFloat(String.valueOf(line[3])));// y2
			String name = "line" + i;
			// 儲存貨架面積位置
			HashMap<String, Object> _line = new HashMap<>();
			_line.put("x1", line[0]);
			_line.put("y1", line[1]);
			_line.put("x2", line[2]);
			_line.put("y2", line[3]);
			_lines.add(_line);
			// 將x1,y1,x2,y2的四個角落的點放置節點中
			for (int x = 1; x <= 2; x++) {
				for (int y = 1; y <= 2; y++) {
					node = new HashMap<>();
					node.put("idx", (double) nodes.size());
					node.put("x", Double.parseDouble(_line.get("x" + x).toString()));
					node.put("y", Double.parseDouble(_line.get("y" + y).toString()));
					node.put("width", 0.0);
					node.put("orien", 0.0);
					nodes.put(name + "_x" + x + "y" + y, node);
					nodeName.add(name + "_x" + x + "y" + y);
				}
			}
			_line = new HashMap<>();
			_line.put("x1", line[2]);
			_line.put("y1", line[1]);
			_line.put("x2", line[0]);
			_line.put("y2", line[3]);
			_lines.add(_line);
		}
		cutLine = new float[cutLines.size()];
		Float[] ar = cutLines.toArray(new Float[cutLines.size()]);
		for (int ii = 0; ii < cutLines.size(); ii++) {
			cutLine[ii] = ar[ii];
		}
		double[][] graph = new double[nodes.size()][nodes.size()];
		for (int i = 0; i < nodes.size(); i++) {
			for (int j = 0; j < nodes.size(); j++) {
				if (isConnected(_lines, nodes.get(nodeName.get(i)), nodes.get(nodeName.get(j)))) {
					graph[i][j] = distance(nodes.get(nodeName.get(i)).get("x"), nodes.get(nodeName.get(i)).get("y"),
							nodes.get(nodeName.get(j)).get("x"), nodes.get(nodeName.get(j)).get("y"));
				} else {
					graph[i][j] = Double.POSITIVE_INFINITY;
				}
			}
		}
		return graph;
	}

	private static boolean isConnected(List<Map<String, Object>> lines, HashMap<String, Double> node1,
			HashMap<String, Double> node2) {

		double x1 = node1.get("x");
		double y1 = node1.get("y");
		double x2 = node2.get("x");
		double y2 = node2.get("y");

		for (int i = 0; i < lines.size(); i++) {
			double x3 = Double.parseDouble(lines.get(i).get("x1").toString());
			double y3 = Double.parseDouble(lines.get(i).get("y1").toString());
			double x4 = Double.parseDouble(lines.get(i).get("x2").toString());
			double y4 = Double.parseDouble(lines.get(i).get("y2").toString());

			double c1 = (x2 * y1 - x1 * y2) / (x2 - x1); // y截距
			double c2 = (x4 * y3 - x3 * y4) / (x4 - x3);

			if (x1 == x2) {
				double m2 = (y4 - y3) / (x4 - x3);// 斜率
				double y = m2 * x1 + c2;

				boolean xIn1 = x1 > Math.min(x3, x4) && x1 < Math.max(x3, x4);
				boolean yIn1 = y > Math.min(y3, y4) && y < Math.max(y3, y4);

				boolean yIn2 = y > Math.min(y1, y2) && y < Math.max(y1, y2);

				if (xIn1 && (yIn2 && yIn1)) {
					return false;
				}

			} else if (y1 == y2) {
				double m2 = (y4 - y3) / (x4 - x3);// 斜率

				double x = (y1 - c2) / m2;

				boolean xIn1 = x > Math.min(x3, x4) && x < Math.max(x3, x4);
				boolean yIn1 = y1 > Math.min(y3, y4) && y1 < Math.max(y3, y4);

				boolean xIn2 = x > Math.min(x1, x2) && x < Math.max(x1, x2);

				if ((xIn1 && xIn2) && yIn1) {
					return false;
				}
			} else {
				double m1 = (y2 - y1) / (x2 - x1);// 斜率
				double m2 = (y4 - y3) / (x4 - x3);// 斜率

				double x = (c2 - c1) / (m1 - m2);

				double y = m1 * x + c1;

				boolean xIn1 = x >= Math.min(x3, x4) && x < Math.max(x3, x4);
				boolean yIn1 = y >= Math.min(y3, y4) && y < Math.max(y3, y4);

				boolean xIn2 = x > Math.min(x1, x2) && x < Math.max(x1, x2);
				boolean yIn2 = y > Math.min(y1, y2) && y < Math.max(y1, y2);

				if (xIn1 && yIn1) {
					if (xIn2 && yIn2)
						return false;
				}
			}

		}

		return true;
	}

	private static double distance(double x1, double y1, double x2, double y2) {
		return Math.hypot(x1 - x2, y1 - y2);
	}

	private static void swap(Object[] array, int x1, int x2) {// 交換
		Object z = array[x1];
		array[x1] = array[x2];
		array[x2] = z;
	}

}

class ZShapeRunnable implements Runnable {

	String[] initialSolution;
	Aisles aisles;
	HashMap<String, HashMap<String, Double>> nodes;

	ArrayList<Path> bestPath;
	String[] bestSolution;
	double distance;

	public ZShapeRunnable(String[] initialSolution, Aisles aisles, HashMap<String, HashMap<String, Double>> nodes) {
		super();
		this.initialSolution = initialSolution;
		this.aisles = aisles;
		this.nodes = nodes;
	}

	@Override
	public void run() {

		Zshap zs = new Zshap(initialSolution, nodes, aisles);

		bestSolution = zs.run();
		bestPath = zs.getPath();
		distance = 0;
		double[] _dis = new double[bestPath.size()];
		for (int i = 0; i < _dis.length; i++) {
			_dis[i] = bestPath.get(i).getTotalDistance();
			distance += _dis[i];
		}
	}

	public String[] getBestSolution() {
		return bestSolution;
	}

	public ArrayList<Path> getBestPath() {
		return bestPath;
	}

	public double getDistance() {
		return distance;
	}

}

class GaRunnable implements Runnable {

	TspFunction f;
	String[] initialSolution;
	String[] bestSolution;

	public GaRunnable(TspFunction f, String[] initialSolution) {
		super();
		this.f = f;
		this.initialSolution = initialSolution;
	}

	@Override
	public void run() {
		SimpleGeneticAlgorithm ga = new SimpleGeneticAlgorithm(f, initialSolution);
		ga.setMaxGeneration(8000);
		ga.setUnchangeLimit(200);
		ga.setPc(0.8);
		ga.setPm(0.7);
		ga.initialSolution();
		try {
			ga.run();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		bestSolution = ga.getBest();
	}

	public String[] getBestSolution() {
		return bestSolution;
	}

}
