package common.bean;

import java.io.File;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.http.HttpServlet;

import param.Pa;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;

public class SysListener extends HttpServlet implements ServletContextListener {

	@Autowired
	JdbcTemplate jdbcTemplate;

	private static final long serialVersionUID = 1L;

	@Override
	public void contextInitialized(ServletContextEvent sce) {
		System.out.println("ini: " + String.valueOf(jdbcTemplate == null));
		// 建立資料夾
		String[] floders = { Pa.examplePath, Pa.graphPath, Pa.imgPath, Pa.mapPath, Pa.uploadPath };
		for (String s : floders) {
			File file = new File(s);
			if (!file.exists()) {
				file.mkdirs();
			}
		}

		sce.getServletContext().setAttribute("js_version", Pa.JS_VERSION);
		sce.getServletContext().setAttribute("lib_link", Pa.CDN_ADMIN_LIB_LINK);
	}

	@Override
	public void contextDestroyed(ServletContextEvent sce) {

	}

}
