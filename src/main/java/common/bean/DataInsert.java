package common.bean;

import java.io.File;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import param.Pa;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;

import order.dao.OrderDataDao;

public class DataInsert {

	@Autowired
	JdbcTemplate jdbcTemplate;

	private static String placeFileName = "D:\\前瞻計畫\\2018大AI計畫\\萊爾富資料\\20180521北區常溫整箱區基本資料.xlsx";
	// private static String orderFileName =
	// "D:\\前瞻計畫\\2018大AI計畫\\萊爾富資料\\訂單\\C.TXT";
	private static String orderFileName = Pa.examplePath + File.separator + "C.TXT";
	private static HashMap<String, String> placeMap = new HashMap<>();
	private static String mapStorageFileName = "D:\\前瞻計畫\\2018大AI計畫\\儲位配置\\座標data\\c_area.csv";
	private static String mapLinesFile = "D:\\前瞻計畫\\2018大AI計畫\\儲位配置\\座標data\\lines.csv";

	public static void main(String[] args) {
		readOrders();
	}

	private static void readOrders() {

		System.out.println("讀取訂單...");

		File file = new File(orderFileName);

		try {
			HashMap<String, ArrayList<Object[]>> orderList = new HashMap<>();

			// ArrayList<Object[]> flowList = new ArrayList<>();
			List<String> lines = FileUtils.readLines(file);
			int count = 0;
			for (String line : lines) {
				String[] order = line.split(",");
				String goodId = order[2];
				String placeId = placeMap.get(goodId);
				int warehouseId;
				if (StringUtils.startsWith(placeId, "C")) {
					warehouseId = 2;
				} else {
					warehouseId = 1;
				}

				String date = order[0].replace("/", "");
				String store = order[1];
				String qty = order[3];
				String yyyymm = date.substring(0, 6);
				if (!orderList.containsKey(yyyymm)) {
					orderList.put(yyyymm, new ArrayList<>());
				}
				orderList.get(yyyymm).add(new Object[] { warehouseId, date, store, goodId, placeId, qty, store,
						DateTimeFormatter.ofPattern("yyyyMMddHHmmss").format(LocalDateTime.now()), "ATone", "", "0" });
				// flowList.add(new Object[] { warehouseId, date, store, goodId, placeId, qty,
				// store, "",
				// new JDateTime().toString("YYYYMMDDhhmmss"), "", "", "", "", "", "" });
				count++;
				if (count % 5000 == 0) {
					System.out.println(count + "/" + lines.size());
				}

				if (count % 100000 == 0) {
					for (String yymm : orderList.keySet()) {
						OrderDataDao.insertBatch(orderList.get(yymm), yymm);
					}
					// PickingFlowDao.insertBatch(flowList);
					orderList.clear();
					// flowList.clear();
				}
			}

			for (String yymm : orderList.keySet()) {
				OrderDataDao.insertBatch(orderList.get(yymm), yymm);
			}
			// PickingFlowDao.insertBatch(flowList);
			orderList.clear();
			// flowList.clear();

			System.out.println("訂單讀取完畢.");

		} catch (Exception e) {
			e.printStackTrace();
			System.exit(1);
		}

	}

}
