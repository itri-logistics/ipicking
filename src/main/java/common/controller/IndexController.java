package common.controller;

import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import param.Pa;
import param.RsMsg;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttribute;

import jodd.util.StringUtil;

/***
 * 訂單資料
 * 
 * 套件:order
 * 
 * @author ATone create by 2017/06/12
 **/

@Controller
@RequestMapping()
public class IndexController {

	@RequestMapping(value = { "/", "/index" })
	public String loadPage(@SessionAttribute(name = "lang", required = false) String lang,
			@RequestParam(value = "lang", defaultValue = "") String _lang,
			HttpServletRequest request,
			HttpSession session, ModelMap model) {

		//System.out.println("SessionAttribute: "+lang);
		//System.out.println("SessionAttribute: "+String.valueOf(session.getAttribute("lang")));
		
		if (!StringUtil.isBlank(_lang)) {
			lang =  _lang;
			session.setAttribute("lang", lang);
		}

		if (StringUtil.isBlank(lang)) {
			Locale locale = request.getLocale();
			lang = locale.getLanguage();
			if (lang.equals(new Locale("zh").getLanguage())) {
				lang = "tw";
			} else {
				lang = "en";
			}
			session.setAttribute("lang", lang);
		} else {
			//System.out.println(lang);
		}

		System.out.println("index: "+lang);
		
		return lang + "/index/indexView";
	}

	@RequestMapping(value = "/changeLang")
	public @ResponseBody Object changeLanguagePage(@RequestParam(value = "lang", defaultValue = "") String lang,
			HttpSession session, ModelMap model) {

		if (StringUtil.isNotBlank(lang)) {
			session.setAttribute("lang", lang);
		}

		//System.out.println("chang: "+lang);
		
		return Pa.success;
	}
}
