package common.utils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Random;



public class Utils {

	public Utils() {

	}
	
	// 在0~range內取得n個不重複亂數
	public static int[] getRandomList(int n, int range) {
		ArrayList<Integer> pool = new ArrayList<>();
		for(int i = 0;i<range;i++) {
			pool.add(i);
		}
		
		Collections.shuffle(pool);
		
		int[] result = new int[n];
		
		for(int i = 0;i<n;i++) {
			result[i] = pool.get(i);
		}				
		
		return result;
	}
	
	// 算出歐式距離
	public static double euclideanDistance(double x1, double y1, double x2, double y2) {
		return Math.hypot(x1 - x2, y1 - y2);
	}

	public static double angle(double lat_a, double lng_a, double lat_b, double lng_b) {
		double d = 0;
		lat_a = lat_a * Math.PI / 180;
		lng_a = lng_a * Math.PI / 180;
		lat_b = lat_b * Math.PI / 180;
		lng_b = lng_b * Math.PI / 180;

		d = Math.sin(lat_a) * Math.sin(lat_b) + Math.cos(lat_a) * Math.cos(lat_b) * Math.cos(lng_b - lng_a);
		d = Math.sqrt(1 - d * d);
		if(d == 0) {
			return 0;
		}
		d = Math.cos(lat_b) * Math.sin(lng_b - lng_a) / d;
		d = Math.asin(d) * 180 / Math.PI;

		return d;
	}

	public static int hammingDistance(String[] s1, String[] s2) {
		int hDistance = Math.abs((s1.length - s2.length));

		int l = s1.length < s2.length ? s1.length : s2.length;

		for (int i = 0; i < l; i++) {
			if (!s1[i].equals(s2[i])) {
				hDistance++;
			}
		}

		return hDistance;
	}

	// 隨機[0,n)間的整數
	public static int dice(int n) {
		Random rand = new Random();
		return rand.nextInt(n);
	}

	public static String timeFormat(double min) {
		String time = "";
		while (min < 0) {
			min += 1440;
		}
		if (min < 60) {
			if (min < 10) {
				return "00:0" + (int) min;
			} else {
				return "00:" + (int) min;
			}
		} else {
			int hh = (int) (min / 60);
			int mm = (int) (min % 60);

			if (hh < 10) {
				time = "0" + hh + ":";
			} else {
				time = hh + ":";
			}

			if (mm < 10) {
				time += "0" + mm;
			} else {
				time += mm;
			}

			return time;
		}
	}

	public static double parseTimeToMin(String time) {
		if (!time.contains(":")) {
			return Double.parseDouble(time);
		}
		String[] timeArray = time.split(":");
		return Double.parseDouble(timeArray[0]) * 60 + Double.parseDouble(timeArray[1]);
	}

}
