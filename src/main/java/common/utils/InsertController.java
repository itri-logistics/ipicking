package common.utils;

import java.io.File;
import java.time.Clock;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import param.Pa;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping(value = "/insertFromFile")
public class InsertController {

	private String placeFileName = "D:\\前瞻計畫\\2018大AI計畫\\萊爾富資料\\20180521北區常溫整箱C區基本資料.xlsx";
	private String orderFileName = "D:\\前瞻計畫\\2018大AI計畫\\萊爾富資料\\訂單\\C.TXT";
	private HashMap<String, String> placeMap = new HashMap<>();

	@RequestMapping()
	public @ResponseBody Object insertDataFromFile() {
		readPlace();
		readOrders();
		return Pa.success;
	}

	private void readOrders() {

		System.out.println("讀取訂單...");

		File file = new File(orderFileName);

		try {

			ArrayList<Object[]> orderList = new ArrayList<>();
			ArrayList<Object[]> flowList = new ArrayList<>();
			List<String> lines = FileUtils.readLines(file);
			for (String line : lines) {
				String[] order = line.split(",");
				String goodId = order[2];
				String placeId = placeMap.get(goodId);

				if (StringUtils.startsWith(placeId, "C")) {
					String date = order[0].replace("/", "");
					String store = order[1];
					String qty = order[3];
					orderList.add(new Object[] { date, store, goodId, placeId, qty, store });
					flowList.add(new Object[] { date, store, goodId, placeId, qty, store, "",
							DateTimeFormatter.ofPattern("yyyyMMddHHmmss").format(LocalDateTime.now(Clock.system(ZoneId.of("+8")))), "", "", "", "", "", "" });
				}

			}

			//OrderDataDao.insertBatch(orderList);
			//PickingFlowDao.insertBatch(flowList);

			System.out.println("訂單讀取完畢.");

		} catch (Exception e) {
			e.printStackTrace();
			System.exit(1);
		}

	}

	private void readPlace() {

		System.out.println("讀取揀位擋...");

		File file = new File(placeFileName);

		if (!file.exists()) {
			System.err.println("file not found: " + placeFileName);
			System.exit(1);
		}

		try {
			XSSFWorkbook workbook = new XSSFWorkbook(file);
			XSSFSheet sheet = workbook.getSheetAt(0);
			int rowNum = sheet.getPhysicalNumberOfRows();
			for (int y = 1; y < rowNum; y++) {
				XSSFRow row = sheet.getRow(y);
				DataFormatter df = new DataFormatter();
				if (row.getCell(2) == null || "".equals(row.getCell(2).toString())) {
					continue;
				}
				String goodId = df.formatCellValue(row.getCell(2));
				String placeId = df.formatCellValue(row.getCell(1));
				if (!placeMap.containsKey(goodId)) {
					placeMap.put(goodId, placeId);
				}
			}
			workbook.close();

			System.out.println("揀位擋讀取完畢.");

		} catch (Exception e) {
			e.printStackTrace();
			System.exit(1);
		}

	}
}
