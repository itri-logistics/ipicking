package graph.controller;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import org.apache.commons.io.IOUtils;
import param.Pa;
import param.RsMsg;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;


/***
 * 上傳平面圖轉graph
 * 
 * 套件:graph
 * 
 * uploadGraphView.jsp uploadGraph.js
 * 
 * @author ATone create by 2017/05/02
 **/

@Controller
@RequestMapping(value = "/uploadGraph")
public class UploadGraphController {

	@RequestMapping()
	public String loadPage(ModelMap model) {
		return "graph/uploadGraphView";
	}

	// 更新場域平面圖
	@RequestMapping(value = "/updateImage", method = RequestMethod.POST)
	public @ResponseBody Object updateImage(@RequestParam("file") MultipartFile file, ModelMap model) {
		try {
			// 更新圖
			String fileName = DateTimeFormatter.ofPattern("yyyyMMddHHmmss").format(LocalDateTime.now());
			// img dir
			String imgPath = Pa.imgPath;
			File imgDirFile = new File(imgPath);
			if (!imgDirFile.exists()) {
				imgDirFile.mkdir();
			}

			if (!file.isEmpty()) {
				try {

					BufferedOutputStream stream = new BufferedOutputStream(
							new FileOutputStream(new File(imgPath + File.separator + fileName + ".jpg")));
					FileCopyUtils.copy(file.getInputStream(), stream);
					stream.flush();
					stream.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}

			return new RsMsg(RsMsg.SUCCESS, fileName);

		} catch (Exception e) {
			e.printStackTrace();
			return new RsMsg("error", e);
		}
	}

	// 下載圖片
	@RequestMapping(value = "/img/{fileName}", method = RequestMethod.GET)
	public ResponseEntity<byte[]> getFile(@PathVariable(value = "fileName") String fileName)
			throws FileNotFoundException, IOException {

		final HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
		headers.setContentType(MediaType.IMAGE_JPEG);
		File img = new File(Pa.imgPath + File.separator + fileName + ".jpg");
		if (!img.exists()) {

		}
		FileInputStream fi = new FileInputStream(img);
		byte[] bs = IOUtils.toByteArray(fi);
		fi.close();
		return new ResponseEntity<byte[]>(bs, headers, HttpStatus.CREATED);
	}

}
