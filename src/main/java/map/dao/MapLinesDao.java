package map.dao;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import param.Pa;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import common.bean.ConnoDataSource;
import common.utils.LoggerController;
import jodd.io.FileUtil;
import jodd.util.ArraysUtil;

@Service
public class MapLinesDao {

	private static JdbcTemplate jdbcTemplate = new JdbcTemplate(new ConnoDataSource().helloWorld());

	public static final String TABLE_NAME = "map_lines";
	public static final int FIELD_WAREHOUSE_ID = 0;
	public static final int FIELD_X1 = 1;
	public static final int FIELD_Y1 = 2;
	public static final int FIELD_X2 = 3;
	public static final int FIELD_Y2 = 4;
	public static final String[] FIELD_ARRAY = { "warehouseId", "x1", "y1", "x2", "y2" };
	public static final String FIELD_ARRAY_STRING = ArraysUtil.toString(FIELD_ARRAY);

	public static List<Map<String, Object>> find(String warehouseId) {

		List<Map<String, Object>> list = new ArrayList<>();

		String filePath = Pa.mapPath + File.separator + warehouseId + File.separator + "map_lines.txt";

		File file = new File(filePath);

		if (file.exists()) {
			try {
				String ss = FileUtil.readString(file);

				String[] lines = ss.split(";");
				for (String line : lines) {
					String[] map = line.split(","); // goodId, placeId

					HashMap<String, Object> node = new HashMap<>();
					node.put(FIELD_ARRAY[FIELD_WAREHOUSE_ID], warehouseId);
					node.put(FIELD_ARRAY[FIELD_X1], map[0]);
					node.put(FIELD_ARRAY[FIELD_Y1], map[1]);
					node.put(FIELD_ARRAY[FIELD_X2], map[2]);
					node.put(FIELD_ARRAY[FIELD_Y2], map[3]);
					list.add(node);
				}

			} catch (IOException e) {

				e.printStackTrace();
			}
		}

		// StringBuilder sql = new StringBuilder("select * from " + TABLE_NAME + " where
		// ");
		// List query = new ArrayList();
		// if (StringUtils.isNotBlank(warehouseId)) {
		// sql.append(FIELD_ARRAY[FIELD_WAREHOUSE_ID] + " = ? and ");
		// query.add(warehouseId);
		// }
		//
		// if (query.isEmpty()) {
		// sql = sql.delete(sql.length() - 6, sql.length());
		// } else {
		// sql = sql.delete(sql.length() - 4, sql.length());
		// }
		//
		// // System.out.println(sql);
		// List<Map<String, Object>> list = jdbcTemplate.queryForList(sql.toString(),
		// query.toArray());

		return list;
	}

	@Transactional
	public static void insert(String warehouseId, String x1, String y1, String x2, String y2) {
		String condi = "insert " + TABLE_NAME + " (" + FIELD_ARRAY_STRING + ") values (?,?,?,?,?,?)";
		jdbcTemplate.update(condi, new Object[] { warehouseId, x1, y1, x2, y2 });
	}

	@Transactional
	public static void insertBatch(ArrayList<Object[]> orderList) {
		LoggerController.dLog.debug("insertOrderToSQLBatch(), " + orderList.toString());

		String condi = "insert ignore " + TABLE_NAME + " (" + FIELD_ARRAY_STRING + ") values (?,?,?,?,?)";
		try {
			if (!orderList.isEmpty()) {
				ArrayList<Object[]> insertList = new ArrayList<>();
				for (int i = 0; i < orderList.size(); i++) {
					insertList.add(orderList.get(i));
					if (insertList.size() == 500 || i == orderList.size() - 1) {
						jdbcTemplate.batchUpdate(condi, insertList);
						System.out.println(i + "/" + orderList.size());
						insertList = new ArrayList<>();
						Thread.sleep(500);
					}
				}
			}
		} catch (DuplicateKeyException e) {
			LoggerController.eLog
					.error("insert data list to " + TABLE_NAME + " Duplicate Key. " + orderList.toString());
			LoggerController.eLog.error(e.toString());
			e.printStackTrace();
		} catch (Exception e) {
			LoggerController.eLog.error("insert data list to " + TABLE_NAME + " fail. " + orderList.toString());
			LoggerController.eLog.error(e.toString());
			e.printStackTrace();
		}
	}

}
