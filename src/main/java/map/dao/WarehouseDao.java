package map.dao;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import common.bean.ConnoDataSource;
import common.utils.LoggerController;
import jodd.util.ArraysUtil;

@Service
public class WarehouseDao {

	private static JdbcTemplate jdbcTemplate = new JdbcTemplate(new ConnoDataSource().helloWorld());

	public static final String TABLE_NAME = "warehouse";
	public static final int FIELD_WAREHOUSE_ID = 0;
	public static final int FIELD_WAREHOUSE_NAME = 1;
	public static final int FIELD_START_X = 2;
	public static final int FIELD_START_Y = 3;
	public static final String[] FIELD_ARRAY = { "warehouseId", "warehouseName", "startX", "startY" };
	public static final String FIELD_ARRAY_STRING = ArraysUtil.toString(FIELD_ARRAY);

	public static Map<String, Object> getWarehouse(String warehouseId) {
		StringBuilder sql = new StringBuilder("select * from " + TABLE_NAME + " where "+FIELD_ARRAY[FIELD_WAREHOUSE_ID] + " = ?");
		List query = new ArrayList();
		query.add(warehouseId);
		
		List<Map<String, Object>> list = jdbcTemplate.queryForList(sql.toString(), query.toArray());

		return list.isEmpty()? null:list.get(0);
	}
	
	public static List<Map<String, Object>> find(String warehouseId, String warehouseName) {
		StringBuilder sql = new StringBuilder("select * from " + TABLE_NAME + " where ");
		List query = new ArrayList();
		if (StringUtils.isNotBlank(warehouseId)) {
			sql.append(FIELD_ARRAY[FIELD_WAREHOUSE_ID] + " = ? and ");
			query.add(warehouseId);
		}

		if (StringUtils.isNotBlank(warehouseName)) {
			sql.append(FIELD_ARRAY[FIELD_WAREHOUSE_NAME] + " = ? and ");
			query.add(warehouseName);
		}

		if (query.isEmpty()) {
			sql = sql.delete(sql.length() - 6, sql.length());
		} else {
			sql = sql.delete(sql.length() - 4, sql.length());
		}

		// System.out.println(sql);
		List<Map<String, Object>> list = jdbcTemplate.queryForList(sql.toString(), query.toArray());

		return list;
	}

	@Transactional
	public static void insert(String warehouseName, String x, String y) {
		String condi = "insert " + TABLE_NAME + " (" + FIELD_ARRAY[FIELD_WAREHOUSE_NAME] + ") values (?,?,?)";
		jdbcTemplate.update(condi, new Object[] { warehouseName,Double.parseDouble(x), Double.parseDouble(y) });
	}

	@Transactional
	public static void insertBatch(ArrayList<Object[]> orderList) {
		LoggerController.dLog.debug("insertOrderToSQLBatch(), " + orderList.toString());

		String condi = "insert ignore " + TABLE_NAME + " (" + FIELD_ARRAY[FIELD_WAREHOUSE_NAME] + ") values (?,?,?)";
		try {
			if (!orderList.isEmpty()) {
				ArrayList<Object[]> insertList = new ArrayList<>();
				for (int i = 0; i < orderList.size(); i++) {
					insertList.add(orderList.get(i));
					if (insertList.size() == 500 || i == orderList.size() - 1) {
						jdbcTemplate.batchUpdate(condi, insertList);
						System.out.println(i + "/" + orderList.size());
						insertList = new ArrayList<>();
						Thread.sleep(500);
					}
				}
			}
		} catch (DuplicateKeyException e) {
			LoggerController.eLog
					.error("insert data list to " + TABLE_NAME + " Duplicate Key. " + orderList.toString());
			LoggerController.eLog.error(e.toString());
			e.printStackTrace();
		} catch (Exception e) {
			LoggerController.eLog.error("insert data list to " + TABLE_NAME + " fail. " + orderList.toString());
			LoggerController.eLog.error(e.toString());
			e.printStackTrace();
		}
	}

}
