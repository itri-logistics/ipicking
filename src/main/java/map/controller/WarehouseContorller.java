package map.controller;

import param.RsMsg;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import map.dao.WarehouseDao;

/***
 * 倉庫資料
 * 
 * 套件:map
 * 
 * @author ATone create by 2018/08/16
 **/

@Controller
@RequestMapping(value = "/warehouse")
public class WarehouseContorller {

	@Autowired
	WarehouseDao warehouseDao;

	// 取得倉儲
	@RequestMapping(value = "/get", method = RequestMethod.GET)
	public @ResponseBody Object getOrderData(
			@RequestParam(value="warehouseId", defaultValue = "") String warehouseId, 
			@RequestParam(value="warehouseName", defaultValue = "") String warehouseName, 
			ModelMap model) {
		try {

			return warehouseDao.find(warehouseId, warehouseName);

		} catch (Exception e) {
			e.printStackTrace();
			return new RsMsg("error", e);
		}
	}

}
