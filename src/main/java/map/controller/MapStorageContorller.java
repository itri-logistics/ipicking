package map.controller;

import param.RsMsg;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import jodd.util.StringUtil;
import map.dao.MapStorageDao;
import map.dao.WarehouseDao;

/***
 * 倉庫資料
 * 
 * 套件:map
 * 
 * @author ATone create by 2018/08/16
 **/

@Controller
@RequestMapping(value = "/map")
public class MapStorageContorller {

	@Autowired
	MapStorageDao mapStorageDao;

	// 取得倉儲
	@RequestMapping(value = "/get", method = RequestMethod.GET)
	public @ResponseBody Object getOrderData(
			@RequestParam(value="warehouseId", defaultValue = "") String warehouseId, 
			@RequestParam(value="layoutId", defaultValue = "") String layoutId, 
			@RequestParam(value="companyAccount", defaultValue = "") String company, 
			@RequestParam(value="userAccount", defaultValue = "") String user,
			ModelMap model) {
		try {
			if (layoutId.equals("undefined")) layoutId = "";
			if(StringUtil.isBlank(warehouseId)) {
				return new RsMsg("error", "warehouseId is required.");
			}
			if(StringUtil.isBlank(company) || StringUtil.isBlank(user) || StringUtil.isBlank(layoutId) ) {
				return mapStorageDao.find(warehouseId);
			}else {
				return mapStorageDao.find(company, user, warehouseId, layoutId);
			}		

		} catch (Exception e) {
			e.printStackTrace();
			return new RsMsg("error", e);
		}
	}

}
