package alg;

import java.util.Arrays;


public class Path {
	int[] path;
	double[] distance;
	double totalDistance;

	public Path(int[] path, double[] distance, double totalDistance) {
		super();
		this.path = path;
		this.distance = distance;
		this.totalDistance = totalDistance;
	}

	public int[] getPath() {
		return path;
	}

	public void setPath(int[] path) {
		this.path = path;
	}

	public double[] getDistance() {
		return distance;
	}

	public void setDistance(double[] distance) {
		this.distance = distance;
	}

	public double getTotalDistance() {
		return totalDistance;
	}

	public void setTotalDistance(double totalDistance) {
		this.totalDistance = totalDistance;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + Arrays.hashCode(distance);
		result = prime * result + Arrays.hashCode(path);
		long temp;
		temp = Double.doubleToLongBits(totalDistance);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Path other = (Path) obj;

		if (!Arrays.equals(distance, other.distance))
			return false;
		if (!Arrays.equals(path, other.path))
			return false;
		if (Double.doubleToLongBits(totalDistance) != Double.doubleToLongBits(other.totalDistance))
			return false;
		return true;
	}
	
	@Override
	public String toString() {
		return "Path [path=" + Arrays.toString(path) + ", distance=" + Arrays.toString(distance)
				+ ", totalDistance=" + totalDistance + "]";
	}

}

