package alg;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.TreeMap;

import jodd.io.FileUtil;

/*
 * Z字穿越政策
 * */
public class Zshap {

	private String[] initialSolution;
	private String[] best;
	private ArrayList<Path> path;
	private HashMap<String, HashMap<String, Double>> nodes;
	private Aisles aisles;

	public Zshap(String[] initialSolution, HashMap<String, HashMap<String, Double>> nodes, Aisles aisles) {
		super();
		this.initialSolution = initialSolution;
		this.nodes = nodes;
		this.aisles = aisles;
	}

	public ArrayList<Path> getPath() {
		return path;
	}

	public void setPath(ArrayList<Path> path) {
		this.path = path;
	}

	public HashMap<String, HashMap<String, Double>> getNodes() {
		return nodes;
	}

	public void setNodes(HashMap<String, HashMap<String, Double>> nodes) {
		this.nodes = nodes;
	}

	public Aisles getAisles() {
		return aisles;
	}

	public void setAisles(Aisles aisles) {
		this.aisles = aisles;
	}

	public String[] getInitialSolution() {
		return initialSolution;
	}

	public void setInitialSolution(String[] initialSolution) {
		this.initialSolution = initialSolution;
	}

	public String[] getBest() {
		return best;
	}

	public void setBest(String[] best) {
		this.best = best;
	}

	public String[] run() {

		TreeMap<String, ArrayList<String>> laneNode = new TreeMap<>();
		ArrayList<String> result = new ArrayList<>();
		ArrayList<Integer> _path = new ArrayList<>();
		ArrayList<Path> _pathes = new ArrayList<>();
		ArrayList<Double> _distance = new ArrayList<>();
		double totalDistance = 0;
		for (String s : initialSolution) {
			String lane = s.substring(1, 2);
			if (!laneNode.containsKey(lane)) {
				laneNode.put(lane, new ArrayList<>());
			}
			laneNode.get(lane).add(s);
		}
		
		if(laneNode.keySet().isEmpty()) {
			return best;
		}

		boolean acs = true;
		HashMap<String, Double> in;
		HashMap<String, Double> out = nodes.get("start");
		_path.add(out.get("idx").intValue());
		_distance.add(0.0);
		String inArea;
		String outArea = "A";		
		String lastLane = laneNode.firstKey();
		for (String lane : laneNode.keySet()) {
			ArrayList<String> _nodes = laneNode.get(lane);
			if (acs) {
				in = aisles.getHead(lane);
			} else {
				in = aisles.getTail(lane);
			}

			sort(_nodes, in);
			// System.out.println("in: "+in.toString());
			// System.out.println(_nodes.toString());

			inArea = _nodes.get(0).substring(0, 1);

			// 上條走道的最後一點至上條走道的出口
			if (acs) {
				if (inArea.equals("B") && outArea.equals("B")) {
					in = aisles.getHead(lastLane, "B");
				} else {
					in = aisles.getHead(lastLane, "A");
				}
			} else {
				if (inArea.equals("A") && outArea.equals("A")) {
					in = aisles.getTail(lastLane, "A");
				} else {
					in = aisles.getTail(lastLane, "B");
				}
			}

			_path.add(in.get("idx").intValue());
			double d = distance(out, in);
			_distance.add(d);
			totalDistance += d;

			out = in;
			// 上條走道的出口至本次走道的入口
			if (acs) {
				if (inArea.equals("B") && outArea.equals("B")) {
					in = aisles.getHead(lane, "B");
				} else {
					in = aisles.getHead(lane, "A");
				}
			} else {
				if (inArea.equals("A") && outArea.equals("A")) {
					in = aisles.getTail(lane, "A");
				} else {
					in = aisles.getTail(lane, "B");
				}
			}

			_path.add(in.get("idx").intValue());
			d = distance(out, in);
			_distance.add(d);
			totalDistance += d;

			for (String n : _nodes) {
				_path.add(nodes.get(n).get("idx").intValue());
				d = distance(in, nodes.get(n));
				_distance.add(d);
				totalDistance += d;
				_pathes.add(new Path(_path.stream().mapToInt(Integer::intValue).toArray(),
						_distance.stream().mapToDouble(Double::doubleValue).toArray(), totalDistance));
				_path = new ArrayList<>();
				_distance = new ArrayList<>();
				_path.add(nodes.get(n).get("idx").intValue());
				_distance.add(0.0);
				totalDistance = 0;
				in = nodes.get(n);
			}

			outArea = _nodes.get(_nodes.size() - 1).substring(0, 1);

			out = in;
			lastLane = lane;
			result.addAll(_nodes);
			acs = !acs;
		}

		// 上條走道的最後一點至上條走道的出口				

		in = aisles.getHead(lastLane, "A", Aisles.RIGHT);	
		_path.add(in.get("idx").intValue());
		double d = distance(out, in);
		_distance.add(d);
		totalDistance += d;

		_pathes.add(new Path(_path.stream().mapToInt(Integer::intValue).toArray(),
				_distance.stream().mapToDouble(Double::doubleValue).toArray(), totalDistance));
		best = result.toArray(new String[result.size()]);
		path = _pathes;
		
		//System.out.println(result.toString());
		//System.out.println(path.toString());
		
		return best;
	}

	private void sort(ArrayList<String> _nodes, HashMap<String, Double> in) {
		_nodes.sort(new Comparator<String>() {
			@Override
			public int compare(String o1, String o2) {
				double d1 = distance(in, nodes.get(o1));
				double d2 = distance(in, nodes.get(o2));
				if (d1 > d2)
					return 1;
				else if (d1 < d2)
					return -1;
				else
					return 0;
			}
		});
	}

	private static double distance(HashMap<String, Double> node1, HashMap<String, Double> node2) {
		return Math.hypot(node1.get("x") - node2.get("x"), node1.get("y") - node2.get("y"));
	}

}
