package alg;

import java.util.ArrayList;
import java.util.Collections;


import common.utils.Utils;

public class TwoOpt {

	public TwoOpt() {
	}	

	public static ArrayList<String> twoOpt(ArrayList<String> stops) {	
		
		int stopNum = stops.size();
		if(stopNum < 2){
			return stops;
		}
		// 隨機抽兩點
		int l, k;
		
		// 隨機選兩點進行轉置

		l = Utils.dice(stopNum);

		do {
			k = Utils.dice(stopNum);
		} while (l == k);

		int i = Math.max(l, k);
		int j = Math.min(l, k);		
		
		while(j<i){
			Collections.swap(stops, i, j);
			j++;
			i--;
		}
		
		return stops;

	}

}
