package alg;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;

/*
 * 通道
 * */
public class Aisles {

	public final static int LEFT = 1;
	public final static int RIGHT = 2;
	public final static int All = 0;
	private final static String INDEX = "idx";
	private final static String X = "x";
	private final static String Y = "y";

	HashMap<String, HashMap<Integer, ArrayList<Node>>> aisles;

	public Aisles() {
		aisles = new HashMap<>();
	}

	public HashMap<String, HashMap<Integer, ArrayList<Node>>> getAisles() {
		return aisles;
	}

	public void setAisles(HashMap<String, HashMap<Integer, ArrayList<Node>>> aisles) {
		this.aisles = aisles;
	}

	public void addAisle(String aisleId) {
		aisles.put(aisleId, new HashMap<>());
		aisles.get(aisleId).put(LEFT, new ArrayList<>());
		aisles.get(aisleId).put(RIGHT, new ArrayList<>());
	}

	public boolean setAisle(String aisleId, HashMap<Integer, ArrayList<Node>> aisle) {
		if (aisle.containsKey(LEFT) && aisle.containsKey(RIGHT)) {
			aisles.put(aisleId, aisle);
			return true;
		}
		return false;
	}

	public void addNode(String aisleId, int side, String nodeId,HashMap<String, Double> node) {
		if (!aisles.containsKey(aisleId)) {
			aisles.put(aisleId, new HashMap<>());
			aisles.get(aisleId).put(LEFT, new ArrayList<>());
			aisles.get(aisleId).put(RIGHT, new ArrayList<>());
		}
		aisles.get(aisleId).get(side).add(new Node(nodeId, node));
	}

	public void sort() {
		sort(false);
	}

	public void sort(boolean reverse) {
		for (String aisleId : aisles.keySet()) {
			for (int side : aisles.get(aisleId).keySet()) {
				if (reverse) {
					aisles.get(aisleId).get(side).sort(compareYReverse);					
				} else {
					aisles.get(aisleId).get(side).sort(compareY);
				}
				System.out.println(aisles.get(aisleId).get(side).toString());
			}
		}
	}

	public void sort(String aisleId, boolean reverse) {
		for (int side : aisles.get(aisleId).keySet()) {
			if (reverse) {
				aisles.get(aisleId).get(side).sort(compareYReverse);
			} else {
				aisles.get(aisleId).get(side).sort(compareY);
			}
		}
	}

	public ArrayList<Node> getNodes(String aisleId, int side) {
		if (side == LEFT || side == RIGHT) {
			return new ArrayList<>(aisles.get(aisleId).get(side));
		} else {
			ArrayList<Node> nodes = new ArrayList<>();
			nodes.addAll(aisles.get(aisleId).get(LEFT));
			nodes.addAll(aisles.get(aisleId).get(RIGHT));
			return nodes;
		}
	}

	public HashMap<String, Double> getHead(String aisleId) {
		return aisles.get(aisleId).get(LEFT).get(0).getData();
	}
	
	public HashMap<String, Double> getHead(String aisleId, int side) {
		return aisles.get(aisleId).get(side).get(0).getData();
	}

	public HashMap<String, Double> getTail(String aisleId) {
		if (aisles.get(aisleId).get(RIGHT).isEmpty()) {
			int size = aisles.get(aisleId).get(LEFT).size();
			return aisles.get(aisleId).get(LEFT).get(size - 1).getData();
		} else {
			int size = aisles.get(aisleId).get(RIGHT).size();
			return aisles.get(aisleId).get(RIGHT).get(size - 1).getData();
		}

	}

	public HashMap<String, Double> getHead(String aisleId, String areaId){
		return getHead(aisleId, areaId, LEFT);
	}
	
	public HashMap<String, Double> getHead(String aisleId, String areaId, int side) {
		for (int i = 0; i < aisles.get(aisleId).get(side).size(); i++) {
			Node node = aisles.get(aisleId).get(side).get(i);
			String _areaId = node.getId().substring(0, 1);
			if(areaId.equals(_areaId)) {
				return node.getData();
			}
		}
		
		return null;
	}
	
	public HashMap<String, Double> getTail(String aisleId, String areaId) {
		int side = RIGHT;
		if (aisles.get(aisleId).get(RIGHT).isEmpty()) {
			side = LEFT;
		}
		for (int i = aisles.get(aisleId).get(side).size() - 1; i >= 0; i--) {
			Node node = aisles.get(aisleId).get(side).get(i);
			String _areaId = node.getId().substring(0, 1);
			if(areaId.equals(_areaId)) {
				return node.getData();
			}
		}
		
		return null;
	}

	
	
	@Override
	public String toString() {
		return "Aisles [aisles=" + aisles + "]";
	}



	private Comparator<Node> compareY = new Comparator<Node>() {
		@Override
		public int compare(Node o1, Node o2) {
			double y1 = o1.getData().get(Y);
			double y2 = o2.getData().get(Y);
			if (y1 < y2)
				return -1;
			else if (y1 > y2)
				return 1;
			else
				return 0;
		}
	};

	private Comparator<Node> compareYReverse = new Comparator<Node>() {
		@Override
		public int compare(Node o1, Node o2) {
			double y1 = o1.getData().get(Y);
			double y2 = o2.getData().get(Y);
			if (y1 < y2)
				return 1;
			else if (y1 > y2)
				return -1;
			else
				return 0;
		}
	};

	public class Node{
		String id;
		HashMap<String, Double> data;
		public Node(String id, HashMap<String, Double> data) {
			super();
			this.id = id;
			this.data = data;
		}
		public String getId() {
			return id;
		}
		public void setId(String id) {
			this.id = id;
		}
		public HashMap<String, Double> getData() {
			return data;
		}
		public void setData(HashMap<String, Double> data) {
			this.data = data;
		}
		@Override
		public String toString() {
			return "[id=" + id + ", data=" + data + "]";
		}
		
		
		
	}
	
}
