package alg;

import java.io.File;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFColor;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import jodd.io.FileUtil;

/*
 * 讀取Excel Map
 * */
public class LoadMap {

	static String date = DateTimeFormatter.ofPattern("yyyyMMddHHmmss").format(LocalDateTime.now());
	static String inputPath = "D:" + File.separator + "前瞻計畫" + File.separator + "2018大AI計畫" + File.separator + "倉儲最短路徑"
			+ File.separator + "map_all.xlsx";
	static String outputPath = "D:" + File.separator + "前瞻計畫" + File.separator + "2018大AI計畫" + File.separator + "倉儲最短路徑"
			+ File.separator + "map_all-" + date + ".txt";

	static HashMap<String, HashMap<String, Double>> nodes = new HashMap<>();
	static HashMap<String, HashSet<String>> connects = new HashMap<>();

	public static void main(String[] args) {
		loadMap();
	}

	public static void loadMap() {
		System.out.println("讀取Excel中...");

		File file = new File(inputPath);
		if (!file.exists()) {
			System.err.println("file not found: " + inputPath);
			System.exit(1);
		}

		try {
			XSSFWorkbook workbook = new XSSFWorkbook(file);
			XSSFSheet sheet = workbook.getSheetAt(0);
			int maxY = sheet.getLastRowNum();
			double maxYValue = 0 - Double.MAX_VALUE;
			HashMap<String, ArrayList<String>> _connects = new HashMap<>();
			for (int y = 0; y < maxY; y++) {
				XSSFRow row = sheet.getRow(y);
				int maxX = row.getLastCellNum();
				//System.out.println(maxX +","+maxY);
				for (int x = 0; x < maxX; x++) {
					XSSFCell cell = row.getCell(x);
					if (cell != null) {
						String nodeName = cell.toString();
											
						if (!"".equals(nodeName) && nodeName != null) {
							//System.out.println(String.valueOf(nodeName)+","+x+", "+y);
							double xVal = x * 0.1;
							//double yVal = 0 - y * 0.1;
							double yVal = y * 0.1;
							if (y > maxYValue) {
								maxYValue = y;
							}
							HashMap<String, Double> node = new HashMap<>();
							node.put("x", xVal);
							node.put("y", yVal);
							nodes.put(nodeName, node);
							char laneCode = nodeName.charAt(1);
							if (Character.isLetter(laneCode)) {
								String lane = nodeName.substring(1, 2);
								if (!_connects.containsKey(lane)) {
									_connects.put(lane, new ArrayList<>());
								}
								_connects.get(lane).add(nodeName);
							}
							String fontColor = "";
							try {
								fontColor = cell.getCellStyle().getFont().getXSSFColor().getARGBHex();
							} catch (Exception e) {
								System.err.println("("+x+","+y+")");
								e.printStackTrace();
								System.exit(1);
							}


							if (!fontColor.equals("FF000000")) {

								if (!_connects.containsKey(fontColor)) {
									_connects.put(fontColor, new ArrayList<>());
								}
								_connects.get(fontColor).add(nodeName);
							}
						}
					}
					
				}
			}
			//workbook.close();
			System.out.println("讀取完畢");
			System.out.println("產生檔案中...");
			for (String key : _connects.keySet()) {
				for (String nodeName : _connects.get(key)) {
					if (!connects.containsKey(nodeName)) {
						connects.put(nodeName, new HashSet<>());
					}
					connects.get(nodeName).addAll(_connects.get(key));
				}
			}

			maxYValue *= 0.1;

			for (String name : nodes.keySet()) {
				//double yVal = nodes.get(name).get("y");
				//yVal += maxYValue;
				//nodes.get(name).put("y", yVal);
				StringBuilder stringBuilder = new StringBuilder();
				if (connects.containsKey(name)) {
					for (String neibor : connects.get(name)) {
						stringBuilder.append("," + neibor);
					}
					stringBuilder.deleteCharAt(0);
				}

				FileUtil.appendString(outputPath, name + "," + String.format("%.2f", nodes.get(name).get("x")) + ","
						+ String.format("%.2f", nodes.get(name).get("y")) + ";" + stringBuilder.toString() + "\n");
//				System.out.println(name + "," + name + "," + String.format("%.2f", nodes.get(name).get("x")) + ","
//						+ String.format("%.2f", nodes.get(name).get("y")) + ";" + stringBuilder.toString() + "\n");
			}

			System.out.println("產生檔案:" + outputPath);

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
