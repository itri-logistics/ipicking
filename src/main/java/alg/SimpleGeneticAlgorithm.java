package alg;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

import common.utils.Utils;

/**
 * 
 * 基因演算法
 * 
 * 之後呼叫 @link #run()執行。
 * 
 * 其餘參數可透過相關set method設定。 Created by ATone on 2017/6/19
 * 
 */
public class SimpleGeneticAlgorithm {

	protected int threadNum = 8;

	// pc: cross over rate, 交配率
	// pm: mutation rate, 突變率
	private double pc = 0.5, pm = 0.1, tolerance = 1.0e-5;

	// 目標函數，產生新的Algorithm Object時需定義
	public TspFunction objectiveFunction;

	// maxGeneration: 最大迭代數，若小於0則迭代至最佳解出現為止; unchangeLimit:
	// 當最佳值維持不變的次數達到unchangeLimit時，停止迭代; populationSize: 母群的大小; populationNum:
	// 母群的數量;
	private int maxGeneration = 100000, unchangeLimit = 1000, populationSize = 500;

	// 停止時間
	private int stopTime = -1;

	// 目標值
	private double threshold = -1;

	// best: 目前的最佳解
	private String[] best;

	// 目前的母群（可能解）
	private ArrayList<String[]> population;

	private ArrayList<Integer> pool;

	private int bestChromosome; // 目前有最佳解的染色體index
	private double fmin = Double.MAX_VALUE; // 目前的最小值

	private double lastFmin;// 上一次的最小值

	// 目前各染色體的解代入目標函數之值
	private double[] fitness;

	// fitness總合
	private double fitnessSum;

	HashMap<String, HashMap<String, Double>> nodes = new HashMap<>();

	private String[] initialSolution;

	public SimpleGeneticAlgorithm(TspFunction f, String[] initialSolution) {
		this.objectiveFunction = f;
		this.initialSolution = initialSolution;
		this.best = initialSolution;
	}

	public double getPc() {
		return pc;
	}

	public void setPc(double pc) {
		this.pc = pc;
	}

	public double getPm() {
		return pm;
	}

	public void setPm(double pm) {
		this.pm = pm;
	}

	public double getTolerance() {
		return tolerance;
	}

	public void setTolerance(double tolerance) {
		this.tolerance = tolerance;
	}

	public TspFunction getObjectiveFunction() {
		return objectiveFunction;
	}

	public void setObjectiveFunction(TspFunction objectiveFunction) {
		this.objectiveFunction = objectiveFunction;
	}

	public int getMaxGeneration() {
		return maxGeneration;
	}

	public void setMaxGeneration(int maxGeneration) {
		this.maxGeneration = maxGeneration;
	}

	public int getUnchangeLimit() {
		return unchangeLimit;
	}

	public void setUnchangeLimit(int unchangeLimit) {
		this.unchangeLimit = unchangeLimit;
	}

	public int getPopulationSize() {
		return populationSize;
	}

	public void setPopulationSize(int populationSize) {
		this.populationSize = populationSize;
	}

	public int getStopTime() {
		return stopTime;
	}

	public void setStopTime(int stopTime) {
		this.stopTime = stopTime;
	}

	public double getThreshold() {
		return threshold;
	}

	public void setThreshold(double threshold) {
		this.threshold = threshold;
	}

	public String[] getBest() {
		return best;
	}

	public void setBest(String[] best) {
		this.best = best;
	}

	public ArrayList<String[]> getPopulation() {
		return population;
	}

	public void setPopulation(ArrayList<String[]> population) {
		this.population = population;
	}

	public int getBestChromosome() {
		return bestChromosome;
	}

	public void setBestChromosome(int bestChromosome) {
		this.bestChromosome = bestChromosome;
	}

	public double getFmin() {
		return fmin;
	}

	public void setFmin(double fmin) {
		this.fmin = fmin;
	}

	public int getThreadNum() {
		return threadNum;
	}

	public void setThreadNum(int threadNum) {
		this.threadNum = threadNum;
	}

	// 產生初始解
	public void initialSolution() {

		population = new ArrayList<String[]>();

		// 亂數排序
		for (int ii = 1; ii < populationSize; ii++) {
			String[] newSol = initialSolution.clone();
			Random random = new Random();
			int index;

			for (int j = 1; j < (newSol.length - 1) / 2; j++) {
				do {
					index = random.nextInt(newSol.length - 2) + 1;
				} while (index == 0 || index == newSol.length - 1);
				String tmp = newSol[j];
				newSol[j] = newSol[index];
				newSol[index] = tmp;
			}

			population.add(newSol);
		}

		population.add(initialSolution.clone());

		System.out.println(new Date().toString() + "初始解已建立");

	}

	// 找出目前最好的染色體
	protected void findBestChromosome(ArrayList<String[]> populationNew) {
		boolean c = false;
		fitnessSum = 0;
		// double[] newFitness = new double[populationSize];
		for (int i = 0; i < populationSize; i++) {
			double tmp = objectiveFunction.cal(populationNew.get(i));
			// newFitness[i] = tmp;
			// System.out.println(tmp+", "+fitness[i]);
			if (tmp < fitness[i]) // 如果新的解比較好
			{
				population.set(i, populationNew.get(i).clone()); // 取代掉舊的解

				fitness[i] = tmp;
			}

			if (fitness[i] < fmin) // 如果目前解比目前的最佳解好
			{
				fmin = fitness[i]; // 設為目前最佳解
				bestChromosome = i;
				c = true;
			}
			fitnessSum += tmp;
			// System.err.println(fmin);
		}

		if (c) {
			// 目前最佳解
			best = population.get(bestChromosome).clone();
		}
	}

	protected void printCurrentSol(String algName, long startTime) {
		System.out.println(algName + "－第" + iterN + "次迭代:\t" + fmin + "\t執行時間："
				+ (System.currentTimeMillis() - startTime) / 1000.0 + "秒。");

	}

	// ====== Genetic Algorithm ======
	int iterN = 0;// 迭代次數

	public String[] run() throws InterruptedException {

		if (best.length <= 2) {
			fmin = objectiveFunction.cal(best);
			String[] result = new String[] { new Date().toString() + "搜尋結束。",
					"執行時間：0秒。", "迭代次數為 = 0",
					"最佳解為 = \n" + Arrays.toString(best), "其值為 = " + fmin };

			for (String s : result) {
				System.out.println(s);
			}

			return best;
		}

		// 找出目前最好的解
		fitness = new double[populationSize];
		fitnessSum = 0;

		for (int i = 0; i < populationSize; i++) {
			fitness[i] = objectiveFunction.cal(population.get(i));

			fitnessSum += fitness[i];
			if (fitness[i] < fmin) {
				fmin = fitness[i];
				bestChromosome = i;
			}

		}

		best = population.get(bestChromosome);

		// 開始迭代
		// 執行起始時間
		long startTime = System.currentTimeMillis();
		Iteration iteration = new Iteration(startTime);
		Thread iterationThread = new Thread(iteration);
		iterationThread.start();

		if (stopTime > 0) {
			try {
				Thread.sleep(stopTime);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			iteration.setFlag(true);
		}

		try {
			iterationThread.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		String[] result = new String[] { new Date().toString() + "搜尋結束。",
				"執行時間：" + (System.currentTimeMillis() - startTime) / 1000.0 + "秒。", "迭代次數為 = " + iterN,
				"最佳解為 = \n" + Arrays.toString(best), "其值為 = " + fmin };

		for (String s : result) {
			System.out.println(s);
		}

		return best;
	}

	class Iteration implements Runnable {

		long startTime;
		boolean flag = false;

		public Iteration(long startTime) {
			super();
			this.startTime = startTime;
		}

		public boolean isFlag() {
			return flag;
		}

		public void setFlag(boolean flag) {
			this.flag = flag;
		}

		@Override
		public void run() {

			// 迭代次數
			iterN = 0;

			int keep = 0;// 解的結果維持不變的次數
			System.out.println(new Date().toString() + "開始迭代...");
			// 開始迭代
			while (keep < unchangeLimit && iterN < maxGeneration) {

				// System.out.println(iterN + "/" + maxGeneration);

				// 複製，決定每個染色體複製到交配池的個數
				ArrayList<String[]> populationNew = null;
				reproduction();

				// 交配
				try {
					populationNew = crossover();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}

				// 突變
				try {
					populationNew = mutation(populationNew);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}

				if (populationNew != null) {
					// 驗整新的解，若比舊的解好，則取代舊的解
					findBestChromosome(populationNew);
				}

				if (iterN > 0 && iterN % 1000 == 0) {
					System.out.println("GA－第" + iterN + "/" + maxGeneration + "次迭代\t" + "執行時間："
							+ (System.currentTimeMillis() - startTime) / 1000.0 + "秒。" + "\t解維持代數：" + keep);
				}
				// 比較上次解和此次解的差異
				if (Math.abs(fmin - lastFmin) < tolerance) {
					keep++;
				} else {
					keep = 0;
					printCurrentSol("GA", startTime);
				}

				// System.out.println(fmin);

				// 更新上一次的解
				lastFmin = fmin;

				// 增加迭代次數
				iterN++;

				if (flag) {
					break;
				}
			}
		}
	}

	// ====== Genetic Algorithm所用到的 methods======
	// 複製
	private void reproduction() {
		int count;
		int slack = populationSize; // 剩餘可複製數
		pool = new ArrayList<Integer>();
		for (int i = 0; i < populationSize && slack > 0; i++) {
			// 此染色體應複製到交配池的個數
			count = (int) (1.5 + iterN / maxGeneration - (fitness[i] / fitnessSum));
			if (count > slack) {
				count = slack;
			}

			// 複製
			for (int j = 0; j < count; j++) {
				pool.add(i);
			}

			slack -= count;
		}

		// 交配池還有位置
		while (pool.size() < populationSize) {
			// 隨機選兩條染色體
			int i = Utils.dice(populationSize);
			int j = Utils.dice(populationSize);
			while (j == i) {
				j = Utils.dice(populationSize);
			}

			// 比較好的丟進交配池
			if (fitness[i] < fitness[j]) {
				pool.add(i);
			} else {
				pool.add(j);
			}
		}

	}

	// 交配
	private ArrayList<String[]> crossover() throws InterruptedException {
		ArrayList<String[]> populationNew = new ArrayList<String[]>();
		double prob;
		int csNum = 0; // 交配次數

		// 決定交配次數
		for (int i = 0; i < populationSize / 2; i++) {
			prob = Math.random();
			if (prob < pc) {
				csNum++;
			}
		}

		// 交配
		CrossOverRunnable runnable[] = new CrossOverRunnable[threadNum];
		Thread crossOverThread[] = new Thread[threadNum];
		for (int i = 0; i < threadNum; i++) {
			runnable[i] = new CrossOverRunnable(csNum / threadNum);
			crossOverThread[i] = new Thread(runnable[i], iterN + "crossOver" + i);
			crossOverThread[i].start();
		}

		for (int i = 0; i < threadNum; i++) {
			crossOverThread[i].join();
		}

		for (int i = 0; i < threadNum; i++) {
			populationNew.addAll(runnable[i].getChromosomeList());
		}

		int p;
		// 填滿
		while (populationNew.size() < populationSize) {
			p = pool.get(Utils.dice(pool.size()));
			populationNew.add(population.get(p).clone());
		}

		return populationNew;
	}

	class CrossOverRunnable implements Runnable {

		int csNum;
		ArrayList<String[]> chromosomeList;

		public CrossOverRunnable(int csNum) {
			this.csNum = csNum;
			this.chromosomeList = new ArrayList<String[]>();
		}

		@Override
		public void run() {
			int p1, p2;
			for (int ii = 0; ii < csNum; ii++) {
				// 隨機抽兩條染色體
				p1 = pool.get(Utils.dice(pool.size()));
				do {
					p2 = pool.get(Utils.dice(pool.size()));
				} while (p2 == p1);
				String[] parent1 = population.get(p1), parent2 = population.get(p2);

				ArrayList<String> c1 = new ArrayList<String>(Arrays.asList(parent1)),
						c2 = new ArrayList<String>(Arrays.asList(parent2));
				ArrayList<ArrayList<String>> cs;

				cs = OrderCrossOver(c1, c2);

				c1 = cs.get(0);
				c2 = cs.get(1);

				String[] child1, child2;
				try {
					child1 = parent2.clone();
					child2 = parent1.clone();
					child1 = c1.toArray(child1);
					child2 = c2.toArray(child2);
				} catch (Exception e) {
					e.printStackTrace();
					child1 = parent1;
					child2 = parent2;
				}

				chromosomeList.add(child1);
				chromosomeList.add(child2);
			}

		}

		public ArrayList<String[]> getChromosomeList() {
			return chromosomeList;
		}

	}

	private ArrayList<ArrayList<String>> OrderCrossOver(ArrayList<String> r1, ArrayList<String> r2) {
		int pos1, pos2;
		ArrayList<String> c1 = new ArrayList<String>(), c2 = new ArrayList<String>();
		int size = r1.size();
		if (size <= 1) {
			return new ArrayList<ArrayList<String>>(Arrays.asList(r1, r2));
		}

		pos1 = Utils.dice(size);

		do {
			pos2 = Utils.dice(size);
		} while (pos1 == pos2);

		int start = Math.min(pos1, pos2);
		int end = Math.max(pos1, pos2);

		c1.addAll(r1.subList(start, end));
		c2.addAll(r2.subList(start, end));

		int currentCityIndex = 0;
		String currentStopInR1, currentStopInR2;
		for (int i = 0; i < size; i++) {
			// get the index of the current city
			currentCityIndex = (end + i) % size;

			// get the city at the current index in each of the two parent tours
			currentStopInR1 = r1.get(currentCityIndex);
			currentStopInR2 = r2.get(currentCityIndex);

			if (!c1.contains(currentStopInR2)) {
				c1.add(currentStopInR2);
			}

			if (!c2.contains(currentStopInR1)) {
				c2.add(currentStopInR1);
			}
		}

		// rotate the lists so the original slice is in the same place as in
		// the parent tours
		Collections.rotate(c1, start);
		Collections.rotate(c2, start);

		if (r1.size() != c2.size() || r2.size() != c1.size()) {

			System.err.println(r1.size() + "," + r2.size() + " ; " + c1.size() + "," + c2.size());
			for (int i = c2.size() - 1; i >= 0; i--) {
				if (r1.contains(c2.get(i))) {
					c2.remove(i);
				}
			}

			for (int i = c1.size() - 1; i >= 0; i--) {
				if (r2.contains(c1.get(i))) {
					c1.remove(i);
				}
			}

			System.err.println(c2.toString());
			System.err.println(c1.toString());
			StringBuilder sb1 = new StringBuilder();
			StringBuilder sb2 = new StringBuilder();
			for (String s : r1) {
				sb1.append(s + ", ");
			}
			for (String s : r2) {
				sb2.append(s + ", ");
			}
			System.err.println(sb1.toString() + sb2.toString());
		}
		// copy the tours from the children back into the parents, because
		// crossover
		// functions are in-place!
		Collections.copy(r1, c2);
		Collections.copy(r2, c1);

		return new ArrayList<ArrayList<String>>(Arrays.asList(c1, c2));
	}

	// 突變
	private ArrayList<String[]> mutation(ArrayList<String[]> populationIn) throws InterruptedException {
		ArrayList<String[]> populationNew = new ArrayList<String[]>();
		ArrayList<String[]> mutationList = new ArrayList<String[]>();
		// 狀態向量：決定相對應的染色體是否會突變
		boolean[] mutate = new boolean[populationSize];
		double prob;
		for (int i = 0; i < populationSize; i++) {
			prob = Math.random();
			mutate[i] = prob < pm;
			if (mutate[i]) {
				mutationList.add(populationIn.get(i));
			}
		}

		int munNum = mutationList.size();

		MutationRunnable runnable[] = new MutationRunnable[threadNum];
		Thread mutationThread[] = new Thread[threadNum];

		for (int i = threadNum - 1; i >= 0; i--) {
			int end = i == threadNum - 1 ? munNum : (i + 1) * munNum / threadNum;
			runnable[i] = new MutationRunnable(mutationList.subList(i * (munNum / threadNum), end));
			mutationThread[i] = new Thread(runnable[i], iterN + "mutation" + i);
			mutationThread[i].start();
		}

		for (int i = 0; i < threadNum; i++) {
			mutationThread[i].join();
		}

		mutationList = new ArrayList<String[]>();
		for (int i = 0; i < threadNum; i++) {
			mutationList.addAll(runnable[i].getChromosomeList());
		}

		int j = 0;
		for (int i = 0; i < populationSize; i++) {
			if (mutate[i]) {
				populationNew.add(mutationList.get(j));
				j++;
			} else {
				populationNew.add(populationIn.get(i).clone());
			}
		}

		return populationNew;
	}

	class MutationRunnable implements Runnable {

		List<String[]> chromosomeList;

		public MutationRunnable(List<String[]> chromosomeList) {
			super();
			this.chromosomeList = chromosomeList;
		}

		public List<String[]> getChromosomeList() {
			return chromosomeList;
		}

		@Override
		public void run() {
			ArrayList<String[]> newList = new ArrayList<>();
			for (String[] chromosome : chromosomeList) {
				if (chromosome.length <= 1) {
					continue;
				}
				ArrayList<String> stops = new ArrayList<>(Arrays.asList(chromosome));

				stops = TwoOpt.twoOpt(stops);
				// Collections.shuffle(stops);
				newList.add(stops.toArray(new String[stops.size()]));

			}
			chromosomeList = newList;
		}

	}

	public static interface TspFunction {
		public double cal(String[] sol);
	}

}