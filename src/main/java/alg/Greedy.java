package alg;

import java.util.ArrayList;
import java.util.Arrays;

public class Greedy {
	
	protected int threadNum = Runtime.getRuntime().availableProcessors();

	String[] route;
	public CostFunction costFunction;	
	
	double cost;
	
	public String[] getRoute() {
		return route;
	}

	public void setRoute(String[] route) {
		this.route = route;
	}

	public CostFunction getCostFunction() {
		return costFunction;
	}

	public void setCostFunction(CostFunction costFunction) {
		this.costFunction = costFunction;
	}

	public Greedy(CostFunction costFunction, String[] route) {
		super();
		this.route = route;
		this.costFunction = costFunction;
	}
	
	public int getThreadNum() {
		return threadNum;
	}

	public void setThreadNum(int threadNum) {
		this.threadNum = threadNum;
	}

	public double getCost() {
		return cost;
	}

	public String[] run() {

		ArrayList<String> initialSol = new ArrayList<>(Arrays.asList(route));
		ArrayList<String> finalList = new ArrayList<>();
		
		String last = "";
		
		cost = 0;
		
		while (!initialSol.isEmpty()) {
			if(!last.equals("")) {
				finalList.add(last);
			}
			
			int tn = Math.min(threadNum, initialSol.size());
			Thread[] threads = new Thread[tn];
			FindDistances[] runnables = new FindDistances[tn];
			for (int i = 0; i < tn; i++) {
				runnables[i] = new FindDistances(last, initialSol, threadNum, initialSol.size(), i);
				threads[i] = new Thread(runnables[i], "greedy " + i);
				threads[i].start();
			}

			for (int i = 0; i < tn; i++) {
				try {
					threads[i].join();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}

			double minCost = Double.MAX_VALUE;
			for (int i = 0; i < tn; i++) {
				if (runnables[i].getMinCost() < minCost) {
					minCost = runnables[i].getMinCost();
					last = runnables[i].getStop();
				}
			}
			cost += minCost;
			initialSol.remove(last);
		}
		finalList.add(last);

		route = finalList.toArray(new String[finalList.size()]);
		
		return route;
	}

	class FindDistances implements Runnable {

		String stop;

		ArrayList<String> stopList;

		int step, n, k;
		double minCost = Double.MAX_VALUE;

		public FindDistances(String stop, ArrayList<String> stopList, int step, int n, int k) {
			super();
			this.stop = stop;
			this.stopList = stopList;
			this.step = step;
			this.n = n;
			this.k = k;
		}

		@Override
		public void run() {
			int minIndex = k;
			for (int i = k; i < n; i += step) {
				double cost = costFunction.cal(stop, stopList.get(i));
				if (cost < minCost) {
					minCost = cost;
					minIndex = i;
				}
			}
			stop = stopList.get(minIndex);
		}

		public String getStop() {
			return stop;
		}

		public double getMinCost() {
			return minCost;
		}

	}

	public static interface CostFunction {
		public double cal(String from, String to);
	}
	
}
