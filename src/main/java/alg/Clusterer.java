package alg;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;

public abstract class Clusterer {
	protected ArrayList<HashMap<String, Double>> centroids;
	protected ArrayList<HashMap<String, Double>> instanceList;
	protected int[] assignments;
	protected int maxIteration = 500;
	protected Set<String> keySet;
	
	public Clusterer(Set<String> keySet, ArrayList<HashMap<String, Double>> instanceList) {
		super();
		this.instanceList = instanceList;
		this.keySet = keySet;
	}

	public ArrayList<HashMap<String, Double>> getCentroids() {
		return centroids;
	}

	public void setCentroids(ArrayList<HashMap<String, Double>> centroids) {
		this.centroids = centroids;
	}

	public ArrayList<HashMap<String, Double>> getInstanceList() {
		return instanceList;
	}

	public void setInstanceList(ArrayList<HashMap<String, Double>> instanceList) {
		this.instanceList = instanceList;
	}

	public int[] getAssignments() {
		return assignments;
	}

	public void setAssignments(int[] assignments) {
		this.assignments = assignments;
	}

	public int getMaxIteration() {
		return maxIteration;
	}

	public void setMaxIteration(int maxIteration) {
		this.maxIteration = maxIteration;
	}
	
	abstract public void buildClusterer();
	protected double calDistance(HashMap<String, Double> instance1, HashMap<String, Double> instance2){
		double d = 0.0;
		for (String key : keySet) {
			double v1 = instance1.get(key);
			double v2 = instance2.get(key);
			d += Math.pow(v1 - v2, 2);
		}
		return Math.sqrt(d);
	};
}
