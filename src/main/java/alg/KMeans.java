package alg;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;
import java.util.Set;

import common.utils.Utils;

/**
 * k means 演算法 分群用
 * 
 * 初始中心點決定方法以k means++實作
 * 
 * created by ATone 2017/05/08
 * 
 */

public class KMeans extends Clusterer{

	protected int clusterNum;
	protected double tolerance = 1.0e-5;

	public KMeans(ArrayList<HashMap<String, Double>> instanceList, int clusterNum) {
		super(instanceList.get(0).keySet(), instanceList);		
		this.clusterNum = clusterNum;
	}
	
	public KMeans(Set<String> keySet, ArrayList<HashMap<String, Double>> instanceList, int clusterNum) {
		super(keySet, instanceList);
		this.clusterNum = clusterNum;
	}

	public int getClusterNum() {
		return clusterNum;
	}

	public void setClusterNum(int clusterNum) {
		this.clusterNum = clusterNum;
	}

	public double getTolerance() {
		return tolerance;
	}

	public void setTolerance(double tolerance) {
		this.tolerance = tolerance;
	}

	@Override
	public void buildClusterer() {

		getInitialCentroid();

		int itr = 0;
		double diff = Double.MAX_VALUE;
		ArrayList<HashMap<String, Double>> m_centroids;
		int[] m_clusterSize = new int[clusterNum];		
		int n = instanceList.size();
		assignments = new int[n];

		while (itr < maxIteration && diff > tolerance) {
			
			m_centroids = new ArrayList<>();
			for (int j = 0; j < clusterNum; j++) {
				HashMap<String, Double> map = new HashMap<String, Double>();
				for (String key : keySet) {
					map.put(key, 0.0);
				}
				m_centroids.add(map);
				m_clusterSize[j] = 0;
			}
			
			// 分群
			for (int i = 0; i < n; i++) {
				HashMap<String, Double> ins = instanceList.get(i);
				double min = Double.MAX_VALUE;
				int cluster = -1;
				for (int j = 0; j < clusterNum; j++) {
					double d = calDistance(ins, centroids.get(j));
					if (d < min) {
						min = d;
						cluster = j;
					}else{
						if(cluster == -1){
							System.out.println("d = "+d+", min = "+min);
						}
					}
				}
				assignments[i] = cluster;
				for (String key : keySet) {
					min = m_centroids.get(cluster).get(key);
					m_centroids.get(cluster).put(key, min + ins.get(key));
				}
				m_clusterSize[cluster]++;
			}

			// 計算新的群中心
			diff = 0;
			for (int j = 0; j < clusterNum; j++) {
				HashMap<String, Double> cen = m_centroids.get(j);
				for (String key : keySet) {
					double v;
					if(m_clusterSize[j]<=0){
						cen.put(key, centroids.get(j).get(key));
					}else{
						v = cen.get(key);
						cen.put(key, v / m_clusterSize[j]);
					}
					
					v = calDistance(cen, centroids.get(j));
					diff += v;
				}
			}

			centroids = m_centroids;
			itr++;
		}
	}

	private void getInitialCentroid() {
		centroids = new ArrayList<>();
		// 決定初始點
		int instanceNum = instanceList.size();
		int p = Utils.dice(instanceNum);
		ArrayList<Integer> c = new ArrayList<>();
		c.add(p);
		Random rand = new Random();
		while (c.size() < clusterNum) {
			HashMap<String, Double> iniCentroid = instanceList.get(p);
			double[] distance = new double[instanceNum];
			double totalDistance = 0;
			for (int i = 0; i < instanceNum; i++) {
				distance[i] = calDistance(iniCentroid, instanceList.get(i));
				totalDistance += distance[i];
			}

			for (int i = 0; i < instanceNum; i++) {
				if (rand.nextDouble() < (distance[i] / totalDistance) && !c.contains(i)) {
					p = i;
					break;
				}
			}
			c.add(p);
		}

		for (int i = 0; i < clusterNum; i++) {
			centroids.add(instanceList.get(c.get(i)));
		}

	}
	
}
