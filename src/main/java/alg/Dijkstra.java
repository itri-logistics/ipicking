package alg;

import java.util.Arrays;
import java.util.PriorityQueue;
import java.util.Stack;

public class Dijkstra {

	int nodeNum; // 圖中的點數
	private double[][] graph; // 圖每邊的權重，通常為距離
	private double[] distance; // 紀錄起點到各個點的最短路徑長度
	private int[] parent; // 記錄各個點在最短路徑樹上的父親是誰
	boolean visit[]; // 記錄各個點是不是已在最短路徑樹之中
	int source; // 起點的index

	public Dijkstra(double[][] graph, int source) {
		this.graph = graph;
		this.source = source;
		this.nodeNum = graph[0].length;
	}

	public void buildPath() {
		distance = new double[nodeNum];
		parent = new int[nodeNum];
		visit = new boolean[nodeNum];
		Arrays.fill(distance, Double.POSITIVE_INFINITY);
		Arrays.fill(parent, -1);
		Arrays.fill(visit, false);

		PriorityQueue<Node> priorityQueue = new PriorityQueue<>();

		distance[source] = 0;
		parent[source] = source;
		priorityQueue.add(new Node(source, distance[source]));

		for (int i = 0; i < nodeNum; i++) {
			int a = -1;

			while (!priorityQueue.isEmpty() && visit[a = priorityQueue.peek().idx]) {
				priorityQueue.poll();
			}

			if (a == -1) {
				break;
			}

			visit[a] = true;
			for (int b = 0; b < nodeNum; b++) {
				if (!visit[b] && distance[a] + graph[a][b] < distance[b]) {
					distance[b] = distance[a] + graph[a][b];
					parent[b] = a;

					// 交由Priority Queue比較大小
					priorityQueue.add(new Node(b, distance[b]));
				}
			}
		}
	}

	public Path findPath(int dest) {
		Stack<Integer> pathStack = new Stack<>();

		int a = dest;

		while (a != source && a >= 0) {
			pathStack.push(a);
			a = parent[a];
		}
		if (a >= 0)
			pathStack.push(a);

		int[] _path = new int[pathStack.size()];
		double[] _distance = new double[_path.length];

		for (int i = 0; i < _path.length; i++) {
			_path[i] = pathStack.pop();
			if (i == 0) {
				_distance[i] = 0;
			} else {
				try {
					_distance[i] = graph[_path[i - 1]][_path[i]];
				} catch (Exception e) {
					System.err.println("i = " + i + ", _path[i-1] = " + _path[i - 1] + ", _path[i] = " + _path[i]);
					e.printStackTrace();
					System.exit(1);
				}

			}
		}

		// System.out.println("路序：" + Arrays.toString(_path));
		// System.out.println("各點間距離：" + Arrays.toString(_distance));
		// System.out.println("總距離：" + distance[dest]);

		return new Path(_path, _distance, distance[dest]);
	}

	class Node implements Comparable<Node> {
		int idx; // 點在graph中的index
		double distance; // 可能的最短路徑長度

		public Node(int idx, double distance) {
			super();
			this.idx = idx;
			this.distance = distance;
		}

		@Override
		public int compareTo(Node o) {
			if (this.distance > o.distance) {
				return 1;
			} else if (this.distance < o.distance) {
				return -1;
			} else {
				return 0;
			}

		}
	}

	
}
