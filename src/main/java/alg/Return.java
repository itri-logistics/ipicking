package alg;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.TreeMap;

import jodd.io.FileUtil;

/*
 * 迴轉策略
 * */
public class Return {

	private String[] initialSolution;
	private String[] best;
	private ArrayList<Path> path;
	private HashMap<String, HashMap<String, Double>> nodes;
	private Aisles aisles;

	public Return(String[] initialSolution, HashMap<String, HashMap<String, Double>> nodes, Aisles aisles) {
		super();
		this.initialSolution = initialSolution;
		this.nodes = nodes;
		this.aisles = aisles;
	}

	public String[] getInitialSolution() {
		return initialSolution;
	}

	public void setInitialSolution(String[] initialSolution) {
		this.initialSolution = initialSolution;
	}

	public String[] getBest() {
		return best;
	}

	public void setBest(String[] best) {
		this.best = best;
	}

	public ArrayList<Path> getPath() {
		return path;
	}

	public void setPath(ArrayList<Path> path) {
		this.path = path;
	}

	public HashMap<String, HashMap<String, Double>> getNodes() {
		return nodes;
	}

	public void setNodes(HashMap<String, HashMap<String, Double>> nodes) {
		this.nodes = nodes;
	}

	public Aisles getAisles() {
		return aisles;
	}

	public void setAisles(Aisles aisles) {
		this.aisles = aisles;
	}

	public String[] run(String endNode) {

		TreeMap<String, HashMap<Integer, ArrayList<String>>> laneNode = new TreeMap<>();
		ArrayList<String> result = new ArrayList<>();
		ArrayList<Integer> _path = new ArrayList<>();
		ArrayList<Path> _pathes = new ArrayList<>();
		ArrayList<Double> _distance = new ArrayList<>();
		double totalDistance = 0;

		for (String s : initialSolution) {
			String lane = s.substring(1, 2);
			int code = Integer.parseInt(s.substring(2, s.length()));
			if (!laneNode.containsKey(lane)) {
				laneNode.put(lane, new HashMap<>());
				laneNode.get(lane).put(0, new ArrayList<>());
				laneNode.get(lane).put(1, new ArrayList<>());
			}

			laneNode.get(lane).get(code % 2).add(s);
		}
		
		if(laneNode.keySet().isEmpty()) {
			return best;
		}

		HashMap<String, Double> in;
		HashMap<String, Double> out = nodes.get("source");
		String inArea;
		String outArea = "A";
		String lastLane = laneNode.firstKey();
		for (String lane : laneNode.keySet()) {

			// 奇數進，偶數出
			ArrayList<String> inSideNodes = laneNode.get(lane).get(1);
			ArrayList<String> outSideNodes = laneNode.get(lane).get(0);
			
			in = aisles.getHead(lane);
			sort(inSideNodes, in, 1);
			sort(outSideNodes, in, -1);
			
			
			
			double d;
			
			if(inSideNodes.isEmpty()) {
				if(!result.isEmpty()) {
					// 上條走道的最後一點至上條走道的出口
					if (outArea.equals("B")) {
						in = aisles.getHead(lastLane, "B", Aisles.RIGHT);
					} else {
						in = aisles.getHead(lastLane, "A", Aisles.RIGHT);
					}

					_path.add(in.get("idx").intValue());
					d = distance(out, in);
					_distance.add(d);
					totalDistance += d;

					out = in;
				}else {
					_path.add(out.get("idx").intValue());
					_distance.add(0.0);
				}
				

				// 上條走道的出口至本次走道的入口
				if (outArea.equals("B")) {
					in = aisles.getHead(lane, "B");
				} else {
					in = aisles.getHead(lane, "A");
				}

				_path.add(in.get("idx").intValue());
				d = distance(out, in);
				_distance.add(d);
				totalDistance += d;

				out = in;
			}else {
				
				inArea = inSideNodes.get(0).substring(0, 1);
				if(!result.isEmpty()) {
					// 上條走道的最後一點至上條走道的出口
					if (outArea.equals("B") && inArea.equals("B")) {
						in = aisles.getHead(lastLane, "B", Aisles.RIGHT);
					} else {
						in = aisles.getHead(lastLane, "A", Aisles.RIGHT);
					}

					_path.add(in.get("idx").intValue());
					d = distance(out, in);
					_distance.add(d);
					totalDistance += d;

					out = in;
				}else {
					_path.add(out.get("idx").intValue());
					_distance.add(0.0);
				}
				

				// 上條走道的出口至本次走道的入口
				if (outArea.equals("B") && inArea.equals("B")) {
					in = aisles.getHead(lane, "B");
				} else {
					in = aisles.getHead(lane, "A");
				}

				_path.add(in.get("idx").intValue());
				d = distance(out, in);
				_distance.add(d);
				totalDistance += d;

				out = in;
			}
			
			

			// 奇數邊			
			for (String nid : inSideNodes) {
				in = nodes.get(nid);
				_path.add(in.get("idx").intValue());
				d = distance(out, in);
				_distance.add(d);
				totalDistance += d;
				_pathes.add(new Path(_path.stream().mapToInt(Integer::intValue).toArray(),
						_distance.stream().mapToDouble(Double::doubleValue).toArray(), totalDistance));
				_path = new ArrayList<>();
				_distance = new ArrayList<>();
				_path.add(in.get("idx").intValue());
				_distance.add(0.0);
				totalDistance = 0;
				out = in;
			}

			result.addAll(inSideNodes);
			if(!inSideNodes.isEmpty()) {
				outArea = inSideNodes.get(inSideNodes.size() - 1).substring(0, 1);
			}else {
				outArea = "A";
			}
			
			if(!outSideNodes.isEmpty()) {
				inArea = outSideNodes.get(0).substring(0, 1);
			}else {
				inArea = "A";
			}

			// 奇數邊揀完後，往回揀偶數邊
			for (String nid : outSideNodes) {
				in = nodes.get(nid);
				_path.add(in.get("idx").intValue());
				d = distance(out, in);
				_distance.add(d);
				totalDistance += d;
				_pathes.add(new Path(_path.stream().mapToInt(Integer::intValue).toArray(),
						_distance.stream().mapToDouble(Double::doubleValue).toArray(), totalDistance));
				_path = new ArrayList<>();
				_distance = new ArrayList<>();
				_path.add(in.get("idx").intValue());
				_distance.add(0.0);
				totalDistance = 0;
				out = in;
			}

			result.addAll(outSideNodes);

			// 更新結束區域
			if(!outSideNodes.isEmpty()) {
				outArea = outSideNodes.get(outSideNodes.size() - 1).substring(0, 1);
			}else {
				outArea = "A";
			}
			
			lastLane = lane;

		}

		// 上條走道的最後一點至上條走道的出口
		
		in = aisles.getHead(lastLane, "A", Aisles.RIGHT);		

		_path.add(in.get("idx").intValue());
		double d = distance(out, in);
		_distance.add(d);
		totalDistance += d;

		// 到終點
		out = in;
		in = nodes.get(endNode);
		if(in==null) {
			System.err.println(endNode);			
			try {
				FileUtil.appendString("D:\\前瞻計畫\\2018大AI計畫\\倉儲最短路徑\\找不到暫存區.txt", endNode+"\n");
			} catch (IOException e) {
				e.printStackTrace();
			}
			endNode = "source";
			in = nodes.get(endNode);
		}
		_path.add(in.get("idx").intValue());
		d = distance(out, in);
		_distance.add(d);
		totalDistance += d;

		_pathes.add(new Path(_path.stream().mapToInt(Integer::intValue).toArray(),
				_distance.stream().mapToDouble(Double::doubleValue).toArray(), totalDistance));
		best = result.toArray(new String[result.size()]);
		path = _pathes;

		return best;
	}

	private void sort(ArrayList<String> _nodes, HashMap<String, Double> in, int acs) {
		_nodes.sort(new Comparator<String>() {
			@Override
			public int compare(String o1, String o2) {
				double d1 = distance(in, nodes.get(o1));
				double d2 = distance(in, nodes.get(o2));
				if (d1 > d2)
					return 1 * acs;
				else if (d1 < d2)
					return -1 * acs;
				else
					return 0;
			}
		});
	}

	private static double distance(HashMap<String, Double> node1, HashMap<String, Double> node2) {
		return Math.hypot(node1.get("x") - node2.get("x"), node1.get("y") - node2.get("y"));
	}
}
