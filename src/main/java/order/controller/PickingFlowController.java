package order.controller;

import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import param.Pa;
import param.RsMsg;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttribute;

import jodd.util.StringUtil;
import order.dao.PickingFlowDao;
import order.service.PickingOrderService;

/***
 * 訂單資料
 * 
 * 套件:order
 * 
 * @author ATone create by 2017/06/12
 **/

@Controller
@RequestMapping(value = "/pickingFlow")
public class PickingFlowController {

	@Autowired
	PickingFlowDao pickingFlowDao;

	@RequestMapping()
	public String loadPage(@SessionAttribute(name = "lang", required = false) String lang, HttpServletRequest request,
			HttpSession session, ModelMap model) {
		if (StringUtil.isBlank(lang)) {
			Locale locale = request.getLocale();
			lang = locale.getLanguage();
			if (lang.equals(new Locale("zh").getLanguage())) {
				lang = "tw";
			} else {
				lang = "en";
			}
			session.setAttribute("lang", lang);
		} else {
			// System.out.println(lang);
		}

		return lang + "/order/pickingFlowView";
	}
	
	

	// 取得揀貨路徑
	@RequestMapping(value = "/getPath", method = RequestMethod.GET)
	public @ResponseBody Object getPath(@RequestParam("warehouseId") String warehouseId,
			@RequestParam(value = "pickingDate", defaultValue = "") String date,
			@RequestParam(value = "createDate", defaultValue = "") String createDate,			
			@RequestParam(value = "storeId", defaultValue = "") String storeId, ModelMap model) {
		try {
			return new RsMsg(RsMsg.SUCCESS, PickingOrderService.getPath(warehouseId, date, storeId, createDate));
		} catch (Exception e) {
			e.printStackTrace();
			return new RsMsg("error", e);
		}
	}

	// 取得揀貨紀錄
	@RequestMapping(value = "/getData", method = RequestMethod.GET)
	public @ResponseBody Object getOrderData(@RequestParam("warehouseId") String warehouseId,
			@RequestParam(value = "pickingDate", defaultValue = "") String date,
			@RequestParam(value = "createDate", defaultValue = "") String createDate,
			@RequestParam(value = "storeId", defaultValue = "") String storeId, 
			@RequestParam(value = "companyAccount", defaultValue = "") String companyAccount, 
			ModelMap model) {
		try {

			return new RsMsg(RsMsg.SUCCESS,pickingFlowDao.find(warehouseId, date, createDate, storeId, companyAccount));

		} catch (Exception e) {
			e.printStackTrace();
			return new RsMsg("error", e);
		}
	}

	// 修改揀貨記錄
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public @ResponseBody Object updateOrderData(@RequestParam Map<String, Object> param, ModelMap model) {
		try {

			String pickingId = String.valueOf(param.get("pickingId"));
			String goodId = String.valueOf(param.get("goodId"));
			String warehouseId = String.valueOf(param.get("warehouseId"));
			String companyId = String.valueOf(param.get("companyId"));
			param.remove("pickingId");
			param.remove("goodId");
			param.remove("warehouseId");
			param.remove("companyId");
			pickingFlowDao.update(warehouseId, pickingId, goodId, companyId,param.keySet().toArray(new String[param.size()]),
					param.values().toArray(new String[param.size()]));

			return Pa.success;

		} catch (Exception e) {
			e.printStackTrace();
			return new RsMsg("error", e);
		}
	}

}
