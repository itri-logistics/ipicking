package order.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import param.Pa;
import param.RsMsg;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttribute;

import jodd.util.StringUtil;
import order.dao.OrderDataDao;
import order.dao.PickingOrderDao;
import order.service.PickingOrderService;
import security.SecurityController;

@Controller
@RequestMapping(value = "/pickingOrder")
public class PickingOrderController extends SecurityController {

	@Autowired
	PickingOrderDao pickingOrderDao;

	@Autowired
	OrderDataDao orderDataDao;

	@RequestMapping()
	public String loadPage(@SessionAttribute(name = "lang", required = false) String lang, HttpServletRequest request,
			HttpSession session, ModelMap model) {
		if (StringUtil.isBlank(lang)) {
			Locale locale = request.getLocale();
			lang = locale.getLanguage();
			if (lang.equals(new Locale("zh").getLanguage())) {
				lang = "tw";
			} else {
				lang = "en";
			}
			session.setAttribute("lang", lang);
		} else {
			// System.out.println(lang);
		}

		return lang + "/order/pickingOrderView";
	}

	// 取得揀貨單頭
	@RequestMapping(value = "/getData", method = RequestMethod.GET)
	public @ResponseBody Object getOrderData(@RequestParam(value = "warehouseId", defaultValue = "") String warehouseId,
			@RequestParam(value = "pickingDate", defaultValue = "") String date,
			@RequestParam(value = "createDate", defaultValue = "") String createDate,
			@RequestParam(value = "picker", defaultValue = "") String picker,
			@RequestParam(value = "storeId", defaultValue = "") String storeId,
			@RequestParam(value = "companyAccount", defaultValue = "") String companyAccount,
			ModelMap model) {
		try {

			if (date.contains("/")) {
				String[] dates = date.split("/");
				int y = Integer.parseInt(dates[0]);
				int m = Integer.parseInt(dates[1]);
				int d = Integer.parseInt(dates[2]);
				date = String.format("%04d", y) + String.format("%02d", m) + String.format("%02d", d);
			}

			System.out.println(date);

			return new RsMsg(RsMsg.SUCCESS, pickingOrderDao.find(warehouseId, date, createDate, storeId, picker, companyAccount));
		} catch (Exception e) {
			e.printStackTrace();
			return new RsMsg("error", e);
		}
	}

	// 取得揀貨單頭
	@RequestMapping(value = "/getDataPeriod", method = RequestMethod.GET)
	public @ResponseBody Object getDataPeriod(@RequestParam(value = "warehouseId", defaultValue = "") String warehouseId,
			@RequestParam(value = "from", defaultValue = "") String from,
			@RequestParam(value = "to", defaultValue = "") String to,
			@RequestParam(value = "state", defaultValue = "") String state, 
			@RequestParam(value = "companyAccount", defaultValue = "") String companyAccount, 
			ModelMap model) {
		try {
			// <option value="all">All</option>
			// <option value="0">Wait for Download</option>
			// <option value="1">Wait for Picking</option>
			// <option value="2">On Picking</option>
			// <option value="3">Finish</option>
			if (from.contains("/")) {
				String[] dates = from.split("/");
				int y = Integer.parseInt(dates[0]);
				int m = Integer.parseInt(dates[1]);
				int d = Integer.parseInt(dates[2]);
				from = String.format("%04d", y) + String.format("%02d", m) + String.format("%02d", d);
			}
			if (to.contains("/")) {
				String[] dates = to.split("/");
				int y = Integer.parseInt(dates[0]);
				int m = Integer.parseInt(dates[1]);
				int d = Integer.parseInt(dates[2]);
				to = String.format("%04d", y) + String.format("%02d", m) + String.format("%02d", d);
			}

			return new RsMsg(RsMsg.SUCCESS, pickingOrderDao.find(warehouseId, from, to, state, companyAccount));
		} catch (Exception e) {
			e.printStackTrace();
			return new RsMsg("error", e);
		}
	}

	// 截單用，產生揀貨單
	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public @ResponseBody Object createPickingOrder(@RequestParam("warehouseId") String warehouseId,
			@RequestParam(value = "storeList", defaultValue = "") String storeList,
			@RequestParam(value = "pickingDate", defaultValue = "") String pickingDate, 
			@RequestParam(value = "companyAccount", defaultValue = "") String companyAccount, ModelMap model) {
		try {
			// {{date}}-{{storeId}}-{{goodId}},{{goodId}},{{goodId}}...;
			String creator = getUpdator();

			ArrayList<Map<String, Object>> orders = new ArrayList<>();

			if (StringUtils.isNotBlank(storeList)) {
				String[] _storeList = storeList.split(";");

				HashMap<String, ArrayList<String>> stores = new HashMap<>();

				for (String _store : _storeList) {

					String[] pickings = _store.split("-");
					List<Map<String, Object>> _orders = orderDataDao.find(warehouseId, pickings[0], pickings[1],
							pickings[2].split(","), "0", companyAccount);
					orders.addAll(_orders);
				}

			}

			if (!orders.isEmpty()) {
				if (pickingDate.contains("/")) {
					String[] dates = pickingDate.split("/");
					int y = Integer.parseInt(dates[0]);
					int m = Integer.parseInt(dates[1]);
					int d = Integer.parseInt(dates[2]);
					pickingDate = String.format("%04d", y) + String.format("%02d", m) + String.format("%02d", d);
				}
				String cd = PickingOrderService.createPickingPath(warehouseId, orders, pickingDate, creator, companyAccount);
				OrderDataDao.close(orders);
				return new RsMsg(RsMsg.SUCCESS, cd);
			}

			System.out.println(orders.toString());

			return new RsMsg(RsMsg.SUCCESS, "");
		} catch (Exception e) {
			e.printStackTrace();
			return new RsMsg("error", e);
		}
	}

}
