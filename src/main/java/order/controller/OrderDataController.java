package order.controller;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import param.Pa;
import param.RsMsg;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttribute;
import org.springframework.web.multipart.MultipartFile;

import com.google.gson.Gson;

import jodd.util.StringUtil;
import order.dao.OrderDataDao;
import order.utils.DataConnectionUtils;
import security.SecurityController;

/***
 * 訂單資料
 * 
 * 套件:order
 * 
 * @author ATone create by 2017/06/12
 **/

@Controller
@RequestMapping(value = "/orderData")
public class OrderDataController extends SecurityController {

	@Autowired
	OrderDataDao orderDataDao;

	@Autowired
	Gson gson;

	@RequestMapping()
	public String loadPage(@SessionAttribute(name = "lang", required = false) String lang, HttpServletRequest request,
			HttpSession session, ModelMap model) {

		if (StringUtil.isBlank(lang) || lang == null) {
			Locale locale = request.getLocale();
			lang = locale.getLanguage();
			if (lang.equals(new Locale("zh").getLanguage())) {
				lang = "tw";
			} else {
				lang = "en";
			}
			session.setAttribute("lang", lang);
		} else {
			// System.out.println(lang);
		}

		return lang + "/order/orderDataView";
	}

	// 下載Excel檔範例
	@RequestMapping(value = "/getExample", method = RequestMethod.GET)
	@ResponseBody
	public FileSystemResource getVehicleExcelFile(HttpServletResponse response) {
		response.setContentType("application/vnd.ms-excel");
		response.setHeader("Content-Disposition", "attachment; filename=orders.xlsx");
		return new FileSystemResource(Pa.examplePath + File.separator + "orders.xlsx");
	}

	// 取得訂單紀錄
	@RequestMapping(value = "/getData", method = RequestMethod.GET)
	public @ResponseBody Object getOrderData(@RequestParam(value = "warehouseId", defaultValue = "") String warehouseId,
			@RequestParam(value = "state", defaultValue = "") String state,
			@RequestParam(value = "from", defaultValue = "") String from,
			@RequestParam(value = "to", defaultValue = "") String to, 
			@RequestParam("companyAccount") String company, @RequestParam("userAccount") String user,
			ModelMap model) {
		try {
			if (warehouseId.toLowerCase().equals("all")) {
				warehouseId = "";
			}
			from = from.replace("/", "");
			to = to.replace("/", "");
			return new RsMsg(RsMsg.SUCCESS, orderDataDao.find(warehouseId, from, to, state, company));

		} catch (Exception e) {
			e.printStackTrace();
			return new RsMsg("error", e);
		}
	}

	// 匯入訂單
	@RequestMapping(value = "/uploadData", method = RequestMethod.POST)
	public @ResponseBody Object uploadOrderData(@RequestParam("warehouseId") String warehouseId,
			@RequestParam("date") String date, @RequestParam("orderId") String orderId,
			@RequestParam("goodId") String goodId, @RequestParam("placeId") String placeId,
			@RequestParam("qty") String qty, @RequestParam("storeId") String storeId,
			@RequestParam("companyAccount") String company, @RequestParam("userAccount") String user,
			ModelMap model) {

		try {

			OrderDataDao.insert(warehouseId, date, orderId, goodId, placeId, qty, storeId, getUpdateDate(), "", "0",
					getUpdator());

			return Pa.success;

		} catch (Exception e) {
			e.printStackTrace();
			return new RsMsg("error", e);
		}
	}

	@RequestMapping(value = "/checkPosition", method = RequestMethod.POST)
	public @ResponseBody Object checkPosition(@RequestParam("warehouseId") String warehouseId,
			@RequestParam("companyAccount") String companyAccount, @RequestParam("userAccount") String userAccount,
			@RequestParam("file") MultipartFile file, @RequestParam("layoutId") String layoutId, ModelMap model) {

		try {

			String companyId = getCompanyId();
			String path = Pa.uploadPath + File.separator + companyId;

			String fileName = file.getOriginalFilename();

			if (fileName.endsWith(".xlsx")) {
				fileName = getUpdateDate() + ".xlsx";
			} else if (fileName.endsWith(".xls")) {
				fileName = getUpdateDate() + ".xls";
			} else {
				return new RsMsg(RsMsg.ERROR, "invalid file format.");
			}

			File dir = new File(path);
			if (!dir.exists()) {
				dir.mkdirs();
			}

			ArrayList<String> existDate = new ArrayList<>();

			if (!file.isEmpty()) {
				try {
					File outFile = new File(path + File.separator + fileName);
					if (!outFile.exists()) {
						outFile.createNewFile();
					}
					BufferedOutputStream os = new BufferedOutputStream(new FileOutputStream(outFile));
					FileCopyUtils.copy(file.getInputStream(), os);
					os.flush();
					os.close();

					existDate = OrderDataDao.checkPosition(warehouseId, layoutId, companyAccount, userAccount, outFile);

					outFile.delete();

				} catch (Exception e) {
					e.printStackTrace();
					return new RsMsg(RsMsg.ERROR, e.getMessage());
				}
			}

			return new RsMsg(RsMsg.SUCCESS, existDate);
		} catch (Exception e) {
			e.printStackTrace();
			return new RsMsg(RsMsg.ERROR, e);
		}

	}

	@RequestMapping(value = "/checkOrder", method = RequestMethod.POST)
	public @ResponseBody Object checkOrder(@RequestParam("warehouseId") String warehouseId,
			@RequestParam("companyAccount") String companyAccount, @RequestParam("userAccount") String userAccount,
			@RequestParam("file") MultipartFile file, @RequestParam("layoutId") String layoutId, ModelMap model) {

		try {

			String companyId = getCompanyId();
			String path = Pa.uploadPath + File.separator + companyId;

			String fileName = file.getOriginalFilename();

			if (fileName.endsWith(".xlsx")) {
				fileName = getUpdateDate() + ".xlsx";
			} else if (fileName.endsWith(".xls")) {
				fileName = getUpdateDate() + ".xls";
			} else {
				return new RsMsg(RsMsg.ERROR, "invalid file format.");
			}

			File dir = new File(path);
			if (!dir.exists()) {
				dir.mkdirs();
			}

			ArrayList<String> existDate = new ArrayList<>();

			if (!file.isEmpty()) {
				try {
					File outFile = new File(path + File.separator + fileName);
					if (!outFile.exists()) {
						outFile.createNewFile();
					}
					BufferedOutputStream os = new BufferedOutputStream(new FileOutputStream(outFile));
					FileCopyUtils.copy(file.getInputStream(), os);
					os.flush();
					os.close();

					existDate = OrderDataDao.checkFile(warehouseId, getUpdator(), getUpdateDate(), outFile, companyAccount);

					outFile.delete();

				} catch (Exception e) {
					e.printStackTrace();
					return new RsMsg(RsMsg.ERROR, e.getMessage());
				}
			}

			return new RsMsg(RsMsg.SUCCESS, existDate);
		} catch (Exception e) {
			e.printStackTrace();
			return new RsMsg(RsMsg.ERROR, e);
		}

	}

	@RequestMapping(value = "/uploadOrder", method = RequestMethod.POST)
	public @ResponseBody Object uploadOrder(@RequestParam("warehouseId") String warehouseId,
			@RequestParam("layoutId") String layoutId, @RequestParam("companyAccount") String companyAccount,
			@RequestParam("userAccount") String userAccount, @RequestParam("file") MultipartFile file, ModelMap model) {

		try {

			String companyId = companyAccount;
			String path = Pa.uploadPath + File.separator + companyId;

			String fileName = file.getOriginalFilename();

			if (fileName.endsWith(".xlsx")) {
				fileName = getUpdateDate() + ".xlsx";
			} else if (fileName.endsWith(".xls")) {
				fileName = getUpdateDate() + ".xls";
			} else {
				return new RsMsg(RsMsg.ERROR, "invalid file format.");
			}

			File dir = new File(path);
			if (!dir.exists()) {
				dir.mkdirs();
			}

			if (!file.isEmpty()) {
				try {
					File outFile = new File(path + File.separator + fileName);
					if (!outFile.exists()) {
						outFile.createNewFile();
					}
					BufferedOutputStream os = new BufferedOutputStream(new FileOutputStream(outFile));
					FileCopyUtils.copy(file.getInputStream(), os);
					os.flush();
					os.close();

					OrderDataDao.parsingFile(warehouseId, layoutId, companyAccount, userAccount, getUpdateDate(), outFile);

					// outFile.delete();

				} catch (Exception e) {
					e.printStackTrace();
					return new RsMsg(RsMsg.ERROR, e.getMessage());
				}
			}

			return Pa.success;
		} catch (Exception e) {
			e.printStackTrace();
			return new RsMsg(RsMsg.ERROR, e);
		}

	}

}
