package order.controller;

import java.time.Clock;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.apache.commons.lang.StringUtils;
import param.Pa;
import param.RsMsg;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import jodd.util.StringUtil;
import order.dao.PickingFlowDao;
import order.dao.PickingOrderDao;
import security.SecurityManager;

/***
 * APP流程用
 * 
 * 套件:order
 * 
 * @author ATone create by 2017/06/12
 **/

@Controller
@RequestMapping(value = "/appFlow")
public class AppFlowController {

	@Autowired
	PickingOrderDao pickingOrderDao;

	@Autowired
	PickingFlowDao pickingFlowDao;

	@Autowired
	SecurityManager securityManager;

	// 下載揀貨單
	@RequestMapping(value = "/download", method = RequestMethod.GET)
	public @ResponseBody Object downloadOrderData(
			@RequestParam(value = "warehouseId", defaultValue = "") String warehouseId,
			@RequestParam(value = "date", defaultValue = "") String date,
			@RequestParam(value = "picker", defaultValue = "") String picker,
			@RequestParam(value = "storeId", defaultValue = "") String storeId, 
			@RequestParam(value = "companyId", defaultValue = "") String companyId, 
			ModelMap model) {

		try {

			String downloadDate = DateTimeFormatter.ofPattern("yyyyMMddHHmmss").format(LocalDateTime.now(Clock.system(ZoneId.of("+8"))));

			// 取得訂單
			List<Map<String, Object>> orderList = (List<Map<String, Object>>) pickingOrderDao.find(warehouseId, date, "", storeId, "", companyId);
			Map<String, List<Map<String, Object>>> pickList = new TreeMap<>();
			List<Map<String, Object>> updateList = new ArrayList<>();
			for (Map<String, Object> order : orderList) {
				String dd = String.valueOf(order.get(PickingOrderDao.FIELD_ARRAY[PickingOrderDao.FIELD_DOWNLOAD_DATE]));
				if (StringUtils.isBlank(dd.trim()) || dd.toLowerCase().equals("null")) {
					String pickingWarehouseId = String
							.valueOf(order.get(PickingOrderDao.FIELD_ARRAY[PickingOrderDao.FIELD_WAREHOUSE_ID]));
					String pickingId = String
							.valueOf(order.get(PickingOrderDao.FIELD_ARRAY[PickingOrderDao.FIELD_PICKING_ID]));
					String pickingDate = String
							.valueOf(order.get(PickingOrderDao.FIELD_ARRAY[PickingOrderDao.FIELD_DATE]));
					String pickingStoreId = String
							.valueOf(order.get(PickingOrderDao.FIELD_ARRAY[PickingOrderDao.FIELD_STORE_ID]));
					String createDate = String
							.valueOf(order.get(PickingOrderDao.FIELD_ARRAY[PickingOrderDao.FIELD_CREATE_DATE]));
					List<Map<String, Object>> pickingFlowList = (List<Map<String, Object>>) pickingFlowDao.find(pickingWarehouseId, pickingDate,
							createDate,pickingStoreId, companyId);

					for (Map<String, Object> pickingFlow : pickingFlowList) {
						pickingFlow.put(PickingFlowDao.FIELD_ARRAY[PickingFlowDao.FIELD_DOWNLOAD_DATE], downloadDate);
						pickingFlow.put(PickingFlowDao.FIELD_ARRAY[PickingFlowDao.FIELD_PICKER], picker.isEmpty()? "user":picker);
					}

					PickingFlowDao.updateBatch(pickingFlowList);

					Collections.sort(pickingFlowList, new Comparator<Map<String, Object>>() {

						@Override
						public int compare(Map<String, Object> o1, Map<String, Object> o2) {
							int _o1 = Integer.parseInt(
									o1.get(PickingFlowDao.FIELD_ARRAY[PickingFlowDao.FIELD_PICKING_ORDER]).toString());
							int _o2 = Integer.parseInt(
									o2.get(PickingFlowDao.FIELD_ARRAY[PickingFlowDao.FIELD_PICKING_ORDER]).toString());

							if (_o1 > _o2) {
								return 1;
							} else if (_o1 < _o2) {
								return -1;
							} else {
								return 0;
							}
						}
					});

					pickList.put(pickingId, pickingFlowList);
					order.put(PickingOrderDao.FIELD_ARRAY[PickingOrderDao.FIELD_DOWNLOAD_DATE], downloadDate);
					order.put(PickingOrderDao.FIELD_ARRAY[PickingOrderDao.FIELD_PICKER], picker);

					updateList.add(order);
				}

			}

			if (!updateList.isEmpty()) {
				PickingOrderDao.updateBatch(updateList);
			}

			List<Map<String, Object>> result = new ArrayList<>();

			for (String key : pickList.keySet()) {
				Map<String, Object> entry = new LinkedHashMap<>();
				String[] tmp = key.split("-");
				entry.put("pickingId", key);
				entry.put("date", tmp[0]);
				entry.put("storeId", tmp[1]);				
				entry.put("warehouseId", tmp[2]);
				entry.put("pickingList", pickList.get(key));
				result.add(entry);
			}

			return result;

		} catch (Exception e) {
			e.printStackTrace();
			return new RsMsg("error", e);
		}
	}

	// 取得揀貨紀錄
	@RequestMapping(value = "/getOrderFlow", method = RequestMethod.GET)
	public @ResponseBody Object getOrderData(
			@RequestParam(value = "warehouseId", defaultValue = "") String warehouseId,
			@RequestParam(value = "date", defaultValue = "") String date,
			@RequestParam(value = "picker", defaultValue = "") String picker,
			@RequestParam(value = "storeId", defaultValue = "") String storeId, 
			@RequestParam(value = "companyId", defaultValue = "") String companyId, 
			ModelMap model) {

		try {

			// 取得訂單
			List<Map<String, Object>> orderList = (List<Map<String, Object>>) pickingOrderDao.find(warehouseId, date, "", storeId,
					picker, companyId);
			Map<String, List<Map<String, Object>>> pickList = new TreeMap<>();
			Map<String, Map<String, Object>> pickInfo = new HashMap<>();

			for (Map<String, Object> order : orderList) {

				String pickingWarehouseId = String
						.valueOf(order.get(PickingOrderDao.FIELD_ARRAY[PickingOrderDao.FIELD_WAREHOUSE_ID]));
				String pickingId = String
						.valueOf(order.get(PickingOrderDao.FIELD_ARRAY[PickingOrderDao.FIELD_PICKING_ID]));
				String pickingDate = String.valueOf(order.get(PickingOrderDao.FIELD_ARRAY[PickingOrderDao.FIELD_DATE]));
				String pickingStoreId = String
						.valueOf(order.get(PickingOrderDao.FIELD_ARRAY[PickingOrderDao.FIELD_STORE_ID]));
				String createDare = String
						.valueOf(order.get(PickingOrderDao.FIELD_ARRAY[PickingOrderDao.FIELD_CREATE_DATE]));
				List<Map<String, Object>> pickingFlowList = (List<Map<String, Object>>) pickingFlowDao.find(pickingWarehouseId, pickingDate,
						createDare,pickingStoreId, companyId);

				Collections.sort(pickingFlowList, new Comparator<Map<String, Object>>() {

					@Override
					public int compare(Map<String, Object> o1, Map<String, Object> o2) {
						int _o1 = Integer.parseInt(
								o1.get(PickingFlowDao.FIELD_ARRAY[PickingFlowDao.FIELD_PICKING_ORDER]).toString());
						int _o2 = Integer.parseInt(
								o2.get(PickingFlowDao.FIELD_ARRAY[PickingFlowDao.FIELD_PICKING_ORDER]).toString());

						if (_o1 > _o2) {
							return 1;
						} else if (_o1 < _o2) {
							return -1;
						} else {
							return 0;
						}
					}
				});
				
				Map<String, Object> info = new HashMap<>();
				
				info.put(PickingOrderDao.FIELD_ARRAY[PickingOrderDao.FIELD_DOWNLOAD_DATE], order.get(PickingOrderDao.FIELD_ARRAY[PickingOrderDao.FIELD_DOWNLOAD_DATE]));
				info.put(PickingOrderDao.FIELD_ARRAY[PickingOrderDao.FIELD_START_DATE], order.get(PickingOrderDao.FIELD_ARRAY[PickingOrderDao.FIELD_START_DATE]));
				info.put(PickingOrderDao.FIELD_ARRAY[PickingOrderDao.FIELD_FINISH_DATE], order.get(PickingOrderDao.FIELD_ARRAY[PickingOrderDao.FIELD_FINISH_DATE]));
				
				pickInfo.put(pickingId, info);
				pickList.put(pickingId, pickingFlowList);

			}

			List<Map<String, Object>> result = new ArrayList<>();

			for (String key : pickList.keySet()) {
				Map<String, Object> entry = new LinkedHashMap<>();
				String[] tmp = key.split("-");
				entry.put("pickingId", key);
				entry.put("date", tmp[0]);
				entry.put("storeId", tmp[1]);
				entry.put("warehouseId", tmp[2]);
				entry.put("downloadDate", pickInfo.get(key).get("downloadDate"));
				entry.put("startDate", pickInfo.get(key).get("startDate"));
				entry.put("finishDate", pickInfo.get(key).get("finishDate"));
				entry.put("pickingList", pickList.get(key));				
				result.add(entry);
			}

			return result;

		} catch (Exception e) {
			e.printStackTrace();
			return new RsMsg("error", e);
		}
	}

	// 開始揀貨
	@RequestMapping(value = "/startPicking", method = RequestMethod.POST)
	public @ResponseBody Object updateOrderData(
			@RequestParam(value = "warehouseId", defaultValue = "") String warehouseId,
			@RequestParam(value = "pickingId", defaultValue = "") String pickingId,
			@RequestParam(value = "goodId", defaultValue = "") String goodId,
			@RequestParam(value = "companyId", defaultValue = "") String companyId, 
			ModelMap model) {
		try {
			if(StringUtil.isBlank(warehouseId)) {
				String[] tmp = pickingId.split("-");
				warehouseId = tmp[2];
			}
			String date = DateTimeFormatter.ofPattern("yyyyMMddHHmmss").format(LocalDateTime.now(Clock.system(ZoneId.of("+8"))));
			Map<String, Object> param = new HashMap<>();
			param.put(PickingFlowDao.FIELD_ARRAY[PickingFlowDao.FIELD_START_DATE], date);
			pickingFlowDao.update(warehouseId, pickingId, goodId, companyId, param.keySet().toArray(new String[param.size()]),
					param.values().toArray(new String[param.size()]));

			
			Map<String, Object> flow = (Map<String, Object>) pickingFlowDao.find(warehouseId, pickingId, goodId, companyId);
			List<Map<String, Object>> orderList = (List<Map<String, Object>>) pickingOrderDao.find(warehouseId, String.valueOf(flow.get("date")),"",String.valueOf(flow.get("storeId")),"");
			Map<String, Object> order = orderList.get(0);
			if(String.valueOf(order.get("startDate")).equals("null") || StringUtil.isBlank(String.valueOf(order.get("startDate")))) {
				pickingOrderDao.update(warehouseId, String.valueOf(order.get("pickingId")), param.keySet().toArray(new String[param.size()]), param.values().toArray(new String[param.size()]));
			}
			return new RsMsg("success", flow);

		} catch (Exception e) {
			e.printStackTrace();
			return new RsMsg("error", e);
		}
	}

	// 結束揀貨
	@RequestMapping(value = "/finishPicking", method = RequestMethod.POST)
	public @ResponseBody Object updateOrderData(
			@RequestParam(value = "warehouseId", defaultValue = "") String warehouseId,
			@RequestParam(value = "pickingId", defaultValue = "") String pickingId,
			@RequestParam(value = "goodId", defaultValue = "") String goodId,
			@RequestParam(value = "last", defaultValue = "") String last, 
			@RequestParam(value = "companyId", defaultValue = "") String companyId, 
			ModelMap model) {
		try {
			if(StringUtil.isBlank(warehouseId)) {
				String[] tmp = pickingId.split("-");
				warehouseId = tmp[2];
			}
			String date = DateTimeFormatter.ofPattern("yyyyMMddHHmmss").format(LocalDateTime.now(Clock.system(ZoneId.of("+8"))));
			Map<String, Object> param = new HashMap<>();

			param.put(PickingFlowDao.FIELD_ARRAY[PickingFlowDao.FIELD_FINISH_DATE], date);
			pickingFlowDao.update(warehouseId, pickingId, goodId, companyId, param.keySet().toArray(new String[param.size()]),
					param.values().toArray(new String[param.size()]));

			if (last.equals("true")) {
				String[] pid = pickingId.split("-");
				String pickingOrderId = pid[0]+"-"+pid[1]+"-"+pid[2];
				pickingOrderDao.update(warehouseId, pickingOrderId, param.keySet().toArray(new String[param.size()]),
						param.values().toArray(new String[param.size()]));
			}

			return new RsMsg("success", pickingFlowDao.find(warehouseId, pickingId, goodId, companyId));

		} catch (Exception e) {
			e.printStackTrace();
			return new RsMsg("error", e);
		}
	}

}
