package order.dao;

import static jodd.util.StringPool.NULL;

import java.io.File;
import java.text.SimpleDateFormat;
import java.time.Clock;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import param.Pa;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import common.bean.ConnoDataSource;
import common.utils.LoggerController;
import jodd.io.FileUtil;
import jodd.util.ArraysUtil;
import order.utils.DataConnectionUtils;

@Service
public class OrderDataDao {

	private static JdbcTemplate jdbcTemplate = new JdbcTemplate(new ConnoDataSource().helloWorld());

	public static final String TABLE_NAME = "orders";
	public static final int FIELD_WAREHOUSE_ID = 0;
	public static final int FIELD_DATE = 1;
	public static final int FIELD_ORDER_ID = 2;
	public static final int FIELD_GOOD_ID = 3;
	public static final int FIELD_PLACE = 4;
	public static final int FIELD_QTY = 5;
	public static final int FIELD_STORE_ID = 6;
	public static final int FIELD_UPLOAD_DATE = 7;
	public static final int FIELD_UPLOADER = 8;
	public static final int FIELD_CLOSE_DATE = 9;
	public static final int FIELD_STATE = 10;
	public static final int FIELD_COMPANY_ID = 11;

	public static final String[] FIELD_ARRAY = { "warehouseId", "date", "orderId", "goodId", "placeId", "qty",
			"storeId", "uploadDate", "uploader", "closeDate", "state", "companyId" };
	public static final String FIELD_ARRAY_STRING = ArraysUtil.toString(FIELD_ARRAY);

	private static String[] findDuration(String from, String to) {
		System.out.println(from + " - " + to);
		StringBuilder sql = new StringBuilder("select * from " + TABLE_NAME + "_index where ");
		List query = new ArrayList();

		if (StringUtils.isNotBlank(from) && StringUtils.isNotBlank(to)) {
			String yymmFrom = from.substring(0, 6);
			String yymmTo = to.substring(0, 6);
			System.out.println(yymmFrom + " - " + yymmTo);
			if (yymmFrom.equals(yymmTo)) {
				return new String[] { yymmFrom };
			}
		} else {
			if (StringUtils.isNotBlank(from)) {
				sql.append("yyyymm >= ? and ");
				query.add(from);
			}

			if (StringUtils.isNotBlank(to)) {
				sql.append("yyyymm <= ? and ");
				query.add(to);
			}
		}				

		if (query.isEmpty()) {
			sql = sql.delete(sql.length() - 6, sql.length());
		} else {
			sql = sql.delete(sql.length() - 4, sql.length());
		}

		System.out.println(query.toString());
		System.out.println(sql);
		List<Map<String, Object>> list = jdbcTemplate.queryForList(sql.toString(), query.toArray());

		String[] idxs = new String[list.size()];
		for (int i = 0; i < list.size(); i++) {
			idxs[i] = list.get(i).get("yyyymm").toString();
		}

		return idxs;
	}

	public static List<Map<String, Object>> find(String warehouseId, String from, String to, String state, String companyId) {

		String[] idxs = findDuration(from, to);

		List<Map<String, Object>> list = new ArrayList<>();

		for (String yymm : idxs) {
			StringBuilder sql = new StringBuilder("select * from " + TABLE_NAME + "_" + yymm + " where ");
			List query = new ArrayList();

			if (StringUtils.isNotBlank(warehouseId)) {
				sql.append(FIELD_ARRAY[FIELD_WAREHOUSE_ID] + " = ? and ");
				query.add(warehouseId);
			}

			if (StringUtils.isNotBlank(state)) {
				sql.append(FIELD_ARRAY[FIELD_STATE] + " = ? and ");
				query.add(state);
			}

			if (StringUtils.isNotBlank(from) && to.equals(from)) {
				sql.append(FIELD_ARRAY[FIELD_DATE] + " = ? and ");
				query.add(from);
			} else {
				if (StringUtils.isNotBlank(from)) {
					sql.append(FIELD_ARRAY[FIELD_DATE] + " >= ? and ");
					query.add(from);
				}

				if (StringUtils.isNotBlank(to)) {
					sql.append(FIELD_ARRAY[FIELD_DATE] + " <= ? and ");
					query.add(to);
				}
			}
			
			if (StringUtils.isNotBlank(companyId)) {
				sql.append(FIELD_ARRAY[FIELD_COMPANY_ID] + " = ? and ");
				query.add(companyId);
			}

			if (query.isEmpty()) {
				sql = sql.delete(sql.length() - 6, sql.length());
			} else {
				sql = sql.delete(sql.length() - 4, sql.length());
			}

			System.out.println(query.toString());
			System.out.println(sql);
			try {
				list.addAll(jdbcTemplate.queryForList(sql.toString(), query.toArray()));
			} catch (Exception e) {
				// TODO: handle exception
			}

		}

		return list;
	}

	public List<Map<String, Object>> find(String warehouseId, String date, String storeId, String[] goodIds,
			String state, String companyId) {

		String yymm = date.substring(0, 6);

		StringBuilder sql = new StringBuilder("select * from " + TABLE_NAME + "_" + yymm + " where ");
		List query = new ArrayList();

		if (StringUtils.isNotBlank(warehouseId)) {
			sql.append(FIELD_ARRAY[FIELD_WAREHOUSE_ID] + " = ? and ");
			query.add(warehouseId);
		}

		if (StringUtils.isNotBlank(date)) {
			sql.append(FIELD_ARRAY[FIELD_DATE] + " = ? and ");
			query.add(date);
		}

		if (StringUtils.isNotBlank(storeId)) {
			sql.append(FIELD_ARRAY[FIELD_STORE_ID] + " = ? and ");
			query.add(storeId);
		}

		if (goodIds.length > 0) {
			sql.append(FIELD_ARRAY[FIELD_GOOD_ID] + " in (" + StringArrayToString(goodIds) + ") and ");
		}

		if (StringUtils.isNotBlank(state)) {
			sql.append(FIELD_ARRAY[FIELD_STATE] + " = ? and ");
			query.add(state);
		}

		if (StringUtils.isNotBlank(companyId)) {
			sql.append(FIELD_ARRAY[FIELD_COMPANY_ID] + " = ? and ");
			query.add(companyId);
		}
		
		if (query.isEmpty()) {
			sql = sql.delete(sql.length() - 6, sql.length());
		} else {
			sql = sql.delete(sql.length() - 4, sql.length());
		}
		//System.out.println(sql);
		//System.out.println(query.toString());
		List<Map<String, Object>> list = jdbcTemplate.queryForList(sql.toString(), query.toArray());
		//System.out.println(list);
		return list;
	}

	@Transactional
	public static void insert(String warehouseId, String date, String orderId, String goodId, String place,
			String orderQuantity, String storeId, String state, String closeDate, String uploadDate, String uploader) {
		String yymm = date.substring(0, 6);
		createTable(yymm);
		String condi = "insert " + TABLE_NAME + "_" + yymm + " (" + FIELD_ARRAY_STRING
				+ ") values (?,?,?,?,?,?,?,?,?,?)";
		jdbcTemplate.update(condi, new Object[] { warehouseId, date, orderId, goodId, place, orderQuantity, storeId,
				uploadDate, uploader, closeDate, state });
	}

	public static ArrayList<String> checkPosition(String warehouseId, String layoutId, String companyAccount,
			String userAccount, File file) {
		ArrayList<String> goodNotFound = new ArrayList<>();
		try {

			// 先檢查儲位配置
			File fileLayout = new File(Pa.mapPath + File.separator + warehouseId + File.separator + layoutId + ".txt");

			boolean ext = fileLayout.exists();

			if (!ext) {
				String token = DataConnectionUtils.getToken(companyAccount, userAccount);
				if (DataConnectionUtils.getLayout(token, warehouseId, layoutId)) {
					fileLayout = new File(
							Pa.mapPath + File.separator + warehouseId + File.separator + layoutId + ".txt");
					ext = true;
				}
			}

			if (ext) {

				String ss = FileUtil.readString(fileLayout);
				HashMap<String, String> posMap = new HashMap<>();

				String[] tmp = ss.split(";");
				for (String s : tmp) {
					String[] p = s.split(","); // goodId, placeId
					posMap.put(p[0], p[1]);
				}

				XSSFWorkbook workbook = new XSSFWorkbook(file);
				XSSFSheet sheet = workbook.getSheetAt(0);

				HashMap<String, ArrayList<Object[]>> insert = new HashMap<>();

				DataFormatter df = new DataFormatter();
				for (int i = 1; i < sheet.getPhysicalNumberOfRows(); i++) {

					XSSFRow row = sheet.getRow(i);
					String gid = df.formatCellValue(row.getCell(2));
					if (!posMap.containsKey(gid)) {
						goodNotFound.add("(" + i + ")" + gid);
					}
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return goodNotFound;
	}

	public static ArrayList<String> checkFile(String warehouseId, String uploader, String updateDate, File file, String companyId) {
		ArrayList<String> existDate = new ArrayList<>();
		try {
			DataFormatter df = new DataFormatter();
			SimpleDateFormat DtFormat = new SimpleDateFormat("yyyyMMdd");

			XSSFWorkbook workbook = new XSSFWorkbook(file);
			XSSFSheet sheet = workbook.getSheetAt(0);

			HashMap<String, ArrayList<Object[]>> insert = new HashMap<>();

			for (int i = 1; i < sheet.getPhysicalNumberOfRows(); i++) {

				XSSFRow row = sheet.getRow(i);
				String date = DtFormat.format(row.getCell(0).getDateCellValue()).toString();

				List<Map<String, Object>> exs = find(warehouseId, date, date, "", companyId);

				if (!exs.isEmpty()) {
					existDate.add(date);
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return existDate;
	}

	public static void parsingFile(String warehouseId, String layoutId, String companyAccount, String userAccount,
			String updateDate, File file) {

		// 0date 1store 2goodId 3place 4qty
		// { "warehouseId", "date", "orderId", "goodId", "placeId", "qty", "storeId",
		// "uploadDate", "uploader", state }

		try {
			// 先檢查儲位配置
			HashMap<String, String> posMap = new HashMap<>();
			File fileLayout = new File(Pa.mapPath + File.separator + warehouseId + File.separator + layoutId + ".txt");

			boolean ext = fileLayout.exists();

			if (!ext) {
				String token = DataConnectionUtils.getToken(companyAccount, userAccount);
				if (DataConnectionUtils.getLayout(token, warehouseId, layoutId)) {
					fileLayout = new File(
							Pa.mapPath + File.separator + warehouseId + File.separator + layoutId + ".txt");
					ext = true;
				}
			}

			if (ext) {

				String ss = FileUtil.readString(fileLayout);

				String[] tmp = ss.split(";");
				for (String s : tmp) {
					String[] p = s.split(","); // goodId, placeId
					posMap.put(p[0], p[1]);
				}

			}

			DataFormatter df = new DataFormatter();
			SimpleDateFormat DtFormat = new SimpleDateFormat("yyyyMMdd");

			XSSFWorkbook workbook = new XSSFWorkbook(file);
			XSSFSheet sheet = workbook.getSheetAt(0);

			HashMap<String, ArrayList<Object[]>> insert = new HashMap<>();

			for (int i = 1; i < sheet.getPhysicalNumberOfRows(); i++) {

				XSSFRow row = sheet.getRow(i);
				String date = DtFormat.format(row.getCell(0).getDateCellValue()).toString();
				String yymm = date.substring(0, 6);

				if (!insert.containsKey(yymm)) {
					insert.put(yymm, new ArrayList<>());
				}
				String goodId = df.formatCellValue(row.getCell(2));
				String placeId = posMap.get(goodId);
				if (placeId != null) {
					insert.get(yymm)
							.add(new Object[] { warehouseId, date, df.formatCellValue(row.getCell(1)), goodId, placeId,
									df.formatCellValue(row.getCell(3)), df.formatCellValue(row.getCell(1)), updateDate,
									userAccount, "", "0", companyAccount });
				}

			}

			for (String yymm : insert.keySet()) {
				insertBatch(insert.get(yymm), yymm);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	@Transactional
	public static void insertBatch(ArrayList<Object[]> orderList, String yymm) {
		// LoggerController.dLog.debug("insertOrderToSQLBatch(), " +
		// orderList.toString());
		System.out.println("insertOrderToSQLBatch(), " + orderList.size());
		createTable(yymm);
		String condi = "replace into " + TABLE_NAME + "_" + yymm + " (" + FIELD_ARRAY_STRING
				+ ") values (?,?,?,?,?,?,?,?,?,?,?,?)";
		try {
			if (!orderList.isEmpty()) {
				ArrayList<Object[]> insertList = new ArrayList<>();
				for (int i = 0; i < orderList.size(); i++) {
					insertList.add(orderList.get(i));
					if (insertList.size() == 10000 || i == orderList.size() - 1) {
						jdbcTemplate.batchUpdate(condi, insertList);
						System.out.println((i + 1) + "/" + orderList.size());
						insertList = new ArrayList<>();
						// Thread.sleep(500);
					}
				}
			}
		} catch (DuplicateKeyException e) {
			// LoggerController.eLog
			// .error("insert data list to " + TABLE_NAME + " Duplicate Key. " +
			// orderList.toString());
			// LoggerController.eLog.error(e.toString());
			// System.err.println("insert data list to " + TABLE_NAME + " Duplicate Key. " +
			// orderList.toString());
			// e.printStackTrace();
			System.err.println("重複");
		} catch (Exception e) {
			// LoggerController.eLog.error("insert data list to " + TABLE_NAME + " fail. " +
			// orderList.toString());
			System.err.println("insert data list to " + TABLE_NAME + "_" + yymm + " fail. " + toString(orderList));
			// LoggerController.eLog.error(e.toString());

			e.printStackTrace();
		}
	}

	public static void close(List<Map<String, Object>> orderList) {
		HashMap<String, ArrayList<Map<String, Object>>> update = new HashMap<>();
		String date = DateTimeFormatter.ofPattern("yyyyMMddHHmmss").format(LocalDateTime.now(Clock.system(ZoneId.of("+8"))));
		for (int i = 0; i < orderList.size(); i++) {
			orderList.get(i).put(FIELD_ARRAY[FIELD_STATE], 1);
			orderList.get(i).put(FIELD_ARRAY[FIELD_CLOSE_DATE], date);
			String yymm = orderList.get(i).get(FIELD_ARRAY[FIELD_DATE]).toString().substring(0, 6);
			if (!update.containsKey(yymm)) {
				update.put(yymm, new ArrayList<>());
			}
			update.get(yymm).add(orderList.get(i));
		}

		for (String yymm : update.keySet()) {
			updateBatch(orderList, yymm);
		}

	}

	@Transactional
	public static void updateBatch(List<Map<String, Object>> orderList, String yymm) {
		System.out.println("updateOrderToSQLBatch(), " + orderList.toString());
		createTable(yymm);
		String[] updateField = FIELD_ARRAY;
		String updateFieldStr = ArraysUtil.toString(updateField).replace(",", "=?,") + "=?";
		String condi = "update " + TABLE_NAME + "_" + yymm + " set " + updateFieldStr + " where "
				+ FIELD_ARRAY[FIELD_WAREHOUSE_ID] + "=? and " + FIELD_ARRAY[FIELD_DATE] + "=? and "
				+ FIELD_ARRAY[FIELD_ORDER_ID] + "=? and " + FIELD_ARRAY[FIELD_GOOD_ID] + "=? and " 
				+ FIELD_ARRAY[FIELD_COMPANY_ID] + "=?";
		try {
			if (!orderList.isEmpty()) {
				ArrayList<Object[]> insertList = new ArrayList<>();
				for (int i = 0; i < orderList.size(); i++) {
					Object[] entry = new Object[FIELD_ARRAY.length + 5];
					for (int j = 0; j < FIELD_ARRAY.length; j++) {
						entry[j] = orderList.get(i).get(FIELD_ARRAY[j]);
					}
					entry[entry.length - 5] = orderList.get(i).get(FIELD_ARRAY[FIELD_WAREHOUSE_ID]);
					entry[entry.length - 4] = orderList.get(i).get(FIELD_ARRAY[FIELD_DATE]);
					entry[entry.length - 3] = orderList.get(i).get(FIELD_ARRAY[FIELD_ORDER_ID]);
					entry[entry.length - 2] = orderList.get(i).get(FIELD_ARRAY[FIELD_GOOD_ID]);
					entry[entry.length - 1] = orderList.get(i).get(FIELD_ARRAY[FIELD_COMPANY_ID]);
					insertList.add(entry);
					if (insertList.size() == 500 || i == orderList.size() - 1) {
						jdbcTemplate.batchUpdate(condi, insertList);
						System.out.println((i + 1) + "/" + orderList.size());
						insertList = new ArrayList<>();
						Thread.sleep(500);
					}
				}
			}
		} catch (DuplicateKeyException e) {
			LoggerController.eLog.error(
					"update data list to " + TABLE_NAME + "_" + yymm + " Duplicate Key. " + orderList.toString());
			LoggerController.eLog.error(e.toString());
			System.err.println(
					"update data list to " + TABLE_NAME + "_" + yymm + " Duplicate Key. " + orderList.toString());
			e.printStackTrace();
		} catch (Exception e) {
			LoggerController.eLog
					.error("update data list to " + TABLE_NAME + "_" + yymm + " fail. " + orderList.toString());
			LoggerController.eLog.error(e.toString());
			e.printStackTrace();
		}
	}

	private static String toString(ArrayList<Object[]> orderList) {
		StringBuilder stb = new StringBuilder();

		for (Object[] oa : orderList) {
			stb.append("," + Arrays.toString(oa));
		}

		if (stb.length() > 0) {
			stb.deleteCharAt(0);
		}

		stb.insert(0, "[");
		stb.append("]");

		return stb.toString();
	}

	private String StringArrayToString(String[] array) {
		if (array == null) {
			return NULL;
		}
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < array.length; i++) {
			if (i != 0) {
				sb.append(',');
			}
			sb.append('\'').append(array[i]).append('\'');
		}
		return sb.toString();
	}

	private static void createTable(String yymm) {
		String sql = "CREATE TABLE IF NOT EXISTS `" + TABLE_NAME + "_" + yymm + "` ("
				+ "  `warehouseId` int(11) NOT NULL DEFAULT '0',"
				+ "  `date` varchar(8) NOT NULL DEFAULT '' COMMENT '理貨時間'," + "  `storeId` varchar(50) DEFAULT NULL,"
				+ "  `orderId` varchar(50) NOT NULL DEFAULT 'null'," + "  `goodId` varchar(50) NOT NULL DEFAULT 'null',"
				+ "  `qty` int(11) DEFAULT NULL," + "  `placeId` varchar(50) DEFAULT NULL,"
				+ "  `uploadDate` varchar(50) DEFAULT NULL," + "  `uploader` varchar(50) DEFAULT NULL,"
				+ "  `closeDate` varchar(50) DEFAULT NULL,"
				+ "  `state` int(11) NOT NULL DEFAULT '0' COMMENT '0: 未截單，1: 已截單',"
				+ "  `companyId` varchar(50) NOT NULL DEFAULT 'ITRI',"
				+ "  PRIMARY KEY (`orderId`,`goodId`,`date`,`warehouseId`,`companyId`)"
				+ ") ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='門市訂單';";
		jdbcTemplate.execute(sql);

		String condi = "insert ignore " + TABLE_NAME + "_index (yyyymm) values (?)";
		jdbcTemplate.update(condi, new Object[] { yymm });
	}

}
