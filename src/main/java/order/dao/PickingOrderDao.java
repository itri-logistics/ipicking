package order.dao;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import common.bean.ConnoDataSource;
import common.utils.LoggerController;
import jodd.util.ArraysUtil;

@Service
public class PickingOrderDao {

	private static JdbcTemplate jdbcTemplate = new JdbcTemplate(new ConnoDataSource().helloWorld());

	public static final String TABLE_NAME = "picking_orders";
	public static final int FIELD_WAREHOUSE_ID = 0;
	public static final int FIELD_PICKING_ID = 1;
	public static final int FIELD_DATE = 2;
	public static final int FIELD_STORE_ID = 3;
	public static final int FIELD_CREATE_DATE = 4;
	public static final int FIELD_DOWNLOAD_DATE = 5;
	public static final int FIELD_START_DATE = 6;
	public static final int FIELD_FINISH_DATE = 7;
	public static final int FIELD_CREATOR = 8;
	public static final int FIELD_PICKER = 9;
	public static final int FIELD_COMPANY_ID = 10;

	public static final String[] FIELD_ARRAY = { "warehouseId","pickingId", "date", "storeId", "createDate", "downloadDate",
			"startDate", "finishDate", "creator", "picker", "companyId" };

	public static final String FIELD_ARRAY_STRING = ArraysUtil.toString(FIELD_ARRAY);

	public Object find(String warehouseId, String from, String to,String state, String companyId) {
		StringBuilder sql = new StringBuilder("select * from " + TABLE_NAME + " where ");
		List query = new ArrayList();
		
		if (StringUtils.isNotBlank(warehouseId)) {
			sql.append(FIELD_ARRAY[FIELD_WAREHOUSE_ID] + " = ? and ");
			query.add(warehouseId);
		}
		
		if (StringUtils.isNotBlank(from) && to.equals(from)) {
			sql.append(FIELD_ARRAY[FIELD_DATE] + " = ? and ");
			query.add(from);
		} else {
			if (StringUtils.isNotBlank(from)) {
				sql.append(FIELD_ARRAY[FIELD_DATE] + " >= ? and ");
				query.add(from);
			}

			if (StringUtils.isNotBlank(to)) {
				sql.append(FIELD_ARRAY[FIELD_DATE] + " <= ? and ");
				query.add(to);
			}
		}
		
		if (StringUtils.isNotBlank(state)) {
			if(state.equals("0")) {
				sql.append(FIELD_ARRAY[FIELD_DOWNLOAD_DATE] + " = ? and ");
				query.add("");
			}else if(state.equals("1")) {
				sql.append(FIELD_ARRAY[FIELD_START_DATE] + " = ? and ");
				query.add("");
			}else if(state.equals("2")) {
				sql.append(FIELD_ARRAY[FIELD_FINISH_DATE] + " = ? and ");
				query.add("");
			}else if(state.equals("3")) {
				sql.append(FIELD_ARRAY[FIELD_FINISH_DATE] + " != ? and ");
				query.add("");
			}			
		}
		
		if (StringUtils.isNotBlank(companyId)) {
			sql.append(FIELD_ARRAY[FIELD_COMPANY_ID] + " = ? and ");
			query.add(companyId);
		}

		if (query.isEmpty()) {
			sql = sql.delete(sql.length() - 6, sql.length());
		} else {
			sql = sql.delete(sql.length() - 4, sql.length());
		}

		System.out.println("PickingOrder find: " + sql);
		List list = jdbcTemplate.queryForList(sql.toString(), query.toArray());

		return list;
	}
	
	public Object find(String warehouseId, String date, String createDate,String storeId, String picker, String companyId) {
		StringBuilder sql = new StringBuilder("select * from " + TABLE_NAME + " where ");
		List query = new ArrayList();
		
		if (StringUtils.isNotBlank(warehouseId)) {
			sql.append(FIELD_ARRAY[FIELD_WAREHOUSE_ID] + "=? and ");
			query.add(warehouseId);
		}
		
		if (StringUtils.isNotBlank(date)) {
			sql.append(FIELD_ARRAY[FIELD_DATE] + "=? and ");
			query.add(date);
			
		}
		
		if (StringUtils.isNotBlank(createDate)) {
			sql.append(FIELD_ARRAY[FIELD_CREATE_DATE] + "=? and ");
			query.add(createDate);
		}

		if (StringUtils.isNotBlank(picker)) {
			sql.append(FIELD_ARRAY[FIELD_PICKER] + "=? and ");
			query.add(picker);
		}

		if (StringUtils.isNotBlank(storeId)) {
			sql.append(FIELD_ARRAY[FIELD_STORE_ID] + "=? and ");
			query.add(storeId);
		}
		
		if (StringUtils.isNotBlank(companyId)) {
			sql.append(FIELD_ARRAY[FIELD_COMPANY_ID] + "=? and ");
			query.add(companyId);
		}


		if (query.isEmpty()) {
			sql = sql.delete(sql.length() - 6, sql.length());
		} else {
			sql = sql.delete(sql.length() - 4, sql.length());
		}

		System.out.println(sql);
		System.out.println(query);
		List list = jdbcTemplate.queryForList(sql.toString(), query.toArray());
//		List list = jdbcTemplate.queryForList("select * from " + TABLE_NAME);
//		System.out.println(list.toString());
//		if (StringUtils.isNotBlank(companyId)) {			
//			companyId = toUtf8(companyId);
//			StringBuilder stb = new StringBuilder();
//			for(int j = 0;j<companyId.length();j++) {
//				stb.append(", "+(int)companyId.charAt(j));
//			}
//			System.out.println(companyId+stb.toString());
//			for(int i = list.size()-1;i>=0;i--) {
//				String _com = toUtf8(String.valueOf(((Map)list.get(i)).get("companyId")));
//				if(!_com.equals(companyId)) {
//					list.remove(i);
//				}
//			}
//		}
//		
//		if (StringUtils.isNotBlank(date)) {
//			for(int i = list.size()-1;i>=0;i--) {
//				String _d = String.valueOf(((Map)list.get(i)).get("date"));
//				if(!_d.equals(date)) {
//					list.remove(i);
//				}
//			}
//		}
		
		//System.out.println(listTest.toString());
		System.out.println(list.toString());
		return list;
	}
	
	public static String toUtf8(String str) {
		try {
			return new String(str.getBytes("UTF-8"),"UTF-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		return str;
	}

	@Transactional
	public void update(String warehouseId, String pickingId, String[] fieldName, String[] value) {
		String updateFieldStr = ArraysUtil.toString(fieldName).replace(",", "=?,") + "=?";
		String condi = "update " + TABLE_NAME + " set " + updateFieldStr + " where " + FIELD_ARRAY[FIELD_WAREHOUSE_ID]
				+ "=? and "+FIELD_ARRAY[FIELD_PICKING_ID]
				+ "=?";
		System.out.println(condi);
		value = ArraysUtil.append(value, warehouseId);
		value = ArraysUtil.append(value, pickingId);
		jdbcTemplate.update(condi, value);
	}

	@Transactional
	public void update(String warehouseId, String pickingId, String date, String storeId, String createDate, String downloadDate,
			String startDate, String finishDate, String creator, String picker, String companyId) {
		String[] updateField = Arrays.copyOfRange(FIELD_ARRAY, 2, FIELD_ARRAY.length - 1);
		String updateFieldStr = ArraysUtil.toString(updateField).replace(",", "=?,") + "=?";
		String condi = "update " + TABLE_NAME + " set " + updateFieldStr + " where " + FIELD_ARRAY[FIELD_WAREHOUSE_ID]
				+ "=? and "+ FIELD_ARRAY[FIELD_PICKING_ID]+ "=? and "+ FIELD_ARRAY[FIELD_COMPANY_ID]
				+ "=?";
		System.out.println(condi);
		jdbcTemplate.update(condi, new Object[] { date, storeId, createDate, downloadDate, startDate, finishDate,
				creator, picker, warehouseId, pickingId, companyId});

	}

	@Transactional
	public static void insert(String warehouseId, String pickingId, String date, String storeId, String createDate, String downloadDate,
			String startDate, String finishDate, String creator, String picker, String companyId) {
		String[] insertField = FIELD_ARRAY;
		String condi = "insert " + TABLE_NAME + " (" + ArraysUtil.toString(insertField)
				+ ") values (?,?,?,?,?,?,?,?,?,?,?)";
		System.out.println(condi);
		jdbcTemplate.update(condi, new Object[] { warehouseId, pickingId, date, storeId, createDate, downloadDate, startDate,
				finishDate, creator, picker, companyId });
	}

	@Transactional
	public static void insertBatch(ArrayList<Object[]> orderList) {
		LoggerController.dLog.debug("insertOrderToSQLBatch(), " + orderList.toString());
		String[] insertField = FIELD_ARRAY;
		String condi = "replace into " + TABLE_NAME + " (" + ArraysUtil.toString(insertField)
				+ ") values (?,?,?,?,?,?,?,?,?,?,?)";
		try {
			if (!orderList.isEmpty()) {
				ArrayList<Object[]> insertList = new ArrayList<>();
				for (int i = 0; i < orderList.size(); i++) {
					insertList.add(orderList.get(i));
					if (insertList.size() == 500 || i == orderList.size() - 1) {
						jdbcTemplate.batchUpdate(condi, insertList);
						System.out.println(i + "/" + orderList.size());
						insertList = new ArrayList<>();
						Thread.sleep(500);
					}
				}
			}
		} catch (DuplicateKeyException e) {
			LoggerController.eLog
					.error("insert data list to " + TABLE_NAME + " Duplicate Key. " + orderList.toString());
			LoggerController.eLog.error(e.toString());
			e.printStackTrace();
		} catch (Exception e) {
			LoggerController.eLog.error("insert data list to " + TABLE_NAME + " fail. " + orderList.toString());
			LoggerController.eLog.error(e.toString());
			e.printStackTrace();
		}
	}

	@Transactional
	public static void updateBatch(List<Map<String, Object>> orderList) {
		LoggerController.dLog.debug("updateOrderToSQLBatch(), " + orderList.toString());
		String[] updateField = Arrays.copyOfRange(FIELD_ARRAY, 2, FIELD_ARRAY.length - 1);
		String updateFieldStr = ArraysUtil.toString(updateField).replace(",", "=?,") + "=?";
		String condi = "update " + TABLE_NAME + " set " + updateFieldStr + " where " + FIELD_ARRAY[FIELD_WAREHOUSE_ID]
				+ "=? and "+ FIELD_ARRAY[FIELD_PICKING_ID]
				+ "=? and "+ FIELD_ARRAY[FIELD_COMPANY_ID]
				+ "=?";
		try {
			if (!orderList.isEmpty()) {
				ArrayList<Object[]> insertList = new ArrayList<>();
				for (int i = 0; i < orderList.size(); i++) {
					Object[] entry = new Object[FIELD_ARRAY.length];
					for (int j = 2; j < FIELD_ARRAY.length; j++) {
						entry[j - 2] = orderList.get(i).get(FIELD_ARRAY[j]);
					}
					entry[FIELD_ARRAY.length - 3] = orderList.get(i).get(FIELD_ARRAY[FIELD_WAREHOUSE_ID]);
					entry[FIELD_ARRAY.length - 2] = orderList.get(i).get(FIELD_ARRAY[FIELD_PICKING_ID]);
					entry[FIELD_ARRAY.length - 1] = orderList.get(i).get(FIELD_ARRAY[FIELD_COMPANY_ID]);
					insertList.add(entry);
					System.out.println(condi);
					System.out.println(entry.toString());
					if (insertList.size() == 500 || i == orderList.size() - 1) {
						jdbcTemplate.batchUpdate(condi, insertList);
						System.out.println((i+1) + "/" + orderList.size());
						insertList = new ArrayList<>();
						Thread.sleep(500);
					}
				}
			}
		} catch (DuplicateKeyException e) {
			LoggerController.eLog
					.error("update data list to " + TABLE_NAME + " Duplicate Key. " + orderList.toString());
			LoggerController.eLog.error(e.toString());
			e.printStackTrace();
		} catch (Exception e) {
			LoggerController.eLog.error("update data list to " + TABLE_NAME + " fail. " + orderList.toString());
			LoggerController.eLog.error(e.toString());
			e.printStackTrace();
		}
	}

}
