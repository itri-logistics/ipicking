package order.dao;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.logging.log4j.util.StringMap;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import common.bean.ConnoDataSource;
import common.utils.LoggerController;
import jodd.util.ArraysUtil;

@Service
public class PickingFlowDao {
	
	private static JdbcTemplate jdbcTemplate = new JdbcTemplate(new ConnoDataSource().helloWorld());

	public static final String TABLE_NAME = "picking_flow";
	public static final int FIELD_WAREHOUSE_ID = 0;
	public static final int FIELD_DATE = 1;
	public static final int FIELD_ORDER_ID = 2;
	public static final int FIELD_GOOD_ID = 3;
	public static final int FIELD_PLACE = 4;
	public static final int FIELD_QTY = 5;
	public static final int FIELD_STORE_ID = 6;
	public static final int FIELD_PICKING_ORDER = 7;
	public static final int FIELD_CREATE_DATE = 8;
	public static final int FIELD_DOWNLOAD_DATE = 9;
	public static final int FIELD_START_DATE = 10;
	public static final int FIELD_FINISH_DATE = 11;
	public static final int FIELD_CREATOR = 12;
	public static final int FIELD_PICKER = 13;
	public static final int FIELD_PICKING_ID = 14;
	public static final int FIELD_COMPANY_ID = 15;

	public static final String[] FIELD_ARRAY = { "warehouseId","date", "orderId", "goodId", "place", "qty", "storeId", "pickingOrder",
			"createDate", "downloadDate", "startDate", "finishDate", "creator", "picker", "pickingId", "companyId" };
	public static final String FIELD_ARRAY_STRING = ArraysUtil.toString(FIELD_ARRAY);

	@Transactional
	public static void insert(String warehouseId, String date, String orderId, String goodId, String place, String qty, String storeId,
			String pickingOrder, String createDate, String downloadDate, String startDate, String finishDate,
			String creator, String picker, String pickingId, String companyId) {
		String condi = "insert " + TABLE_NAME + " (" + FIELD_ARRAY_STRING + ") values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
		jdbcTemplate.update(condi, new Object[] { warehouseId, date, orderId, goodId, place, qty, storeId, pickingOrder, createDate,
				downloadDate, startDate, finishDate, creator, picker, pickingId, companyId });
	}

	@Transactional
	public static void insertBatch(ArrayList<Object[]> orderList) {
		LoggerController.dLog.debug("insertOrderToSQLBatch(), " + orderList.toString());

		String condi = "replace into " + TABLE_NAME + " (" + FIELD_ARRAY_STRING
				+ ") values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
		try {
			if (!orderList.isEmpty()) {
				ArrayList<Object[]> insertList = new ArrayList<>();
				for (int i = 0; i < orderList.size(); i++) {
					insertList.add(orderList.get(i));
					if (insertList.size() == 500 || i == orderList.size() - 1) {
						jdbcTemplate.batchUpdate(condi, insertList);
						System.out.println(i + "/" + orderList.size());
						insertList = new ArrayList<>();
						Thread.sleep(500);
					}
				}
			}
		} catch (DuplicateKeyException e) {
			LoggerController.eLog
					.error("insert data list to " + TABLE_NAME + " Duplicate Key. " + orderList.toString());
			System.err.println("insert data list to " + TABLE_NAME + " Duplicate Key. " + orderList.toString());
			LoggerController.eLog.error(e.toString());
			e.printStackTrace();
		} catch (Exception e) {
			LoggerController.eLog.error("insert data list to " + TABLE_NAME + " fail. " + orderList.toString());
			System.err.println("insert data list to " + TABLE_NAME + " fail. " + orderList.toString());
			LoggerController.eLog.error(e.toString());
			e.printStackTrace();
		}
	}	

	public Object find(String warehouseId, String pickingId, String goodId, String companyId) {
		StringBuilder sql = new StringBuilder(
				"select * from " + TABLE_NAME + " where " 
						+ FIELD_ARRAY[FIELD_WAREHOUSE_ID] + " = ? and " 
						+ FIELD_ARRAY[FIELD_PICKING_ID] + " LIKE CONCAT(?, '%') and "
						+ FIELD_ARRAY[FIELD_GOOD_ID] + " = ? and "
						+ FIELD_ARRAY[FIELD_COMPANY_ID] + " = ?");
		List query = new ArrayList();
		query.add(warehouseId);
		query.add(pickingId);
		query.add(goodId);
		query.add(companyId);

		// System.out.println(sql);
		List<Map<String, Object>> list = jdbcTemplate.queryForList(sql.toString(), query.toArray());

		return list.isEmpty() ? null : list.get(0);
	}
	
	public Object find(String warehouseId, String date, String createDate, String storeId, String companyId) {
		StringBuilder sql = new StringBuilder("select * from " + TABLE_NAME + " where ");
		List query = new ArrayList();
		
		if (StringUtils.isNotBlank(warehouseId)) {
			sql.append(FIELD_ARRAY[FIELD_WAREHOUSE_ID] + " = ? and ");
			query.add(warehouseId);
		}
		
		if (StringUtils.isNotBlank(date)) {
			sql.append(FIELD_ARRAY[FIELD_DATE] + " = ? and ");
			query.add(date);
		}

		if (StringUtils.isNotBlank(createDate)) {
			sql.append(FIELD_ARRAY[FIELD_CREATE_DATE] + " = ? and ");
			query.add(createDate);
		}

		if (StringUtils.isNotBlank(storeId)) {
			sql.append(FIELD_ARRAY[FIELD_STORE_ID] + " = ? and ");
			query.add(storeId);
		}
		
		if (StringUtils.isNotBlank(companyId)) {
			sql.append(FIELD_ARRAY[FIELD_COMPANY_ID] + " = ? and ");
			query.add(companyId);
		}

		if (query.isEmpty()) {
			sql = sql.delete(sql.length() - 6, sql.length());
		} else {
			sql = sql.delete(sql.length() - 4, sql.length());
		}

		System.out.println(sql);
		System.out.println(query);
		List<Map<String, Object>> list = jdbcTemplate.queryForList(sql.toString(), query.toArray());

		return list;
	}

	@Transactional
	public void update(String warehouseId, String pickingId, String goodId, String companyId, String[] fieldName, String[] value) {
		String updateFieldStr = ArraysUtil.toString(fieldName).replace(",", "=?,") + "=?";
		String condi = "update " + TABLE_NAME + " set " + updateFieldStr + " where " 
				+ FIELD_ARRAY[FIELD_WAREHOUSE_ID] + "=? and "
				+FIELD_ARRAY[FIELD_PICKING_ID] + " LIKE CONCAT(?, '%') and "
				+FIELD_ARRAY[FIELD_GOOD_ID]+ "=? and "
				+FIELD_ARRAY[FIELD_COMPANY_ID]+ "=?";
		
		value = ArraysUtil.append(value, warehouseId);
		value = ArraysUtil.append(value, pickingId);
		value = ArraysUtil.append(value, goodId);
		value = ArraysUtil.append(value, companyId);
		System.out.println(condi);
		System.out.println(ArraysUtil.toString(value));
		jdbcTemplate.update(condi, value);
	}

	@Transactional
	public void update(String warehouseId, String date, String orderId, String goodId, String place, String qty, String storeId,
			String pickingOrder, String createDate, String downloadDate, String startDate, String finishDate,
			String creator, String picker, String pickingId, String companyId) {
		String updateFieldStr = ArraysUtil.toString(FIELD_ARRAY).replace(",", "=?,") + "=?";
		String condi = "update " + TABLE_NAME + " set " + updateFieldStr + " where " + FIELD_ARRAY[FIELD_PICKING_ID]
				+ " LIKE CONCAT(?, '%') and "+ FIELD_ARRAY[FIELD_COMPANY_ID]+"=?";
		System.out.println(condi);
		jdbcTemplate.update(condi, new Object[] { warehouseId, date, orderId, goodId, place, qty, storeId, pickingOrder, createDate,
				downloadDate, startDate, finishDate, creator, picker, pickingId, companyId });

	}
	
	

	@Transactional
	public static void updateBatch(List<Map<String, Object>> orderList) {
		LoggerController.dLog.debug("updateOrderToSQLBatch(), " + orderList.toString());
		String[] updateField = Arrays.copyOfRange(FIELD_ARRAY, 1, FIELD_ARRAY.length);
		String updateFieldStr = ArraysUtil.toString(updateField).replace(",", "=?,") + "=?";
		String condi = "update " + TABLE_NAME + " set " + updateFieldStr + " where " + FIELD_ARRAY[FIELD_WAREHOUSE_ID]
				+ "=? and "+FIELD_ARRAY[FIELD_PICKING_ID]
				+ " LIKE CONCAT(?, '%') and "
				+FIELD_ARRAY[FIELD_GOOD_ID] +"=? and "
				+FIELD_ARRAY[FIELD_COMPANY_ID] +"=?";
		System.out.println(condi);
		try {
			if (!orderList.isEmpty()) {
				ArrayList<Object[]> insertList = new ArrayList<>();
				for (int i = 0; i < orderList.size(); i++) {
					Object[] entry = new Object[FIELD_ARRAY.length+3];
					for (int j = 1; j < FIELD_ARRAY.length; j++) {
						entry[j - 1] = orderList.get(i).get(FIELD_ARRAY[j]);
					}
					entry[FIELD_ARRAY.length-1] = orderList.get(i).get(FIELD_ARRAY[FIELD_WAREHOUSE_ID]);
					entry[FIELD_ARRAY.length] = orderList.get(i).get(FIELD_ARRAY[FIELD_PICKING_ID]);
					entry[FIELD_ARRAY.length+1] = orderList.get(i).get(FIELD_ARRAY[FIELD_GOOD_ID]);
					entry[FIELD_ARRAY.length+2] = orderList.get(i).get(FIELD_ARRAY[FIELD_COMPANY_ID]);
					System.out.println(ArraysUtil.toString(entry));
					insertList.add(entry);
					if (insertList.size() == 500 || i == orderList.size() - 1) {
						jdbcTemplate.batchUpdate(condi, insertList);
						System.out.println((i+1) + "/" + orderList.size());
						insertList = new ArrayList<>();
						Thread.sleep(500);
					}
				}
			}
		} catch (DuplicateKeyException e) {
			LoggerController.eLog
					.error("update data list to " + TABLE_NAME + " Duplicate Key. " + orderList.toString());
			LoggerController.eLog.error(e.toString());
			System.err.println("update data list to " + TABLE_NAME + " Duplicate Key. " + orderList.toString());
			e.printStackTrace();
		} catch (Exception e) {
			LoggerController.eLog.error("update data list to " + TABLE_NAME + " fail. " + orderList.toString());
			LoggerController.eLog.error(e.toString());
			e.printStackTrace();
		}
	}
	
	

}
