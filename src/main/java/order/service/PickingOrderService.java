package order.service;

import java.io.File;
import java.io.IOException;
import java.time.Clock;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import param.Pa;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import alg.Dijkstra;
import alg.Greedy;
import alg.Greedy.CostFunction;
import alg.Path;
import alg.SimpleGeneticAlgorithm;
import alg.SimpleGeneticAlgorithm.TspFunction;
import jodd.util.StringUtil;
import map.dao.MapLinesDao;
import map.dao.MapStorageDao;
import map.dao.WarehouseDao;
import order.dao.OrderDataDao;
import order.dao.PickingFlowDao;
import order.dao.PickingOrderDao;

@Service
public class PickingOrderService {

	static Gson gson = new Gson();

	static float pickTime = 0.1f;
	static float moveRate = 90;

	public static Object getPath(String warehouseId, String pickingDate, String storeId, String createDate) {

		File file = new File(
				Pa.graphPath + File.separator + warehouseId + File.separator + pickingDate + "-" + storeId + "-"+createDate+".txt");
		if (file.exists()) {
			try {
				String context = FileUtils.readFileToString(file);
				
				return gson.fromJson(context, new TypeToken<ArrayList<HashMap<String, Double>>>(){}.getType());

			} catch (IOException e) {

				e.printStackTrace();
			}

		}

		return null;
	}

	public static String createPickingPath(String warehouseId, List<Map<String, Object>> orders, String pickingDate,
			String creator, String companyId) {
		HashMap<String, HashMap<String, Double>> nodes = new HashMap<>();
		ArrayList<String> nodeNames = new ArrayList<>();

		// 讀取揀位資料
		double[][] graph = readMap(warehouseId, nodes, nodeNames);

		//System.out.println("read graph done. " + Arrays.toString(graph));
		System.out.println("read graph done. ");

		// 產生初始揀貨單
		HashMap<String, Map<String, Object>> orderData = new HashMap<>();
		TreeMap<String, HashSet<String>> orderList = createPickingOrder(orders, orderData, pickingDate);

		//System.out.println("read order data done. " + orderData.toString());
		System.out.println("read order data done. " + orderData.size());

		// 產生揀位表
		HashSet<String> placeList = new HashSet<>();
		for (String key : orderList.keySet()) {
			placeList.addAll(orderList.get(key));
			// System.out.println(key + ": " + orderList.get(key).toString());
		}

		String[] place = placeList.toArray(new String[placeList.size()]);
		String[] orderIdArray = orderList.keySet().toArray(new String[orderList.keySet().size()]);

		HashMap<String, Dijkstra> dijkstraMap = new HashMap<>();
		dijkstraMap.put("start", new Dijkstra(graph, nodes.get("start").get("idx").intValue()));
		dijkstraMap.get("start").buildPath();
		for (String s : place) {
			try {
				int src = nodes.get(s).get("idx").intValue();
				dijkstraMap.put(s, new Dijkstra(graph, src));
				dijkstraMap.get(s).buildPath();
			} catch (Exception e) {
				System.err.println(s);
				e.printStackTrace();
				// System.exit(1);
			}

		}

		ArrayList<ArrayList<Path>> pathes = ga(dijkstraMap, nodes, orderIdArray, orderList, orderData);

		//String createDate = new JDateTime().toString("YYYYMMDDhhmmss");
		String createDate = DateTimeFormatter.ofPattern("yyyyMMddHHmmss").format(LocalDateTime.now(Clock.system(ZoneId.of("+8"))));

		// 產生揀貨流程
		ArrayList<Object[]> pickingFlowList = new ArrayList<>();
		for (String key : orderData.keySet()) {
			String[] pid = key.split("-");

			Map<String, Object> od = orderData.get(key);
			String _pickingDate;
			if (StringUtil.isNotBlank(pickingDate)) {
				_pickingDate = pickingDate;
			} else {
				_pickingDate = od.get(OrderDataDao.FIELD_ARRAY[OrderDataDao.FIELD_DATE]).toString();
			}

			pickingFlowList.add(new Object[] { warehouseId, _pickingDate,
					od.get(OrderDataDao.FIELD_ARRAY[OrderDataDao.FIELD_ORDER_ID]),
					od.get(OrderDataDao.FIELD_ARRAY[OrderDataDao.FIELD_GOOD_ID]),
					od.get(OrderDataDao.FIELD_ARRAY[OrderDataDao.FIELD_PLACE]),
					od.get(OrderDataDao.FIELD_ARRAY[OrderDataDao.FIELD_QTY]),
					od.get(OrderDataDao.FIELD_ARRAY[OrderDataDao.FIELD_STORE_ID]),
					od.get(PickingFlowDao.FIELD_ARRAY[PickingFlowDao.FIELD_PICKING_ORDER]), createDate, "", "", "",
					creator, "user", pid[0] + "-" + pid[1] + "-" + warehouseId + "-"
							+ od.get(PickingFlowDao.FIELD_ARRAY[PickingFlowDao.FIELD_PICKING_ORDER]) + "-" + pid[2], companyId });
		}
		PickingFlowDao.insertBatch(pickingFlowList);

		// 產生揀貨單頭
		ArrayList<Object[]> pickingOrderList = new ArrayList<>();
		for (String orderId : orderIdArray) {
			String[] pid = orderId.split("-");
			String _pickingDate;
			if (StringUtil.isNotBlank(pickingDate)) {
				_pickingDate = pickingDate;
			} else {
				_pickingDate = pid[0];
			}
			pickingOrderList.add(new Object[] { warehouseId, orderId + "-" + warehouseId, _pickingDate, pid[1],
					createDate, "", "", "", creator, "user", companyId});
		}
		PickingOrderDao.insertBatch(pickingOrderList);

		// 產生揀貨路徑
		for (int ii = 0; ii < orderIdArray.length; ii++) {
			String pId = orderIdArray[ii]; // date-store

			ArrayList<Path> bestPath = pathes.get(ii);
			ArrayList<HashMap<String, Double>> passNodes = new ArrayList<>();
			for (Path path : bestPath) {
				int[] pNodes = path.getPath();
				for (int idx : pNodes) {
					String nodeName = nodeNames.get(idx);
					HashMap<String, Double> pn = nodes.get(nodeName);
					passNodes.add(pn);
				}
			}

			try {
				FileUtils.write(new File(Pa.graphPath + File.separator + warehouseId + File.separator + pId+"-"+createDate + ".txt"),
						gson.toJson(passNodes));
			} catch (IOException e) {
				e.printStackTrace();
			}

		}
		
		return createDate;

	}

	private static ArrayList<ArrayList<Path>> ga(HashMap<String, Dijkstra> dijkstraMap,
			HashMap<String, HashMap<String, Double>> nodes, String[] orderIdArray,
			TreeMap<String, HashSet<String>> orderList, final HashMap<String, Map<String, Object>> orderData) {

		String start = "start";
		ArrayList<String[]> bestSolutionesGa = new ArrayList<>();
		ArrayList<Double> minDistancesGa = new ArrayList<>();
		ArrayList<ArrayList<Path>> bestPathesGa = new ArrayList<>();

		// 定義 fitness function
		TspFunction f = new TspFunction() {

			@Override
			public double cal(String[] sol) {
				double d = 0;
				ArrayList<Path> path = new ArrayList<>();
				String _start = start;
				for (String s : sol) {
					int idx = nodes.get(s).get("idx").intValue();
					path.add(dijkstraMap.get(_start).findPath(idx));
					d += path.get(path.size() - 1).getTotalDistance();
					_start = s;
				}
//				int idx = nodes.get(start).get("idx").intValue();
//				path.add(dijkstraMap.get(_start).findPath(idx));
//				d += path.get(path.size() - 1).getTotalDistance();

				// d += calTime(sol, path, i);
				return d;
			}
		};
		
		// 定義 costFunction
		CostFunction cf = new CostFunction() {
			
			@Override
			public double cal(String from, String to) {				
				
				if(from.equals("")) {
					from = start;
				}
				
				int idx = nodes.get(to).get("idx").intValue();
				Path path = dijkstraMap.get(from).findPath(idx);				
				
				return path.getTotalDistance();
			}
		};

		int cores = Runtime.getRuntime().availableProcessors();

		GaRunnable[] runnables = new GaRunnable[orderIdArray.length];
		ThreadPoolExecutor executor = new ThreadPoolExecutor(cores, cores * 3, 120, TimeUnit.SECONDS,
				new LinkedBlockingDeque<Runnable>());
		for (int ii = 0; ii < orderIdArray.length; ii++) {
			String gId = orderIdArray[ii];
			runnables[ii] = new GaRunnable(cf, f, orderList.get(gId).toArray(new String[orderList.get(gId).size()]));
			executor.execute(runnables[ii]);
		}

		executor.shutdown();
		try {
			while (!executor.awaitTermination(10, TimeUnit.SECONDS)) {
				System.out.println("執行中任務數 =" + executor.getActiveCount());
				System.out.println("佇列任務數 =" + executor.getCompletedTaskCount() + " / " + orderIdArray.length);
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		for (int ii = 0; ii < orderIdArray.length; ii++) {
			String gId = orderIdArray[ii];

			String[] bestSolution = runnables[ii].getBestSolution();

			bestSolutionesGa.add(bestSolution);
			minDistancesGa.add(f.cal(bestSolution));

			ArrayList<Path> bestPath = new ArrayList<>();
			String _start = start;
			int po = 1;
			for (String s : bestSolution) {
				int idx = nodes.get(s).get("idx").intValue();
				bestPath.add(dijkstraMap.get(_start).findPath(idx));
				_start = s;
				orderData.get(gId + "-" + s).put(PickingFlowDao.FIELD_ARRAY[PickingFlowDao.FIELD_PICKING_ORDER], po);
				po++;
			}

			double[] _dis = new double[bestPath.size()];
			for (int i = 0; i < _dis.length; i++) {
				_dis[i] = bestPath.get(i).getTotalDistance();
			}

			bestPathesGa.add(bestPath);

			System.out.println("GA: " + ii + "/" + orderIdArray.length);
		}

		System.out.println("GA: ");
		for (int i = 0; i < orderIdArray.length; i++) {
			String gId = orderIdArray[i];
			System.out.println("路序 " + gId + ": " + Arrays.toString(bestSolutionesGa.get(i)));
			System.out.println("總距離 " + gId + ": " + minDistancesGa.get(i));
		}

		return bestPathesGa;

	}

	private static ArrayList<float[]> calTime(ArrayList<String[]> solutiones, ArrayList<ArrayList<Path>> pathes,
			String[] orderIdArray, HashMap<String, HashMap<String, Object>> orderData) {
		ArrayList<float[]> arriveTimes = new ArrayList<>();
		int orderNum = solutiones.size();

		for (int i = 0; i < orderNum; i++) {
			String[] solution = solutiones.get(i);
			if (solution == null) {
				float[] arriveTime = new float[0];
				arriveTimes.add(arriveTime);
				continue;
			}
			ArrayList<Path> path = pathes.get(i);
			int nodeNum = solution.length;
			float[] arriveTime = new float[nodeNum];
			for (int j = 0; j < nodeNum; j++) {

				arriveTime[j] = (float) (path.get(j).getTotalDistance() / moveRate) + pickTime
						* Integer.parseInt(String.valueOf(orderData.get(orderIdArray[i] + "-" + solution[j])));

			}

			arriveTimes.add(arriveTime);

		}
		return arriveTimes;
	}

	// 將同門市、同揀貨日的訂單併成同一張揀貨單
	private static TreeMap<String, HashSet<String>> createPickingOrder(List<Map<String, Object>> orders,
			HashMap<String, Map<String, Object>> orderData, String pickingDate) {

		TreeMap<String, HashSet<String>> orderList = new TreeMap<>();

		for (Map<String, Object> order : orders) {
			String pid;
			if (StringUtil.isNotBlank(pickingDate)) {
				pid = pickingDate + "-" + order.get(OrderDataDao.FIELD_ARRAY[OrderDataDao.FIELD_STORE_ID]);
			} else {
				pid = order.get(OrderDataDao.FIELD_ARRAY[OrderDataDao.FIELD_DATE]) + "-"
						+ order.get(OrderDataDao.FIELD_ARRAY[OrderDataDao.FIELD_STORE_ID]);
			}
			String place = order.get(OrderDataDao.FIELD_ARRAY[OrderDataDao.FIELD_PLACE]).toString();
			if (!orderList.containsKey(pid)) {
				orderList.put(pid, new HashSet<>());
			}

			orderList.get(pid).add(place);

			if (orderData.containsKey(pid + "-" + place)) {
				Map<String, Object> _order = orderData.get(pid + "-" + place);
				int qty = Integer.parseInt(_order.get(OrderDataDao.FIELD_ARRAY[OrderDataDao.FIELD_QTY]).toString())
						+ Integer.parseInt(order.get(OrderDataDao.FIELD_ARRAY[OrderDataDao.FIELD_QTY]).toString());
				orderData.get(pid + "-" + place).put(OrderDataDao.FIELD_ARRAY[OrderDataDao.FIELD_QTY], qty);
			} else {
				orderData.put(pid + "-" + place, order);
			}

		}

		System.out.println(orderData);

		return orderList;

	}

	private static double[][] readMap(String warehouseId, final HashMap<String, HashMap<String, Double>> nodes,
			final ArrayList<String> nodeName) {

		//Map<String, Object> start = WarehouseDao.getWarehouse(warehouseId);

		HashMap<String, Double> node = new HashMap<>();
		node.put("idx", (double) nodes.size());
		//node.put("x", Double.parseDouble(start.get(WarehouseDao.FIELD_ARRAY[WarehouseDao.FIELD_START_X]).toString()));
		//node.put("y", Double.parseDouble(start.get(WarehouseDao.FIELD_ARRAY[WarehouseDao.FIELD_START_Y]).toString()));
		node.put("x", 0.0);
		node.put("y", 0.0);
		node.put("width", 0.0);

		nodes.put("start", node);
		nodeName.add("start");

		List<Map<String, Object>> storages = MapStorageDao.find(warehouseId);

		for (int i = 0; i < storages.size(); i++) {
			Map<String, Object> storage = storages.get(i);
			node = new HashMap<>();
			node.put("idx", (double) nodes.size());
			node.put("x", Double.parseDouble(storage.get(MapStorageDao.FIELD_ARRAY[MapStorageDao.FIELD_X]).toString()));
			node.put("y", Double.parseDouble(storage.get(MapStorageDao.FIELD_ARRAY[MapStorageDao.FIELD_Y]).toString()));
			node.put("width", Double.parseDouble(storage.get(MapStorageDao.FIELD_ARRAY[MapStorageDao.FIELD_WIDTH]).toString()));
			String name = storage.get(MapStorageDao.FIELD_ARRAY[MapStorageDao.FIELD_PLACE_ID]).toString();
			nodes.put(name, node);
			nodeName.add(name);
		}

		List<Map<String, Object>> lines = MapLinesDao.find(warehouseId);
		List<Map<String, Object>> _lines = new ArrayList<>();
		for (int i = 0; i < lines.size(); i++) {
			Map<String, Object> line = lines.get(i);

			String name = "line" + i;

			_lines.add(line);
			HashMap<String, Object> _line = new HashMap<>();
			_line.put("x1", line.get("x2"));
			_line.put("y1", line.get("y1"));
			_line.put("x2", line.get("x1"));
			_line.put("y2", line.get("y2"));
			_lines.add(_line);

			for (int x = 1; x <= 2; x++) {
				for (int y = 1; y <= 2; y++) {
					node = new HashMap<>();
					node.put("idx", (double) nodes.size());
					node.put("x", Double.parseDouble(line.get("x" + x).toString()));
					node.put("y", Double.parseDouble(line.get("y" + y).toString()));
					node.put("width", 0.0);
					node.put("orien", 0.0);
					nodes.put(name + "_x" + x + "y" + y, node);
					nodeName.add(name + "_x" + x + "y" + y);
				}
			}

		}

		double[][] graph = new double[nodes.size()][nodes.size()];
		for (int i = 0; i < nodes.size(); i++) {
			for (int j = 0; j < nodes.size(); j++) {			
				
				if (isConnected(_lines, nodes.get(nodeName.get(i)), nodes.get(nodeName.get(j)))) {
					graph[i][j] = distance(nodes.get(nodeName.get(i)).get("x"), nodes.get(nodeName.get(i)).get("y"),
							nodes.get(nodeName.get(j)).get("x"), nodes.get(nodeName.get(j)).get("y"));
				} else {
					graph[i][j] = Double.POSITIVE_INFINITY;
				}
			}
		}

		return graph;

	}

	private static boolean isConnected(List<Map<String, Object>> lines, HashMap<String, Double> node1,
			HashMap<String, Double> node2) {

		double x1 = node1.get("x");
		double y1 = node1.get("y");
		double x2 = node2.get("x");
		double y2 = node2.get("y");


		for (int i = 0; i < lines.size(); i++) {
			double x3 = Double.parseDouble(lines.get(i).get("x1").toString());
			double y3 = Double.parseDouble(lines.get(i).get("y1").toString());
			double x4 = Double.parseDouble(lines.get(i).get("x2").toString());
			double y4 = Double.parseDouble(lines.get(i).get("y2").toString());			
			

			double c1 = (x2 * y1 - x1 * y2) / (x2 - x1); // y截距
			double c2 = (x4 * y3 - x3 * y4) / (x4 - x3);

			if (x1 == x2) {
				double m2 = (y4 - y3) / (x4 - x3);// 斜率
				double y = m2 * x1 + c2;

				boolean xIn1 = x1 > Math.min(x3, x4) && x1 < Math.max(x3, x4);
				boolean yIn1 = y > Math.min(y3, y4) && y < Math.max(y3, y4);

				
				boolean yIn2 = y > Math.min(y1 , y2 )
						&& y < Math.max(y1 , y2 );				
				
				if (xIn1 && (yIn2 && yIn1)) {					
					return false;											
				}

			} else if(y1==y2){
				double m2 = (y4 - y3) / (x4 - x3);// 斜率

				double x = (y1-c2)/ m2;

				boolean xIn1 = x > Math.min(x3, x4) && x < Math.max(x3, x4);
				boolean yIn1 = y1 > Math.min(y3, y4) && y1 < Math.max(y3, y4);

				boolean xIn2 = x > Math.min(x1, x2)
						&& x < Math.max(x1, x2);
								
				if ((xIn1 && xIn2) && yIn1) {					
						return false;
				}
			}else {
				double m1 = (y2 - y1) / (x2 - x1);// 斜率
				double m2 = (y4 - y3) / (x4 - x3);// 斜率

				double x = (c2 - c1) / (m1 - m2);

				double y = m1 * x + c1;

				boolean xIn1 = x >= Math.min(x3, x4) && x < Math.max(x3, x4);
				boolean yIn1 = y >= Math.min(y3, y4) && y < Math.max(y3, y4);

				boolean xIn2 = x > Math.min(x1, x2)
						&& x < Math.max(x1, x2);
				boolean yIn2 = y > Math.min(y1, y2)
						&& y < Math.max(y1, y2);				
				
				if (xIn1 && yIn1) {
					if (xIn2 && yIn2)
						return false;
				}
			}

		}

		return true;
	}

	private static double distance(double x1, double y1, double x2, double y2) {
		return Math.hypot(x1 - x2, y1 - y2);
	}

	private static void swap(Object[] array, int x1, int x2) {// 交換
		Object z = array[x1];
		array[x1] = array[x2];
		array[x2] = z;
	}

}

class GaRunnable implements Runnable {

	CostFunction cf;
	TspFunction f;
	String[] initialSolution;
	String[] bestSolution;

	public GaRunnable(CostFunction cf, TspFunction f, String[] initialSolution) {
		super();
		this.cf = cf;
		this.f = f;
		this.initialSolution = initialSolution;
	}

	@Override
	public void run() {
		//System.out.println(Arrays.toString(initialSolution));
		Greedy gdy = new Greedy(cf, initialSolution);
		initialSolution = gdy.run();
		//System.out.println(Arrays.toString(initialSolution));
		
		SimpleGeneticAlgorithm ga = new SimpleGeneticAlgorithm(f, initialSolution);
		ga.setMaxGeneration(8000);
		ga.setUnchangeLimit(600);
		ga.setPc(0.8);
		ga.setPm(0.7);
		ga.initialSolution();
		try {
			ga.run();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		bestSolution = ga.getBest();
	}

	public String[] getBestSolution() {
		return bestSolution;
	}

}
