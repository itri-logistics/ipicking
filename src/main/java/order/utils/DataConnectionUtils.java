package order.utils;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.FileUtils;
import param.Pa;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import jodd.http.HttpRequest;
import jodd.http.HttpResponse;

public class DataConnectionUtils {

	 // iDeploy IP
	static String server = "http://172.104.67.158";	//測試
//	static String server = "http://139.162.112.104";	//正式
	
	public static String getToken(String companyId, String userId) {
		Gson gson = new Gson();

		Map<String, Object> args = new HashMap<>();
		args.put("companyAccount", companyId);
		args.put("userAccount", userId);

		HttpResponse response = HttpRequest.post(server+"/iDeploy/login").body(gson.toJson(args))
				.contentType("application/json").send();

		//System.out.println(response.body());

		Map<String, Object> jsonObj = gson.fromJson(response.body(), new TypeToken<Map<String, Object>>() {
		}.getType());
		String token = jsonObj.get("token").toString();
		//System.out.println(token);
		return token;
	}
	
	public static boolean getLatestLayout(String token, String warehouseId) {
		try {
			Gson gson = new Gson();
			String url = server+"/iDeploy/api/jwt/warehouse/"
					+ warehouseId + "/model_ids";
			System.out.println(url);
			HttpResponse response = HttpRequest.get(url).tokenAuthentication(token).send();

			Map<String, Object> jsonObj = gson.fromJson(response.body(), new TypeToken<Map<String, Object>>() {
			}.getType());
			
			String jsonListStr = gson.toJson(jsonObj.get("model_id_list"));
			List<Object> jsonList = gson.fromJson(jsonListStr, new TypeToken<List<Object>>() {
			}.getType());
			
			for(int ii = jsonList.size() - 1 ; ii>=0;ii-- ) {
				String layoutId = String.valueOf(jsonList.get(ii));
				layoutId = String.valueOf((int)Double.parseDouble(layoutId));
				if(getLayout(token, warehouseId, layoutId)) {
					return true;
				}
			}

			return false;

		} catch (Exception e) {
			e.printStackTrace();
		}

		return false;

	}

	public static boolean getLayout(String token, String warehouseId, String layoutId) {
		try {
			Gson gson = new Gson();

			HttpResponse response = HttpRequest.get(server+"/iDeploy/api/jwt/warehouse/"
					+ warehouseId + "/position_names/" + layoutId).tokenAuthentication(token).send();

			//System.out.println(response.body());

			Map<String, Object> jsonObj = gson.fromJson(response.body(), new TypeToken<Map<String, Object>>() {
			}.getType());

			List<String> posId = (List<String>) jsonObj.get("ranked_pos_id");
			List<List<String>> posName = (List<List<String>>) jsonObj.get("ranked_pos_name");
			List<String> productId = (List<String>) jsonObj.get("ranked_product_id");

			response = HttpRequest.get(server+"/iDeploy/warehouse_env_params/" + warehouseId)
					.send();

			//System.out.println(response.body());

			jsonObj = gson.fromJson(response.body(), new TypeToken<Map<String, Object>>() {
			}.getType());

			List<String> depth = (List<String>) gson.fromJson(jsonObj.get("grid_depth_json").toString(),
					new TypeToken<List<String>>() {}.getType());
			List<String> width = (List<String>) gson.fromJson(jsonObj.get("grid_width_json").toString(),
					new TypeToken<List<String>>() {}.getType());
			List<String> rackId = (List<String>) gson.fromJson(jsonObj.get("rack_id_json").toString(),
					new TypeToken<List<String>>() {}.getType());
			List<String> xs = (List<String>) gson.fromJson(jsonObj.get("x_pos_all_json").toString(),
					new TypeToken<List<String>>() {}.getType());
			List<String> ys = (List<String>) gson.fromJson(jsonObj.get("y_pos_all_json").toString(),
					new TypeToken<List<String>>() {}.getType());
			

			System.out.println(xs.size()+", "+posId.size());
			
			HashSet<Integer> drawed = new HashSet<>();
			
			// 建地圖
			StringBuilder stb = new StringBuilder();
			StringBuilder stbP = new StringBuilder();
			for (int i = 0; i < posId.size(); i++) {
				int idx = Integer.parseInt(posId.get(i)) - 1;
				// placeId, x, y, orien, width, depth
				int orien = Integer.parseInt(rackId.get(idx)) % 2;
				drawed.add(idx);
				for(int j = 0;j<posName.get(i).size();j++) {
					stb.append(";" + posName.get(i).get(j) + "," + xs.get(idx) + "," + ys.get(idx) + "," + orien + ","
							+ width.get(idx) + "," + depth.get(idx));
				}				

				// goodId, placeId
				stbP.append(";" + productId.get(i) + "," + posName.get(i).get(0));				
			}
			
			for(int idx = 0; idx < xs.size();idx++) {
				if(!drawed.contains(idx)) {
					int orien = Integer.parseInt(rackId.get(idx)) % 2;
					stb.append(";" + "unnamed," + xs.get(idx) + "," + ys.get(idx) + "," + orien + ","
							+ width.get(idx) + "," + depth.get(idx));					
				}
			}
			
			if (stb.length() > 0) {
				stb.deleteCharAt(0);
			}
			if (stbP.length() > 0) {
				stbP.deleteCharAt(0);
			}

			String outPath = Pa.mapPath + File.separator + warehouseId + File.separator + "map_storage.txt";

			FileUtils.write(new File(outPath), stb.toString());

			outPath = Pa.mapPath + File.separator + warehouseId + File.separator + layoutId + ".txt";

			FileUtils.write(new File(outPath), stbP.toString());

			List<Object> rackLines = (List<Object>) gson.fromJson(jsonObj.get("rack_lines_json").toString(),
					new TypeToken<List<Object>>() {}.getType());
			stb = new StringBuilder();
			for (int i = 0; i < rackLines.size(); i++) {
				List<String> line = (List<String>) gson.fromJson(rackLines.get(i).toString(), new TypeToken<List<String>>() {}.getType());
				// x1, y1, z1, x2, y2, z2
				stb.append(";" + line.get(0) + "," + line.get(1) + "," + line.get(3) + "," + line.get(4));
			}
			if (stb.length() > 0) {
				stb.deleteCharAt(0);
			}
			outPath = Pa.mapPath + File.separator + warehouseId + File.separator + "map_lines.txt";

			FileUtils.write(new File(outPath), stb.toString());

			return true;

		} catch (Exception e) {
			e.printStackTrace();
		}

		return false;

	}

}
