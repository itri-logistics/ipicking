package param;

import java.io.File;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.List;

import org.apache.commons.io.FileUtils;

/***
 * 共用參數
 * 
 * @author ATone create by 2017/04/27
 */

public class Pa {
	//final public static String rootPath = "D:" + File.separator + "itri_ai_picker";
	final public static String rootPath = System.getProperty("user.home") + File.separator + "itri_ai_picker_test";
	static {
		HashMap<String, String> mapConfig = new HashMap<>();
		File confFile = new File(rootPath + File.separator + ".config");
		if (!confFile.exists()) {
			System.err.println("設定檔不存在: " + confFile.getAbsolutePath());
		} else {
			try {
				
				try {
					List<String> configLines = FileUtils.readLines(confFile);
					if (!configLines.isEmpty()) {
						for(String s:configLines) {
							String[] conf = s.split("#");
							mapConfig.put(conf[0], conf[1]);
						}
					}					
					System.out.println("設定檔讀取完畢.");
				} catch (Exception e) {
					e.printStackTrace();
				}

			} catch (Exception e) {
				e.printStackTrace();
				System.err.println("設定檔讀取錯誤");
			}
		}
		System.out.println(mapConfig.toString());
		//log4j2 config setting
		System.setProperty("log4j.configurationFile",rootPath +File.separator+"log4j-config"+File.separator+"log4j2.xml");
		
	}

	final public static String DB_USER = "itri_picker";
	final public static String DB_PWD = "t9X#4&@#*URs9yS%aM5g";
	final public static String DB_NAME = "itri_picker_test";
	final public static String CDN_IP = "http://imeasures.org/";
	//final public static String DB = "localhost";
	final public static String DB = "60.248.82.82";
	//final public static String DB = "61.221.12.34";

	public static RsMsg error = new RsMsg("error", "error");
	public static RsMsg success = new RsMsg("success", "success");

	final public static String JS_VERSION = DateTimeFormatter.ofPattern("yyyyMMddHHmmss").format(LocalDateTime.now());
	final public static String CDN_ADMIN_LIB_LINK = CDN_IP + "itri_ai";

	final public static String examplePath = rootPath + File.separator + "picker_example";
	final public static String graphPath = rootPath + File.separator + "picker_graph";
	final public static String mapPath = rootPath + File.separator + "picker_map";
	final public static String imgPath = rootPath + File.separator + "picker_img";
	final public static String uploadPath = rootPath + File.separator + "picker_upload";
			

	public static int PAGE_NUM = 16;
}
