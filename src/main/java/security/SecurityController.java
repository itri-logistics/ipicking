package security;


import java.time.Clock;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;

import org.apache.logging.log4j.Logger;
import org.springframework.security.core.context.SecurityContextHolder;

public class SecurityController {
	
	public String getUpdator(){
		if(SecurityContextHolder.getContext()==null||SecurityContextHolder.getContext().getAuthentication()==null){
			return "system";
		}else{
			ManageUser user = (ManageUser)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
			return user.getUsername();
		}
	}
	
	public String getCompanyId(){
		try {
			ManageUser user = (ManageUser)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
			return user.getCompanyId();
		} catch (Exception e) {
			//e.printStackTrace();
			return "null";
		}
	}
	
	public String getType() {
		try {
			ManageUser user = (ManageUser)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
			return user.getType();
		} catch (Exception e) {
			e.printStackTrace();
			return "null";
		}
	}	
	
	public String getUpdateDate(){
		return DateTimeFormatter.ofPattern("yyyyMMddHHmmss").format(LocalDateTime.now(Clock.system(ZoneId.of("+8"))));
	}
	
}
